#ifndef H_GRASSMANAGER_
#define H_GRASSMANAGER_

#include "Grass.h"

#include <vector>

class GrassManager
{
public:

	GrassManager()
	~GrassManager();

	void Update(u32 dt);
	void Render();

private:
	
	Grass mGrassModel;
};

#endif