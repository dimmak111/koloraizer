#ifndef H_GLOWSEQUENCE_
#define H_GLOWSEQUENCE_

#include <Asset/Shader.h>
#include <Core/Camera.h>
#include <Core/Sequence.h>
#include <Renderer/FrameBufferObject.h>
#include <Renderer/Mesh.h>
#include <Renderer/Sprite.h>

class GlowSequence : public Sequence
{
public:
	GlowSequence(App* app);
	virtual ~GlowSequence();

	virtual void Init();
	virtual void Update(u32 dt=0);
	virtual void Render();

	virtual void ProcessEvents(Input &input);

private:

};

#endif