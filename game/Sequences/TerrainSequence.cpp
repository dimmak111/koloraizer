#include "TerrainSequence.h"

#include <Math/Vector4.h>
#include <Renderer/Renderer.h>

#include <iostream>

TerrainSequence::TerrainSequence(App* app) 
: Sequence(app)
{
}

TerrainSequence::~TerrainSequence()
{
}

void TerrainSequence::Init()
{
	mCamera.Set(Vector3f(0, 100, -200), Vector3f(256, 0, 0), Vector3f(0, 1, 0), GetApp());

	mSkyBox = new SkyBox("ressources/images/skyboxes/sky_front.png",
						 "ressources/images/skyboxes/sky_back.png",
						 "ressources/images/skyboxes/sky_right.png",
						 "ressources/images/skyboxes/sky_left.png",
						 "ressources/images/skyboxes/sky_top.png",
						 "ressources/images/skyboxes/sky_bottom.png");
	//Add(mSkyBox);

	mTerrain = new Terrain(500,500,120,10);
	Add(mTerrain);
}

void TerrainSequence::Update(u32 dt)
{
	mCamera.ProcessEvents(GetInputManager());
	Renderer::Call().Set3DViewMatrix(mCamera.GetViewMatrix());
}

void TerrainSequence::ProcessEvents(Input& input)
{
	if(input.IsKeyUp(Key_R)) {
		mTerrain->SetSeed(static_cast<f32>(rand() % 4096));
	}

	
	// Picking
	/*
	if ( input.IsKeyUp(Key_P)) {
		s32 width =  GetApp().GetWindow().GetWidth();
		s32 height = GetApp().GetWindow().GetHeight();
		s32 mouseX = Math::Clamp<s32>(input.GetMouseX(), 0, width);
		s32 mouseY = Math::Clamp<s32>(input.GetMouseY(), 0, height);
		mouseY = height - mouseY;
		float depth;

		glReadPixels(mouseX, mouseY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
		std::cout.precision(2);
		// std::cout << mouseX << ", " << mouseY << ": " << depth << "\n";
		if ( depth != 1.f ) {

			Vector3f mouseCoord(mouseX, mouseY, depth);
			Vector4f viewport(0, GetApp().GetWindow().GetWidth(), 0, GetApp().GetWindow().GetHeight());
			
			const Matrix4& view(Renderer::Call().Get3DViewMatrix());
			const Matrix4& proj(Renderer::Call().Get3DProjectionMatrix());

			Matrix4 inverse = proj*view;
			inverse.Inverse();

			float x = (mouseCoord[0] / width) * 2.f - 1.f;
			float y = (mouseCoord[1] / height) * 2.f - 1.f;  
			float z = mouseCoord[2] * 2.f - 1.f;
			
			Vector4f tmp(x, y, z, 1.f);
			//std::cout << tmp[0] << ", " << tmp[1] << ", " << tmp[2] << "\n";
			tmp = inverse * tmp;
			tmp /= tmp[3];
			std::cout << tmp[0] << ", " << tmp[1] << ", " << tmp[2] << "\n";
		}
	}*/
}