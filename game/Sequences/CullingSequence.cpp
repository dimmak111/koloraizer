#include "CullingSequence.h"

#include <Math/AABB.h>

#include <vector>
#include <iostream>

CullingSequence::CullingSequence(App* app) : Sequence(app)
{
}

CullingSequence::~CullingSequence()
{
	glDeleteQueries(mNumTeapot*mNumTeapot, &(mQueries[0]));
}

void CullingSequence::Init()
{
	mCam.Set(Vector3f(0, 0, -100), Vector3f(0, 0, 0), Vector3f(0, 1, 0), GetApp());
	mCam2.Set(Vector3f(0, 0,-100), Vector3f(0, 0, 0), Vector3f(0, 1, 0), GetApp());

	mDefProgram.ComputeAndLink("ressources/shaders/normal.vs", "ressources/shaders/normal.fs");
	mBoxProgram.ComputeAndLink("ressources/shaders/default.vs", "ressources/shaders/default.fs");
	mCullProgram.ComputeAndLink("ressources/shaders/culling.vs", "ressources/shaders/culling.fs", "ressources/shaders/culling.gs");

	mTeapot = new Mesh();
	mTeapot->SetShaderToUse(&mDefProgram);
	mTeapot->FromOBJ("ressources/models/teapot.obj");
	mTeapot->CreateAABB();
	mTeapot->SetScale(0.2f ,0.2f, 0.2f);
	Add(mTeapot);

	mCam.AttachFrustum(&mFrustum);
	mFrustum.AttachCamera(&mCam);

	mSimplified = new Mesh();
	mSimplified->SetShaderToUse(&mBoxProgram);
	mSimplified->MakeCube();
	Add(mSimplified);

	mIsTestMode = true;
	mCullingMode = eCullingCPU;

	mNumTeapot = 10;

	mQueries.resize(mNumTeapot*mNumTeapot, 0);
	glGenQueries(mNumTeapot*mNumTeapot, &(mQueries[0]));

	mInFrustum.resize(mNumTeapot*mNumTeapot, true);

	HowTo();
}

void CullingSequence::InitTBO()
{
	//glGenBuffers(1, &mTBO);
}

void CullingSequence::HowTo() 
{
	std::cout << "Use [T] to switch between culling and non culling camera" << eol;
	std::cout << "CPU culling mode" << eol;
}

void CullingSequence::Update(u32 dt) 
{
	if(mIsTestMode) {
		Renderer::Call().Set3DViewMatrix(mCam.GetViewMatrix());
		mCam.ProcessEvents(GetInputManager());
	}
	else {
		Renderer::Call().Set3DViewMatrix(mCam2.GetViewMatrix());
		mCam2.ProcessEvents(GetInputManager());
	}
}

void CullingSequence::Render() 
{	
	if(mCullingMode == eCullingCPU) {
		CPUCulling();
	} else {
		GPUCulling();
	}
}

void CullingSequence::GPUCulling() 
{
}

void CullingSequence::CPUCulling() 
{
	u32 visible = 0;
	u32 result = 0;
	u32 cur = 0;

	s32 Half = mNumTeapot * 0.5f;

	if(mIsTestMode) {
		
		glColorMask(0, 0, 0, 0);
		glDepthMask(GL_FALSE);
		
		AABB b = mTeapot->GetAABB();

		for(s32 i = Half; i > -Half; --i) {
			for(s32 j = Half; j > -Half; --j) {

				Vector3f size = b.GetEnd()-b.GetOrigin();
				Vector3f scale = mTeapot->GetScale();

				mSimplified->SetScale(size.x*scale.x,size.y*scale.y,size.z*scale.z);
				mSimplified->SetPosition(i*25,0,j*25);
			
				if(mFrustum.IsMeshInside(mSimplified)) {
					glBeginQuery(GL_SAMPLES_PASSED, mQueries[cur]);
					mSimplified->Render();
					glEndQuery(GL_SAMPLES_PASSED);	
					mInFrustum[cur] = true;
				} else {
					mInFrustum[cur] = false;
				}

				++cur;

			}
		}

		glDepthMask(GL_TRUE);
		glColorMask(1, 1, 1, 1);
	}

	cur = 0;
	for(s32 i = Half; i > -Half; --i) {
		for(s32 j = Half; j > -Half; --j) {
			
			if(mInFrustum[cur]) {
				mTeapot->SetPosition(i*25, 0, j*25);
				glGetQueryObjectuiv(mQueries[cur], GL_QUERY_RESULT, &result);		
				if(result > 0) {
					mTeapot->Render();
					visible++;
				}
			}
			++cur;
		}
	}
}

void CullingSequence::ProcessEvents(Input& input)
{
	if(input.IsKeyUp(Key_T)) {
		mIsTestMode = !mIsTestMode;
	}

	if(input.IsKeyUp(Key_C)) {
		mCullingMode = mCullingMode == eCullingCPU ? eCullingGPU : eCullingCPU;

		if(mCullingMode == eCullingCPU) {
			std::cout << "CPU culling mode"<<eol;
		}else {
			std::cout << "GPU culling mode"<<eol;
		}
	}
}
