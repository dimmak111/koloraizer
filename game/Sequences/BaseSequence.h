#ifndef H_BASESEQUENCE_
#define H_BASESEQUENCE_

#include <Asset/Shader.h>

#include <Core/Sequence.h>

#include <Renderer/Geometry.h>
#include <Renderer/Materials/DiffuseMaterial.h>
#include <Renderer/Mesh.h>


class App;

class BaseSequence : public Sequence
{
public:
	
	BaseSequence(App* app);

	virtual void Init();
	virtual void Update(u32 dt=0);

private:

	Geometry mGeom;
	DiffuseMaterial mMat;
	Shader mShader;

	Mesh* mCube;
};

#endif