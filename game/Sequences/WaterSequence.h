#ifndef H_WATERSEQUENCE_
#define H_WATERSEQUENCE_

#include <Asset/Shader.h>

#include <Core/Camera.h>
#include <Core/Sequence.h>

#include <Renderer/Mesh.h>
#include <Renderer/FrameBufferObject.h>


class WaterSequence : public Sequence
{
public:
	WaterSequence(App* app);
	virtual ~WaterSequence();

	virtual void Init();
	virtual void Update(u32 dt);

	virtual void ProcessEvents(Input& input);

private:

	Camera mCamera;
	Mesh* mCube;

	Shader mDefaultShader;
	FrameBufferObject mFBO;
};

#endif