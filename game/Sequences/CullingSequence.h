#ifndef H_CULLINGSEQUENCE_
#define H_CULLINGSEQUENCE_

#include <Asset/Shader.h>
#include <Core/Camera.h>
#include <Core/Frustum.h>
#include <Core/Sequence.h>
#include <Renderer/Mesh.h>

#include <vector>

class CullingSequence : public Sequence
{
public:

	enum eCullingMode 
	{
		eCullingCPU = 0,
		eCullingGPU
	};

	CullingSequence(App* app);
	virtual ~CullingSequence();

	virtual void HowTo();

	virtual void Init();
	void InitTBO();
	virtual void Update(u32 dt);

	virtual void Render();
	
	void CPUCulling();
	void GPUCulling();

	virtual void ProcessEvents(Input& input);

private:

	Shader mDefProgram;
	Shader mBoxProgram;
	Shader mCullProgram;

	Mesh* mTeapot;
	Mesh* mSimplified;

	Camera mCam;
	Camera mCam2;

	Frustum mFrustum;

	bool mIsTestMode;
	eCullingMode mCullingMode;

	s32 mNumTeapot;
	std::vector<u32> mQueries;
	std::vector<bool> mInFrustum;

	u32 mTBO;
};

#endif