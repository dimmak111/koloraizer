#include "NoeSequence.h"

#include <Renderer/Renderer.h>

#include <App.h>

#include <iostream>

NoeSequence::NoeSequence(App *app)
: Sequence(app)
{
}

void NoeSequence::Init()
{
    mGeom.MakePlane(0.5f, 0.5f);
    //mGeom.MakeCube(1.f);

    mShader.ComputeAndLink("ressources/shaders/old/2d.vs", "ressources/shaders/old/2d.fs");
    //mShader.ComputeAndLink("ressources/shaders/", "ressources/shaders/old/2d.fs");

    mMat.SetShader(&mShader);
    mMat.SetDiffuseColor(Color(0.6f, 0.2f, 0.2f));

    mChar = new Mesh(&mGeom, &mMat);
    //mChar->SetRotation(-90.0f, 0.0f, 0.f);
    Add(mChar);

    // 2D view
    Matrix4 id;
    id.Identity();
    float halfWidth = GetApp().GetWindow().GetHalfWidth();
    float halfHeight = GetApp().GetWindow().GetHalfHeight();

    Matrix4 view2D;
    view2D.MakeOrtho(-halfWidth, halfWidth, -halfHeight, halfHeight);
    Renderer::Call().Set2DProjectionMatrix(view2D);
    Renderer::Call().Set3DProjectionMatrix(id);
    Renderer::Call().Set3DViewMatrix(id);
    //Renderer::Call().Set3DViewMatrix(view2D);
}

float lastmx, lastmy;

void NoeSequence::Update(u32 dt)
{
    float mouseX = GetInputManager().GetMouseX();
    float mouseY = GetInputManager().GetMouseY();
    float height = GetApp().GetWindow().GetHeight();

    if ( lastmx != mouseX || lastmy != mouseY )
	std::cout << "mouse : " << mouseX << "," << mouseY << "\n";

    lastmx = mouseX;
    lastmy = mouseY;

    mouseX = (mouseX / GetApp().GetWindow().GetHalfWidth()) - 1.f;
    mouseY = (-mouseY / GetApp().GetWindow().GetHalfHeight()) + 1.f;;




    mChar->SetPosition( mouseX, mouseY, 0 );
}
