#include "GlowSequence.h"

GlowSequence::GlowSequence(App* app)
: Sequence(app)
{
}

GlowSequence::~GlowSequence()
{
}

void GlowSequence::Init()
{
}

void GlowSequence::Update(u32 dt)
{
}

void GlowSequence::Render() 
{
}

void GlowSequence::ProcessEvents(Input &input)
{

}

	