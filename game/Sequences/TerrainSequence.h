#ifndef H_TERRAINSEQUENCE_
#define H_TERRAINSEQUENCE_

#include <Asset/Shader.h>
#include <Asset/Texture.h>
#include <Core/Camera.h>
#include <Core/Sequence.h>
#include <Renderer/FrameBufferObject.h>
#include <Renderer/Mesh.h>
#include <Renderer/Sprite.h>

#include <Terrain.h>
#include <SkyBox.h>

class Input;

class TerrainSequence : public Sequence
{
public:
	TerrainSequence(App* app);
	virtual ~TerrainSequence();

	virtual void Init();
	virtual void Update(u32 dt=0);

	virtual void ProcessEvents(Input& input);

private:

	Camera mCamera;
	Terrain* mTerrain;
	SkyBox* mSkyBox;

	Shader mSprShad;
	Texture mSprText;
	Sprite* mSpr;
};

#endif