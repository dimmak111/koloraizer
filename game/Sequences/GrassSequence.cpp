#include "GrassSequence.h"

#include <Renderer/Renderer.h>
#include <Renderer/Mesh.h>

#include "Grass.h"

#include <cstdio>

GrassSequence::GrassSequence(App* app)
: Sequence(app), mGrassModel(NULL)
{
}

GrassSequence::~GrassSequence()
{
	delete mGrassModel;
}

void GrassSequence::Init()
{
	srand ( time(NULL) );

	Matrix4 view;
	view.LookAt(Vector3f(0, 20.f, -50.f), Vector3f(0, -2, 0), Vector3f(0, 1, 0));
	Renderer::Call().Set3DViewMatrix(view);

	mShaderGround.ComputeAndLink("ressources/shaders/normal.vs", "ressources/shaders/normal.fs");
	mShader.ComputeAndLink("ressources/shaders/grass.vs", "ressources/shaders/grass.fs");
	mTexture.FromFile("ressources/images/grass.png");
	
	//MakeGround();
	
	mGrassModel = new Grass();
	mGrassModel->SetShaderToUse(&mShader);
	mGrassModel->SetTexture(&mTexture);
	mGrassModel->CreateBuffers();

	for(int i = 0; i < 100; ++i)
	{
		f32 posX = static_cast<f32>(rand() % 40 - 20);
		f32 posZ = static_cast<f32>(rand() % 40 - 20);

		Grass* temp = new Grass(*mGrassModel);
		temp->SetOrigin(temp->GetSize().x/2, 0, 0);
		temp->SetPosition(posX, 0, posZ);
		temp->SetRotation(0, 45, 0);
		temp->SetScale(5, 5, 5);

		Grass* temp2 = new Grass(*mGrassModel);
		temp2->SetOrigin(temp->GetSize().x/2, 0, 0);
		temp2->SetPosition(posX, 0, posZ);
		temp2->SetRotation(0, -45, 0);
		temp2->SetScale(5, 5, 5);

		Grass* temp3 = new Grass(*mGrassModel);
		temp3->SetOrigin(temp->GetSize().x/2, 0, 0);
		temp3->SetPosition(posX, 0, posZ-5);
		temp3->SetScale(5, 5, 5);

		mGrasses.push_back(temp);
		mGrasses.push_back(temp2);
		mGrasses.push_back(temp3);
		
		Add(temp);
		Add(temp2);
		Add(temp3);
	}
}

void GrassSequence::MakeGround()
{
	Vector3f vertices[] = 
	{
		Vector3f(-10.f, 0.f, -10.f),
		Vector3f( 10.f, 0.f, -10.f),
		Vector3f( 10.f, 0.f,  10.f),
		Vector3f(-10.f, 0.f,  10.f)
	};

	Vector3f normals[] =
	{
		Vector3f(0.f, -1.f, 0.f),
		Vector3f(0.f, -1.f, 0.f),
		Vector3f(0.f, -1.f, 0.f),
		Vector3f(0.f, -1.f, 0.f)
	};

	Vector2f textcoords[] =
	{
		Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f),
		Vector2f(1.f, 0.f),
		Vector2f(0.f, 0.f)
	};

	u32 indices[] =
	{
		0, 1, 2,
		2, 0, 3
	};

	std::vector<Vector3f> vVertices(vertices, vertices + sizeof(vertices) / sizeof(Vector3f));
	std::vector<Vector3f> vNormals(normals, normals + sizeof(normals) / sizeof(Vector3f));
	std::vector<Vector2f> vTextCoords(textcoords, textcoords + sizeof(textcoords) / sizeof(Vector2f));
	std::vector<u32> vIndices(indices, indices + sizeof(indices) / sizeof(u32));
	
	mGround = new Mesh(vVertices, vNormals, vTextCoords, vIndices, &mShader);
	Add(mGround);
}

void GrassSequence::Update(u32 dt)
{
	Object::Update(dt);
}

void GrassSequence::ProcessEvents(Input &input)
{
}
