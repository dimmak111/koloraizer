#ifndef H_GRASSSEQUENCE_
#define H_GRASSSEQUENCE_

#include <Asset/Shader.h>
#include <Asset/Texture.h>

#include <Core/Sequence.h>

#include <Math/Vector2.h>

#include <Utils/Timer.h>

class App;
class Mesh;
class Grass;

class GrassSequence : public Sequence
{
public:
	GrassSequence(App* app);
	virtual ~GrassSequence();

	virtual void Init();
	virtual void Update(u32 dt=0);

	void MakeGround();

	virtual void ProcessEvents(Input &input);

private:

	Shader mShaderGround;
	Shader mShader;
	Texture mTexture;

	Grass* mGrassModel;
	Mesh* mGround;
	
	std::vector<Grass*> mGrasses;

	Timer mTimer;
};

#endif