#include "WaterSequence.h"

#include <Renderer/Sprite.h>

#include <SFML/Graphics/Image.hpp>

#include <iostream>
#include <cstdio>

WaterSequence::WaterSequence(App* app) : Sequence(app)
{
}

WaterSequence::~WaterSequence()
{
}

void WaterSequence::Init()
{
	mDefaultShader.ComputeAndLink("ressources/shaders/normal.vs", "ressources/shaders/normal.fs");

	mCamera.Set(Vector3f(0, 5, -15), Vector3f(0, 0, 0), Vector3f(0, 1, 0), GetApp());
	mCube = new Mesh();
	mCube->MakeCube();
	mCube->SetShaderToUse(&mDefaultShader);
	mCube->Scale(2.f, 2.f, 2.f);
	mCube->Move(0,5,0);
	Add(mCube);

	mFBO.Resize(512, 512);
	mFBO.Create();
}

void WaterSequence::Update(u32 dt)
{
	mCamera.ProcessEvents(GetInputManager());
	mCube->Rotate(1, 1, 0);
}

void WaterSequence::ProcessEvents(Input& input)
{
}