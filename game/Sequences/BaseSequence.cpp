#include "BaseSequence.h"

#include <App.h>

#include <iostream>

BaseSequence::BaseSequence(App *app)
: Sequence(app)
{
}

void BaseSequence::Init()
{
	mGeom.MakeCube(2.0f);

	mShader.ComputeAndLink("ressources/shaders/diffuse.vs", "ressources/shaders/diffuse.fs");
	
	mMat.SetShader(&mShader);
	mMat.SetDiffuseColor(Color(0.6f, 0.2f, 0.2f));

	mCube = new Mesh(&mGeom, &mMat);
	Add(mCube);
}


void BaseSequence::Update(u32 dt)
{
	mCube->Rotate(1.0f, 1.0f, 0);
}
