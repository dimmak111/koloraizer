#ifndef H_NOESEQUENCE_
#define H_NOESEQUENCE_

#include <Asset/Shader.h>

#include <Core/Sequence.h>

#include <Renderer/Geometry.h>
#include <Renderer/Materials/DiffuseMaterial.h>
#include <Renderer/Mesh.h>


class App;

class NoeSequence : public Sequence
{
public:
	
	NoeSequence(App* app);

	virtual void Init();
	virtual void Update(u32 dt=0);

private:

	Geometry mGeom;
	DiffuseMaterial mMat;
	Shader mShader;

	Mesh* mChar;
};

#endif