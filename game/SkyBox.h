#ifndef H_SKYBOX_
#define H_SKYBOX_

#include <Asset/TextureCubeMap.h>
#include <Asset/Shader.h>
#include <Renderer/Mesh.h>
#include <Utils/Shared.h>

/* http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=13 
   http://www.keithlantz.net/2011/10/rendering-a-skybox-using-a-cube-map-with-opengl-and-glsl/ */

class SkyBox : public Mesh
{
public:

	SkyBox(	const std::string& pFront, 
			const std::string& pBack, 
			const std::string& pLeft, 
			const std::string& pRight, 
			const std::string& pTop, 
			const std::string& pBottom );

	~SkyBox();

	virtual void Update(u32 dt=0);
	virtual void Render();

private:
	
	Shader mSkyShader;
	TextureCubeMap mCubeMap;
};

#endif