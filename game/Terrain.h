#ifndef H_TERRAIN_
#define H_TERRAIN_

#include <Asset/Shader.h>
#include <Asset/Texture.h>

#include <Math/Vector2.h>

#include <Renderer/FrameBufferObject.h>
#include <Renderer/Mesh.h>
#include <Renderer/Sprite.h>

#include <vector>

/* 
	http://freespace.virgin.net/hugo.elias/models/m_perlin.htm
	http://opengameart.org/ 
	http://www.cplusplus.com/forum/general/85758/
*/

class Input;

struct Chunck
{
	u32 size;
	std::vector<u32> ivecs;
	std::vector<u32> isorted;

	inline u32 Get(u32 i, u32 j) { return ivecs[i*size+j]; }

	void Sort() {

		u32 i,j;

		for(i=0; i<size-1; ++i) {
			for(j=0; j<size-1; ++j) {
				
				u32 i0 = Get(i,j);
				u32 i1 = Get(i, j+1);
				u32 i2 = Get(i+1, j);
				u32 i3 = Get(i+1, j+1);

				isorted.push_back(i0);
				isorted.push_back(i1);
				isorted.push_back(i2);

				isorted.push_back(i2);
				isorted.push_back(i1);
				isorted.push_back(i3);
			}
		}
	}
};

class Terrain : public Object
{

public:
	
	Terrain();
	Terrain(const f32 width, const f32 height, const u32 lod, const u32 chunks);
	~Terrain();

	void Init();
	void InitFBO();

	// inherite from Object class.
	virtual void Update(u32 dt=0)	{ }
	virtual void Render();
	
	void RenderToTexture();
	void RenderTerrain();
	void RenderChuncks();
	
	void CreateGeometry();

	// setters
	void SetSeed(f32 s)				{ mSeed = s; mToUpdate = true; }
	inline f32 GetSeed()			{ return mSeed; } 

private:

	enum
	{
		eMat_Sand = 0,
		eMat_Grass,
		eMat_Rock,
		eMat_Snow,
		eMat_Max
	};

	Vector3f GetVector(u32 x, u32 y);
	inline u32 GetIndex(u32 x, u32 y) const { return x * mLod + y; }

	void ChunckIt();

private:

	

	Shader mShader;
	Shader mFBOShader;

	Shader mDebugShader;
	Sprite* mDebugSprite;

	FrameBufferObject mFBOHeight;
	u32	mFBOVao;

	s32 mLod;
	Vector2f mSize;
	
	f32 mHeighMax;
	f32 mWaterHeight;

	Texture* mHeightTexture;
	std::vector<Texture*> mMaterials;

	u32 mVao;
	u32 mNb;

	f32 mSeed;

	u32 mChunkTesselation;
	std::vector<Chunck> mChunks;

	f32 mPerlinEval;

	bool mToUpdate;
};

#endif