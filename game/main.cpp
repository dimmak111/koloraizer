#include "KoloRaizeR.h"

int main(int argc, char** argv)
{
	KoloRaizeR app;

	app.SetUpdateRate(60);
	app.SetMaxUpdate(10);

	app.Run();
	
	return EXIT_SUCCESS;
}