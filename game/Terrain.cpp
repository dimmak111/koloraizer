#include "Terrain.h"

#include <Core/Input.h>
#include <Core/Frustum.h>
#include <Core/Settings.h>
#include <Math/Rect.h>
#include <Renderer/Renderer.h>

#include <SFML/Graphics/Image.hpp>

#include <iostream>

//#define DEBUG_HEIGHTMAP

Terrain::Terrain()
{
}

Terrain::Terrain(const f32 width, const f32 height, const u32 lod, const u32 chunks)
{
	mLod = lod;
	mSize = Vector2f(width, height);
	mSeed = 2203.f;
	
	mPerlinEval = 2.5f;

	mToUpdate = true;

	mChunkTesselation = chunks;
	mChunks.resize(chunks*chunks);

	Init();
	InitFBO();

#ifdef DEBUG_HEIGHTMAP
	mDebugShader.ComputeAndLink("ressources/shaders/2d.vs", "ressources/shaders/2d.fs");
	mDebugSprite = new Sprite(mHeightTexture, &mDebugShader);
	Add(mDebugSprite);
#endif

}

Terrain::~Terrain()
{
	for(u32 i = 0; i < eMat_Max; ++i)
		delete mMaterials[i];
}

void Terrain::Init()
{	
	// shader used to render
	mShader.ComputeAndLink("ressources/shaders/terrain.vs", "ressources/shaders/terrain.fs", "ressources/shaders/terrain.gs"); 
	mFBOShader.ComputeAndLink("ressources/shaders/heightmap.vs", "ressources/shaders/heightmap.fs");

	// used tiles to texture the map
	mMaterials.resize(eMat_Max, 0);
	for(u32 i = 0; i < eMat_Max; ++i) {
		mMaterials[i] = new Texture();
	}
	
	mMaterials[eMat_Sand]->FromFile("ressources/images/sand_tile.png");
	mMaterials[eMat_Grass]->FromFile("ressources/images/grass_tile.png");
	mMaterials[eMat_Rock]->FromFile("ressources/images/rock_tile.png");
	mMaterials[eMat_Snow]->FromFile("ressources/images/snow_tile.png");

	CreateGeometry();
}

void Terrain::InitFBO()
{
	mFBOHeight.Resize(mLod, mLod);
	mFBOHeight.Create();	

	mHeightTexture = mFBOHeight.GetTexture(0);

	// FBO Geom.
	std::vector<Vector3f> vertices(4);
	std::vector<u32> indices(6);

	vertices[0] = Vector3f(0.f,		0.f,	0.f);
	vertices[1] = Vector3f(mLod,	0.f,	0.f);
	vertices[2] = Vector3f(0.f,		mLod,	0.f);
	vertices[3] = Vector3f(mLod,	mLod,	0.f);

	indices[0] = 2;
	indices[1] = 1;
	indices[2] = 0;

	indices[3] = 1;
	indices[4] = 2;
	indices[5] = 3;
	
	u32 vbo, ibo;

	glGenVertexArrays(1, &mFBOVao);
	glBindVertexArray(mFBOVao);

	glGenBuffers(1,	&vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vector3f), &(vertices[0]), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), 0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1,	&ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(u32), &(indices[0]), GL_STATIC_DRAW);

	glBindVertexArray(0);
}

void Terrain::CreateGeometry()
{
	// Terrain Geom.
	std::cout << "Generating Terrain..." << eol;

	std::vector<Vector3f> vertices;
	std::vector<u32> indices;

	for(s32 i = 0; i < mLod; ++i){
		for(s32 j = 0; j < mLod; ++j){
			vertices.push_back(GetVector(i, j));
		}
	}
	/*
	s32 inChunk = mLod / mChunkTesselation;
	
	u32 i,j;
	for(i = 0; i < mLod; ++i) {
		for(j = 0; j < mLod; ++j) {
			
			u32 cx = i / inChunk;
			u32 cy = j / inChunk;
			
			mChunks[cx*mChunkTesselation+cy].ivecs.push_back(GetIndex(i,j));

			if(cx == 0 && cy == mChunkTesselation - 1) {
				mChunks[cx*mChunkTesselation+cy].ivecs.push_back(GetIndex(i,j));
				mChunks[cx*mChunkTesselation+cy].ivecs.push_back(GetIndex(i,j+1));
				mChunks[cx*mChunkTesselation+cy].ivecs.push_back(GetIndex(i+1,j));
			}
		}
	}

	
	for(i = 0; i < mChunks.size(); ++i) {
		
		mChunks[i].size = Math::Sqrt(mChunks[i].ivecs.size());
		mChunks[i].Sort();
		
		indices.insert(indices.end(), mChunks[i].isorted.begin(), mChunks[i].isorted.begin()+mChunks[i].isorted.size());
	}*/
	
	
	s32 n = mLod;
	for(s32 i = 0; i < n-1; ++i) {
		for(s32 j = 0; j < n-1; ++j) {

			u32 i0 = GetIndex(i, j);
			u32 i1 = GetIndex(i+1, j);
			u32 i2 = GetIndex(i, j+1);
			u32 i3 = GetIndex(i+1, j+1);
				
			indices.push_back(i0);
			indices.push_back(i2);
			indices.push_back(i3);
			
			indices.push_back(i3);
			indices.push_back(i1);
			indices.push_back(i0);

			//std::cout <<i0<<" "<<i1<<" "<<i2<<" "<<i3<<eol;
		}
	}

	std::cout << "Vertices: " << vertices.size() << eol;
	//std::cout << "Triangles: " << indices.size() / 3 << eol; 

	u32 vbo, ibo;
	
	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);

	glGenBuffers(1,	&vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vector3f), &(vertices[0]), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), 0);
	
	glGenBuffers(1,	&ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(u32), &(indices[0]), GL_STATIC_DRAW);

	glBindVertexArray(0);
	
	mNb = indices.size();
}

f32 pixels[120*120];

void Terrain::ChunckIt() 
{
	/*
	s32 nbChunks = mLod / mChunkTesselation;

	mFBOHeight.Bind();
		glReadPixels(0, 0, mLod, mLod, GL_RED, GL_FLOAT, &pixels);
	mFBOHeight.UnBind();
	
	for(int i = 0; i < mLod; ++i) {
		for(int j = 0; j < mLod; ++j) {
			
			u32 ichunkx = ((f32)i / (f32)mLod) * ((f32)nbChunks);
			u32 ichunky = ((f32)j / (f32)mLod) * ((f32)nbChunks);
		}
	}
	*/
}

Vector3f Terrain::GetVector(u32 x, u32 y)
{
	f32 nx = (float)(x * mSize.x) / (float)(mLod-1.0);
	f32 nz = (float)(y * mSize.y) / (float)(mLod-1.0);
	return Vector3f(nx, 0.f, nz);
}

void Terrain::Render()
{
	if(mToUpdate) {
		RenderToTexture();
		ChunckIt();
		glViewport(0, 0, Settings::Call().GetSettingInt("WindowWidth"), Settings::Call().GetSettingInt("WindowHeight"));
		mToUpdate = false;
	}
	
#ifdef DEBUG_HEIGHTMAP
	Object::Render();
#endif

	RenderTerrain();
}

void Terrain::RenderToTexture()
{
	mFBOHeight.Bind();
	mFBOHeight.ResizeViewPort();
	mFBOHeight.Clear();
	
		Matrix4 QuadOrtho;
		QuadOrtho.MakeOrtho(0,mLod,0,mLod); 
	
		glBindVertexArray(mFBOVao);
		mFBOShader.Bind();

			mFBOShader.SendMatrix4("OrthoProj", QuadOrtho);
			mFBOShader.SendFloat("Seed", mSeed);
			mFBOShader.SendFloat("LOD", mLod);
			mFBOShader.SendFloat("TexLOD", mPerlinEval);
			mFBOShader.SendVector2("Size", mSize);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		mFBOShader.UnBind();
		glBindVertexArray(0);
	mFBOHeight.UnBind();
}

Frustum frustum;

void Terrain::RenderTerrain()
{
	//Renderer::Call().SetRenderMode(Renderer::RENDERMODE_WIREFRAME);
	Matrix4 Projection		= Renderer::Call().Get3DProjectionMatrix();
	Matrix4 View			= Renderer::Call().Get3DViewMatrix();
	Matrix4 Model			= GetWorldTransform();
	Matrix4 ModelView		= View * Model;
	Matrix4 MVP				= Projection * ModelView;
	
	mShader.Bind();
		glBindVertexArray(mVao);
			glEnableVertexAttribArray(0);
			
				mShader.SendMatrix4("Projection", Projection);
				mShader.SendMatrix4("View", View);
				mShader.SendMatrix4("Model", Model);
				mShader.SendMatrix4("ModelView", ModelView);
				mShader.SendMatrix4("MVP", MVP);

				mShader.SendVector2("Size", mSize);
				mShader.SendFloat("LOD", static_cast<f32>(mLod));
				mShader.SendFloat("TexLOD", 2.5);
				
				// HeightMap
				glActiveTexture(GL_TEXTURE0);
				mHeightTexture->Bind();
				mShader.SendTexture("HeightSampler", 0);
				
				// Sand Material 
				glActiveTexture(GL_TEXTURE1);
				mMaterials[eMat_Sand]->Bind();
				mShader.SendTexture("SandSampler", 1);
				
				glActiveTexture(GL_TEXTURE2);
				mMaterials[eMat_Grass]->Bind();
				mShader.SendTexture("GrassSampler", 2);

				glActiveTexture(GL_TEXTURE3);
				mMaterials[eMat_Rock]->Bind();
				mShader.SendTexture("RockSampler", 3);

				glActiveTexture(GL_TEXTURE4);
				mMaterials[eMat_Snow]->Bind();
				mShader.SendTexture("SnowSampler", 4);

				glDrawElements(GL_TRIANGLES, mNb, GL_UNSIGNED_INT, 0);
/*
				for(u32 i = 0; i < mChunks.size(); ++i) {
					glDrawElements(GL_TRIANGLES, mChunks[i].isorted.size(), GL_UNSIGNED_INT, (void*)(i*mChunks[i].isorted.size()*sizeof(u32)));
				}*/
								/*
				for(u32 i = 0; i < mChunks.size(); ++i) {

					//glDrawElements(GL_TRIANGLES, 11, GL_UNSIGNED_INT, (void*)((min+max)*sizeof(u32)));	
				}*/
						//if(frustum.IsBoxInside(MVP, vmin, vmax)) {
						//glDrawElements(GL_TRIANGLES, mChunks[i].iMin+mChunks[i].iMax, GL_UNSIGNED_INT, (void*)((mChunks[ichunck].iMin+mChunks[ichunck].iMax)*sizeof(u32)));	
					//}
				//}
			
			glDisableVertexAttribArray(0);
		glBindVertexArray(0);
	mShader.UnBind();
	//Renderer::Call().SetRenderMode(Renderer::RENDERMODE_FILL);
}

void Terrain::RenderChuncks() 
{

}