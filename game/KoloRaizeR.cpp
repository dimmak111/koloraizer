#include "KoloRaizeR.h"

#include <Asset/Shader.h>
#include <Math/Matrix4.h>
#include <Renderer/Renderer.h>
#include <Core/Settings.h>
#include <Asset/RessourceManager.h>

#include "Sequences/BaseSequence.h"


KoloRaizeR::KoloRaizeR()
{
}

KoloRaizeR::~KoloRaizeR()
{
}

void KoloRaizeR::Init()
{
	// 2D Ortho
	Matrix4 view2D;
	view2D.MakeOrtho(0, GetWindow().GetWidth(), 0, GetWindow().GetHeight());
	Renderer::Call().Set2DProjectionMatrix(view2D);
	
	// 3D Projection
	Matrix4 projection3D;
	projection3D.MakePerspective(45.f, (640.f / 480.f), 1.f, 1000.f);
	Renderer::Call().Set3DProjectionMatrix(projection3D);
	
	// 3D View
	Matrix4 view;
	view.LookAt(Vector3f(0, 0, -5.f), Vector3f(0, 0, 0), Vector3f(0, 1, 0));
	Renderer::Call().Set3DViewMatrix(view);
	
	//std::cout << Settings::Call().GetSettingString("AssetsFolder") << eol;	
}

void KoloRaizeR::RegisterSequences(SequenceManager* sm)
{
	sm->RegisterSequence("BaseSequence", new BaseSequence(this));
	//sm->RegisterSequence("GlowSequence", new GlowSequence(this));
	//sm->RegisterSequence("GrassSequence", new GrassSequence(this));
	//sm->RegisterSequence("WaterSequence", new WaterSequence(this));
	//sm->RegisterSequence("TerrainSequence", new TerrainSequence(this));
	sm->SetCurSequence("BaseSequence");
}

void KoloRaizeR::ReleaseAndCleanUp()
{
}

void KoloRaizeR::ProcessCustomEvents(Input& input)		
{
	if(input.IsKeyDown(Key_Escape)) {
		GetWindow().Close();
	}
}