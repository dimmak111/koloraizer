#include "SkyBox.h"

#include <Renderer/Renderer.h>

#include <iostream>

SkyBox::SkyBox(	const std::string& pFront, 
				const std::string& pBack, 
				const std::string& pLeft, 
				const std::string& pRight, 
				const std::string& pTop, 
				const std::string& pBottom )
{
	mSkyShader.ComputeAndLink("ressources/shaders/skybox.vs", "ressources/shaders/skybox.fs");
	SetShaderToUse(&mSkyShader);
	mCubeMap.FromFiles(pFront, pBack, pLeft, pRight, pTop, pBottom);
	MakeCube();
}

SkyBox::~SkyBox()
{
}

void SkyBox::Update(u32 dt)
{
}

void SkyBox::Render()
{
	glCullFace(GL_FRONT);
	glDepthFunc(GL_LEQUAL);

	Matrix4 P = Renderer::Call().Get3DProjectionMatrix();
	Matrix4 V = Renderer::Call().Get3DViewMatrix();
	Matrix4 VInv = V;
	Matrix4 MVP = P * V * GetWorldTransform();

	VInv.Inverse();

	//Renderer::Call().DisableDepthMask();
	//glDisable(GL_DEPTH_TEST);
	{
		glBindVertexArray(mVao);
		{
			glEnableVertexAttribArray(0);
			GetShaderUsed()->Bind();
			{
				GetShaderUsed()->SendMatrix4("MVP", MVP);
				GetShaderUsed()->SendVector3("Pos", VInv.GetTranslation());
		
				glActiveTexture(GL_TEXTURE0);
				mCubeMap.Bind();
				GetShaderUsed()->SendTexture("CubeMap", 0);

				glDrawElements(GL_TRIANGLES, mNbIndices, GL_UNSIGNED_INT, 0);
			}
			GetShaderUsed()->UnBind();
			glDisableVertexAttribArray(0);
		}
		glBindVertexArray(0);
	}
	//glEnable(GL_DEPTH_TEST);
	//Renderer::Call().EnableDepthMask();
	glDepthFunc(GL_LESS);
	glCullFace(GL_BACK);
}