#ifndef H_KOLORAIZER_
#define H_KOLORAIZER_

#include <App.h>

class KoloRaizeR : public App
{
public:

	KoloRaizeR();
	virtual ~KoloRaizeR();

	virtual void Init();

	virtual void RegisterSequences(SequenceManager* sm);
	virtual void ReleaseAndCleanUp();

	virtual void ProcessCustomEvents(Input& input);
};

#endif

