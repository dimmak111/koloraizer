#include "Grass.h"
#include <Asset/Shader.h>

#include <iostream>
#include <cstdio>

Grass::Grass(const Grass& g) : Mesh(), mDelta(0), mElapsedTime(0)
{
	mVao = g.mVao;
	mVbo = g.mVbo;
	mTbo = g.mTbo;
	mIbo = g.mIbo;

	mShader = g.mShader;
	mTexture = g.mTexture;


	mNbIndices = g.mNbIndices;

	SetSize(g.GetSize());

	mSpeed = static_cast<f32>(rand() % 10 + 1);
}

Grass::Grass() : mElapsedTime(0)
{
	mSpeed = static_cast<f32>(rand() % 2 + 1);

	Vector3f vertices[] = 
	{
		Vector3f(-0.5f, -0.5f, 0.f),
		Vector3f( 0.5f, -0.5f, 0.f),
		Vector3f( 0.5f,  0.5f, 0.f),
		Vector3f(-0.5f,  0.5f, 0.f)
	};

	Vector3f normals[] =
	{
		Vector3f(0.f, -1.f, 0.f),
		Vector3f(0.f, -1.f, 0.f),
		Vector3f(0.f, -1.f, 0.f),
		Vector3f(0.f, -1.f, 0.f)
	};

	Vector2f textcoords[] =
	{
		Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f),
		Vector2f(1.f, 0.f),
		Vector2f(0.f, 0.f)
	};

	u32 indices[] =
	{
		0, 1, 2,
		2, 0, 3
	};

	mVertices = std::vector<Vector3f>(vertices, vertices + sizeof(vertices) / sizeof(Vector3f));
	mNormals = std::vector<Vector3f>(normals, normals + sizeof(normals) / sizeof(Vector3f));
	mTextCoord = std::vector<Vector2f>(textcoords, textcoords + sizeof(textcoords) / sizeof(Vector2f));
	mIndices = std::vector<u32>(indices, indices + sizeof(indices) / sizeof(u32));
}

void Grass::Update(u32 dt)
{
	mElapsedTime += dt;
	mDelta += dt;
}

void Grass::PreRender()
{
	GetShaderUsed()->SendFloat("WindDirX", mWindDirX);
	GetShaderUsed()->SendFloat("WindDirZ", mWindDirZ);
	GetShaderUsed()->SendFloat("Speed", mSpeed/10);
	GetShaderUsed()->SendFloat("Time", static_cast<f32>(mElapsedTime));
	GetShaderUsed()->SendFloat("LoopTime", 7500);
}

void Grass::PostRender()
{
}