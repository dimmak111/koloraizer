#ifndef H_BRUSH_
#define H_BRUSH_

#include <vector>

#include <Math/Vector2.h>

#define BRUSH_INNER_RADIUS	0
#define BRUSH_OUTER_RADIUS	1
#define BRUSH_HEAD_TYPE		2
#define BRUSH_VERTEX_SPACING 3

#define	BRUSH_HEAD_AIRWAVE	0
#define BRUSH_HEAD_SPATIAL	1


/*
*	Ground brush
*	based on: http://www.decarpentier.nl/scape-brush-pipeline
*/
class Brush {
public:
	Brush();
	~Brush();

	void SetParameter(const int name, const int value);
	void SetParameter(const int name, const float value);
	void BeginStroke( const Vector2f& at);
	void PushPoint( const Vector2f& point, const float delta_time = 0.1f );
	void EndStroke( const float delta_time = 0.1f);

private:
	struct Point {
		Vector2f position;
		float intensity;
		Point(const Vector2f& at = Vector2f(0.f, 0.f), float ii = 1.f) : position(at), intensity(ii) {};
	};

	float	mInnerRad;
	float	mOuterRad;
	float	mVertexSpacing;
	int		mHeadType;

	std::vector<Point>mStrokePoints;


	void __pushPoint( const Vector2f& p );
};

#endif