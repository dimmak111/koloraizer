project "game"
	kind "ConsoleApp"
	language "C++"
	includedirs { ".", "../engine/include", "../engine/include/Extlibs" }
	files  { "**.h", "**.cpp" }

	configuration { "Debug" }
	targetdir "../bin"
	objdir "../bin/Objects"
    defines { "_DEBUG", "DEBUG" }
    flags   { "Symbols" }
    links { "engine" }

    configuration { "Release" }
    targetdir "../bin"
    objdir "../bin/Objects"
    defines { "NDEBUG" }
    flags   { "Optimize" }
    links { "engine" }