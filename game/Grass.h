#ifndef H_GRASS_
#define H_GRASS_

#include <Renderer/Mesh.h>

class Grass : public Mesh
{
public:

	Grass();
	Grass(const Grass& grass);
	virtual ~Grass()				{ }

	virtual void Update(u32 dt);
	
	//void ToggleWind()				{ mWindDir = -mWindDir; }

	virtual void PreRender();
	virtual void PostRender();

private:
	
	u32 mDelta;
	u32 mElapsedTime;
	f32 mWindDirX;
	f32 mWindDirZ;
	f32 mSpeed;
};

#endif