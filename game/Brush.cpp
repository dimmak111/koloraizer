#include <Math/Mathf.h>
#include "Brush.h"


#define choice(x,y, elsez) x == y ? y : elsez;


Brush::Brush() :
mInnerRad(0.1f),
mOuterRad(0.1f),
mHeadType(BRUSH_HEAD_AIRWAVE)
{}


Brush::~Brush() 
{}


void Brush::SetParameter(const int name, const float value) {
	switch(name) {
		case BRUSH_INNER_RADIUS: mInnerRad = Math::Max(0.1f, value); break;
		case BRUSH_OUTER_RADIUS: mOuterRad = Math::Max(mInnerRad, value); break;
		case BRUSH_VERTEX_SPACING: mVertexSpacing = Math::Max(0.01f, value); break;
	}

}
void Brush::SetParameter(const int name, const int value) {
	switch(name) {
		default: SetParameter(name, (float) value); break;
		case BRUSH_HEAD_TYPE: mHeadType = choice(value, BRUSH_HEAD_AIRWAVE, BRUSH_HEAD_SPATIAL); break;
	}
}

void Brush::BeginStroke(const Vector2f &at) {
	mStrokePoints.clear();
	//mStrokePoints.reserve(16);
	__pushPoint(at);
}

void Brush::PushPoint(const Vector2f &point, const float delta_time) {
	Point& last = mStrokePoints.back();

	if ( mHeadType == BRUSH_HEAD_AIRWAVE ) {
		last.intensity += delta_time;
	}

	if ( ( point -  last.position).Length() < mVertexSpacing ) {
		return;
	}
	__pushPoint(point);
}

void Brush::EndStroke(const float delta_time) {
	if ( mHeadType == BRUSH_HEAD_AIRWAVE ) {
		mStrokePoints.back().intensity = delta_time;
	}

	//TODO process points:
	// -> create spline
	// -> sample spline (from a vertex array)
	// -> weight samples (by interpolated intensity)
	// -> 
}


/*
*	What is a sample
*		-> a collection of weighted arrays
*		-> the evaluation of a partial function between 2 control point
*		
*
*
*
*/

void Brush::__pushPoint( const Vector2f& pos ) {
	Point p(pos);

	if ( mHeadType == BRUSH_HEAD_AIRWAVE ) {
		p.intensity = 0.f;
	}
	mStrokePoints.push_back(p);
}