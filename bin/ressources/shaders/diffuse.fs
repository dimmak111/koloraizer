#version 330

uniform vec4 DiffuseColor;

out vec4 FragColor;

void main()
{
	FragColor = DiffuseColor;
}