#version 330

flat in int isNormal;
in vec2 gTexCoord;

out vec4 FragColor;
void main() {
	vec4 color = vec4(gTexCoord, 0.0, 1.0);
	
	if ( isNormal == 1 )
		color = vec4(1.0, 0.0, 0.0, 0.2);
	FragColor = color;
}