#version 330

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Normal;
layout(location = 2) in vec2 in_TexCoord;

uniform mat4 MVP;
uniform mat4 Model;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoord;

void main() {
	mat3 NormalMat = transpose( inverse( mat3(Model)));
	vec4 position = vec4(in_Position, 1.0);
	
	// output
	gl_Position = position;
	vPosition = position.xyz;
	vNormal =  in_Normal;
	vTexCoord = in_TexCoord;
}