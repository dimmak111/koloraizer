#version 330

in vec3 ex_Color;
in vec2 ex_TextCoord;

uniform float Textured;
uniform sampler2D Texture;

out vec4 FragColor;

void main()
{
	vec4 color;

	if(Textured >= 1.0)
		color = texture2D(Texture, ex_TextCoord.st);
	else
		color = vec4(1.0);

	if(color.a < 0.9)
		discard;

	FragColor = color;
}