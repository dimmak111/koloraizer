#version 330

precision highp float;

in vec3 gPosition;
in vec2 gTextCoord;
in vec4 gNormal;
in vec4 gLightDir;
flat in int gColorize;

uniform sampler2D HeightSampler;

uniform vec2 Size;
uniform float LOD;
uniform float TexLOD;

// materials
uniform sampler2D SandSampler;
uniform sampler2D GrassSampler;
uniform sampler2D RockSampler;
uniform sampler2D SnowSampler;

out vec4 FragColor;

const float Height = 100.0;
float step = 1.0/LOD;

vec4 processColor(float height, float slope)
{
	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
	height = clamp(height, 0.0, 1.0);

	vec4 colorSand 		= texture(SandSampler, gTextCoord.st);
	vec4 colorGrass 	= texture(GrassSampler, gTextCoord.st);
	vec4 colorRock 		= texture(RockSampler, gTextCoord.st);
	vec4 colorSnow 		= texture(SnowSampler, gTextCoord.st);

	float range = 0.0;
	float weight = 0.0;

	if(height <= 0.25)
	{
		range = 0.25;
		weight = 1.0 - max(0.0, (range - abs(height - 0.25)) / range);
		color += weight * colorSand;
	}

	range = 0.50 - 0.00;
	weight = max(0.0, (range - abs(height - 0.50)) / range);
	color += weight * colorGrass;
   
	range = 0.95 - 0.50;
	weight = max(0.0, (range - abs(height - 0.95)) / range);
	color += weight * colorRock;

	range = 1.00 - 0.95;
	weight = max(0.0, (range - abs(height - 1.00)) / range);
	color += weight * colorSnow;

   	return color;
}

void main()
{
	// normalized height and slope
	float height = gPosition.y / Height;
	float slope = 1.0 - gNormal.y;

	vec4 color = vec4(processColor(height, slope).xyzw);
	vec4 ambient = vec4(0.45, 0.45, 0.45, 1.0);
	vec4 diffuse = vec4(0.85, 0.85, 0.85, 1.0);

	//ambient compute
	FragColor = color*ambient;

	//diffuse
	float intensity = max(dot(gNormal, gLightDir), 0.0);
	if(intensity > 0)
	{
		FragColor += color * (diffuse*intensity);
	}

	// normal display
	//FragColor = vec4(gNormal.xyz, 1.0);

	//slope display
	//FragColor = vec4(slope, slope, slope, 1.0);

	// height display
	//FragColor = vec4(vec3(height), 1.0);

	if(gColorize == 1)
		FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}