#version 330

layout(location = 0)in vec3 in_Position;
layout(location = 1)in vec3 in_Normal;
layout(location = 2)in vec2 in_TextCoord;
layout(location = 3)in vec3 in_Color;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;
uniform mat4 ModelView;
uniform mat4 MVP;

out vec3 ex_Color;
out vec2 ex_TextCoord;
out vec3 ex_Normal;

void main()
{
	gl_Position = MVP * vec4(in_Position, 1.0); 
}