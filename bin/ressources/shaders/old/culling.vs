#version 330

layout(location = 0)in vec3 in_Position;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;

void main(void)
{
	gl_Position = Projection * View * Model * vec4(in_Position, 0.0);	
}