#version 330

layout(location = 0)in vec3 in_Position;

uniform mat4 MVPMat;

out vec2 varyingTexCoord;

float step = 1.0 / 512.0;

void main() {
	vec3 position = in_Position * step;
	varyingTexCoord = position.xy;
	gl_Position = MVPMat * vec4(in_Position, 1.0);
}
