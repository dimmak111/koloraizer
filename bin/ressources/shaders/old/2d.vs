#version 330

layout(location = 0)in vec3 in_Position;
layout(location = 1)in vec2 in_TextCoord;

uniform mat4 View;
uniform mat4 Model;

out vec2 ex_TextCoord;

void main()
{
	vec3 final = vec3(in_Position.x, in_Position.y, 0.0);
		
	gl_Position = Model * vec4(final, 1.0); 
	ex_TextCoord = in_TextCoord;
}