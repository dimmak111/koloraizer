#version 330

in vec2 varyingTexCoord;
uniform sampler2D texturemap;
out vec4 fragment;

float step = 1.0 / 512.0;

int isWhite( vec3 frag )  {
	if ( frag.r == 1.0 && frag.g == 1.0 && frag.b == 1.0 )
		return 1;
	else
		return 0;
}	
void main() {
	vec3 right = texture(texturemap, varyingTexCoord + vec2(step, 0)).rgb;
	vec3 left = texture(texturemap, varyingTexCoord + vec2(-step, 0)).rgb;
	vec3 down = texture(texturemap, varyingTexCoord + vec2(0, -step)).rgb;
	vec3 up = texture(texturemap, varyingTexCoord + vec2(0, step)).rgb;
	
	vec3 tmpFrag = texture(texturemap, varyingTexCoord).rgb;
	
	
	if ( isWhite(tmpFrag) == 1 ) {
		if ( isWhite(right) == 0 )
			fragment = vec4(1.0, 0.0, 0.0, 1.0);
		else if ( isWhite(left) == 0 )
			fragment = vec4(1.0, 0.0, 0.0, 1.0);
		else if ( isWhite(up) == 0 )
			fragment = vec4(1.0, 0.0, 0.0, 1.0);
		else if ( isWhite(down) == 0 )
			fragment = vec4(1.0, 0.0, 0.0, 1.0);			
		else
			fragment = vec4(0.0);
		
	}
	else {
		fragment = vec4(tmpFrag, 1.0);
	}
}
