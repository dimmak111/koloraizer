#version 330

in vec3 ex_TextCoord;

uniform samplerCube CubeMap;

out vec4 FragColor;

void main(void)
{
	FragColor = texture(CubeMap, ex_TextCoord);
}