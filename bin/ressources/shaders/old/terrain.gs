#version 330

layout(triangles) in;
layout(triangle_strip) out;
layout(max_vertices = 3) out;

in vec3 vPosition[3];
in vec2 vTextCoord[3];
in vec4 vNormal[3];
in vec4 vLightDir[3];
in vec4 vInstance[3];

uniform mat4 MVP;

out vec3 gPosition;
out vec2 gTextCoord;
out vec4 gNormal;
out vec4 gLightDir;
flat out int gColorize;

void main()
{	
	//vec3 center = vec3(0.0);

	for(int i=0; i < gl_in.length(); i++)
  	{
  		gPosition 		= vPosition[i];
  		gTextCoord 		= vTextCoord[i];
  		gNormal 		= vNormal[i];
  		gLightDir 		= vLightDir[i];
  		gColorize		= 0;
    	gl_Position 	= gl_in[i].gl_Position;

    	//center += vPosition[i];

    	EmitVertex();
  	}

  	EndPrimitive();

  	/*
  	center /= 3.0;

 	vec3 U = vPosition[1].xyz - vPosition[0].xyz;
 	vec3 V = vPosition[2].xyz - vPosition[0].xyz;
  	vec3 N = normalize(cross(U, V));

  	gColorize = 1;
  	gl_Position = MVP * vec4(center, 1.0);
  	EmitVertex();

  	gColorize = 1;
  	gl_Position = MVP * vec4(center + (N * 4.0), 1.0);
  	EmitVertex();

  	EndPrimitive();*/
} 