#version 330

layout(location = 0)in vec3 in_Vertex;

uniform mat4 MVP;
uniform vec3 Pos;

out vec3 ex_TextCoord;

void main(void)
{
	vec4 v =  MVP * vec4(in_Vertex + Pos, 1.0);
	gl_Position = v.xyww;
	ex_TextCoord = in_Vertex;
}