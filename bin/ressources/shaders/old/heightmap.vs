#version 330

layout(location = 0)in vec3 in_Position;

uniform mat4 OrthoProj;

void main(void)
{
	// output
	gl_Position = OrthoProj * vec4(in_Position, 1.0); 
}
