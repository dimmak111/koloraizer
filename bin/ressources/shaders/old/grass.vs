#version 330

layout(location = 0)in vec3 in_Position;
layout(location = 1)in vec3 in_Normal;
layout(location = 2)in vec2 in_TextCoord;
//layout(location = 3)in vec3 in_Color;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;
uniform mat4 ModelView;
uniform mat4 MVP;

uniform float Time;
uniform float LoopTime;

uniform float WindDirX;
uniform float WindDirZ;
uniform float Speed;

//out vec3 ex_Color;
out vec2 ex_TextCoord;

void main()
{
	vec3 Movement = vec3(0.0);

	float curTime = mod(Time, LoopTime);
	float curLerp = curTime / LoopTime;

	float cosw = cos(mix(0, 3.14*2, curLerp));
	float sinw = sin(mix(0, 3.14*2, curLerp));

	if(in_TextCoord.t < 0.1)
	{
		Movement.x = cosw * 0.05;
		Movement.z = sinw * 0.05;
	}

	gl_Position = MVP * vec4(in_Position + Movement, 1.0); 
	
	//ex_Color = in_Color;
	ex_TextCoord = in_TextCoord;
}