#version 330

layout(location = 0)in vec3 in_Position;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;
uniform mat4 ModelView;
uniform mat4 MVP;

uniform sampler2D HeightSampler;

uniform vec2 Size;
uniform float LOD;
uniform float TexLOD;

out vec3 vPosition;
out vec2 vTextCoord;
out vec4 vNormal;
out vec4 vLightDir;

const float Height = 100.0;
float step = 1.0/LOD;

// calculate normal for a face
vec3 faceNormal(vec3 v1, vec3 v2, vec3 v3)
{
	vec3 U = normalize(v2 - v1);
	vec3 V = normalize(v3 - v1);
	
	return normalize(cross(V, U));
}

// smooth normal of each adjacent face
vec3 smoothNormal(vec2 texCoord)
{
	vec2 origin = texCoord;
	vec3 p = in_Position;

	//vec2 origin = vec2(p.x*step, p.z*step);	
	vec3 sum = vec3(0.0);

	float h0 = texture(HeightSampler, origin).r;							// Center
	float h1 = texture(HeightSampler, origin+vec2(-step, 	0.0)).r;		// Left
	float h2 = texture(HeightSampler, origin+vec2(  0.0, -step)).r;			// Top 
	float h3 = texture(HeightSampler, origin+vec2( step, -step)).r;			// Top Right
	float h4 = texture(HeightSampler, origin+vec2( step,   0.0)).r;			// Right
	float h5 = texture(HeightSampler, origin+vec2(  0.0,  step)).r;			// Bottom
	float h6 = texture(HeightSampler, origin+vec2( -step, step)).r;			// Bottom Left

	vec3 v0 = vec3(p.x, 	h0*Height,  p.z);
	vec3 v1 = vec3(p.x-1.0, h1*Height,  p.z);
	vec3 v2 = vec3(p.x,     h2*Height,  p.z-1.0);
	vec3 v3 = vec3(p.x+1.0, h3*Height,  p.z-1.0);
	vec3 v4 = vec3(p.x+1.0, h4*Height,  p.z);
	vec3 v5 = vec3(p.x,     h5*Height,  p.z+1.0);
	vec3 v6 = vec3(p.x-1.0, h6*Height,  p.z+1.0);

	sum += faceNormal(v2, v0, v1);
	sum += faceNormal(v0, v2, v3);
	sum += faceNormal(v3, v4, v0);
	sum += faceNormal(v5, v0, v4);	
	sum += faceNormal(v6, v0, v5);
	sum += faceNormal(v6, v1, v0);

	return normalize(sum);
}

void main()
{
	// Light
	vec4 lightPos = vec4(100.0, 150.0, 200.0, 1.0);
	mat4 NormalMatrix = transpose(inverse(Model));

	// Texture to World
	vec3 pos = in_Position;
	vec2 texCoords = (pos.xz / Size.xy);

	texCoords.x = clamp(texCoords.x, 0.0, 1.0);
	texCoords.y = clamp(texCoords.y, 0.0, 1.0);

	// Compute final position
	float h = texture(HeightSampler, texCoords.st).x;
	vec4 final = vec4(in_Position.x, h*Height, in_Position.z, 1.0);

	gl_Position = MVP * final;

	// Fragment
	vPosition = vec3(final);
	vTextCoord = texCoords * 5.0;
	vNormal = NormalMatrix * vec4(smoothNormal(texCoords), 1.0);
	vLightDir = normalize(lightPos - final);
}
