#version 330

layout(triangles) in;
layout(line_strip) out;
layout(max_vertices = 6) out;

in vec3 vPosition[3];
in vec3 vNormal[3];
in vec2 vTexCoord[3];

uniform mat4 Projection;
uniform mat4 MVP;

flat out int isNormal;
out vec2 gTexCoord;

void main() {
	int i;
	vec4 center = vec4(0.0);
	vec3 normal = vec3(0.0);
	// a->b->c->a
	for( i = 0; i < 4; ++i) {
		int indice = i % 3;
		if ( i < 3 ) {
			center += gl_in[indice].gl_Position;
			normal += vNormal[indice];
		}
		gl_Position = MVP * gl_in[indice].gl_Position;
		isNormal = 0;
		gTexCoord = vTexCoord[indice];
		EmitVertex();
	}
	EndPrimitive();
	
	center /= 3;
	normal /= 3;
	
	gl_Position = MVP * center;
	isNormal = 1;
	gTexCoord = vec2(0.0);
	EmitVertex();
	gl_Position = MVP * (center + vec4(normal,0.0) * 2);
	isNormal = 1;
	gTexCoord = vec2(0.0);
	EmitVertex();
	
	EndPrimitive();
}