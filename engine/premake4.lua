project "engine"
	kind "SharedLib"
	language "C++"
	includedirs { ".", "include", "include/Extlibs" }
	files  { "**.h", "**.cpp", "**.inl" }

	configuration { "Debug" }
	targetdir "../bin"
	objdir "../bin/Objects"
    defines { "_DEBUG", "DEBUG", "SFEXPORTS" }
    flags   { "Symbols" }
    links { "glew32", "opengl32", "sfml-graphics-d", "sfml-system-d", "sfml-window-d", "assimp-d" }

    configuration { "Release" }
    targetdir "../bin"
    objdir "../bin/Objects"
    defines { "NDEBUG" }
    flags   { "Optimize" }
    links { "glew32", "opengl32", "sfml-graphics", "sfml-system", "sfml-window", "assimp" }