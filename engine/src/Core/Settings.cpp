#include "Core/Settings.h"
#include "Debug/LogManager.h"

const std::string Settings::WINDOW_TITLE	= "WindowTitle";
const std::string Settings::WINDOW_WIDTH	= "WindowWidth";
const std::string Settings::WINDOW_HEIGHT	= "WindowHeight";
const std::string Settings::ASSET_FOLDER	= "AssetFolder";

Settings::Settings() : mParsed(false)
{
}

Settings::~Settings()
{
}

Setting Settings::SplitRawSetting(const std::string& set)
{
	std::string key;
	std::string value;

	size_t pos = set.find_first_of("=");

	key = set.substr(0,pos);
	value = set.substr(pos+1);

	return Setting(key, value);
}

void Settings::Parse()
{	
	if(mParsed)
		return;

	mParsed = true;

	mSettingFile.Open(defaultSettings);
	if(mSettingFile.IsOpened())
	{
		while(!mSettingFile.End())
		{
			AddSetting(SplitRawSetting(mSettingFile.GetLine()));
		}
		mSettingFile.Close();

	} else {

		// make default settings file.
	}
}

void Settings::AddSetting(const Setting& set)
{
	if(!set.Key.empty() && !set.Value.empty())
		mSettings.push_back(set);
}	


const std::string Settings::GetSettingString(const std::string& key)
{
	return Settings::Call().GetSetting(key);
}

const s32 Settings::GetSettingInt(const std::string& key)
{
	return std::atoi(Settings::Call().GetSetting(key).c_str());
}

const std::string Settings::GetSetting(const std::string& key)
{
	std::vector<Setting>::iterator it;
	for(it = mSettings.begin(); it != mSettings.end(); ++it)
	{
		if((*it).Key == key)
			return (*it).Value;
	}


	LogManager::Call().Warning("Setting " + key + " not found in Setting.cfg");
	return "";
}