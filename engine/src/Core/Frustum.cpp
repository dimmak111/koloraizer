#include "Core/Frustum.h"
#include "Math/Matrix4.h"
#include "Renderer/Mesh.h"
#include "Renderer/Renderer.h"

Frustum::Frustum() : mCamera(0)
{
}

Frustum::~Frustum(){
}

#include <iostream>

bool Frustum::IsBoxInside(const Matrix4& pMVP, const Vector3f& min, const Vector3f& max)
{
	Vector3f size = max - min;
	
	Vector4f vecs[] = {
		Vector4f(pMVP * Vector4f(min.x,			min.y,			min.z,			1.0f)),
		Vector4f(pMVP * Vector4f(min.x+size.x,	min.y,			min.z,			1.0f)),
		Vector4f(pMVP * Vector4f(min.x,			min.y+size.y,	min.z,			1.0f)),
		Vector4f(pMVP * Vector4f(min.x,			min.y,			min.z+size.z,	1.0f)),
		Vector4f(pMVP * Vector4f(min.x+size.x,	min.y+size.y,	min.z,			1.0f)),
		Vector4f(pMVP * Vector4f(min.x+size.x,	min.y,			min.z+size.z,	1.0f)),
		Vector4f(pMVP * Vector4f(min.x,			min.y+size.y,	min.z+size.z,	1.0f)),
		Vector4f(pMVP * Vector4f(min.x+size.x,	min.y+size.y,	min.z+size.z,	1.0f))
	};

	u32 in = 0;
	for(u32 i = 0; i < 8; ++i) {

		if(vecs[i].x > -vecs[i].w && 
		   vecs[i].x <  vecs[i].w &&
		   vecs[i].y > -vecs[i].w &&
		   vecs[i].y <  vecs[i].w &&
		   vecs[i].z >  0.f		  &&
		   vecs[i].z <  vecs[i].w)
		{
			++in;
		}
	}

	return in ? true : false;
}

/*
bool Frustum::IsMeshInside(Mesh *m) {
	
	Matrix4 P = Renderer::Call().Get3DProjectionMatrix();
	Matrix4 V = mCamera ? mCamera->GetViewMatrix() : Renderer::Call().Get3DViewMatrix();
	Matrix4 M = m->GetWorldTransform();
	Matrix4 MVP = P*V*M;
		
	AABB box = m->GetAABB();
	return IsBoxInside(MVP, box.GetOrigin(), box.GetEnd());
}*/