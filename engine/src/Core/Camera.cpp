#include "Core/Camera.h"
#include "Math/Mathf.h"
#include "Renderer/Renderer.h"

#include <iostream>

Camera::Camera() : mTheta(0), mPhi(0), mSensivity(0.2f), mSpeed(2.f), mWindow(0), mFrustum(0), mEnabled(true)
{
}

Camera::Camera(const Vector3f& at, const Vector3f& to, const Vector3f& up)
: mAt(at), mTo(to), mUp(up), mTheta(0), mPhi(0), mSensivity(0.2f), mSpeed(2.f), mWindow(0), mFrustum(0), mEnabled(true)
{
}

void Camera::Set(const Vector3f& at, const Vector3f& to, const Vector3f& up, App& app)
{
	mAt = at;
	mTo = to;
	mUp = up;

	mWindow =  &app.GetWindow();

	mLastMouseX = mWindow->GetHalfWidth();
	mLastMouseY = mWindow->GetHalfHeight();

	sf::Mouse::setPosition(sf::Vector2i(mWindow->GetHalfWidth(), mWindow->GetHalfHeight()), 
						   mWindow->GetWindow());

	VectorsFromAngles();
	UpdateView();
}

void Camera::UpdateView()
{
	mViewMatrix.LookAt(mAt, mTo, mUp);
}

void Camera::VectorsFromAngles()
{
	mPhi = Math::Clamp(mPhi, -89.f, 89.f);

	mForward.x = Math::Sin(mTheta) * Math::Cos(mPhi);
	mForward.y = Math::Sin(mPhi);
	mForward.z = Math::Cos(mTheta) * Math::Cos(mPhi);

	mRight = CrossProduct(mUp, mForward);
	mRight.Normalize();

	mTo = mAt + mForward;
}

void Camera::ProcessEvents(Input& inputs)
{
	if(inputs.IsKeyUp(Key_E))
	{
		mEnabled = !mEnabled;
		mLastMouseX = inputs.GetMouseX();
		mLastMouseY = inputs.GetMouseY();
	}
	if(!IsEnabled()) return;

	if(inputs.IsKeyDown(Key_Up))
	{
		mAt += mForward * mSpeed;
		mToUpdate = true;
	}
	if(inputs.IsKeyDown(Key_Down))
	{
		mAt -= mForward * mSpeed;
		mToUpdate = true;
	}
	if(inputs.IsKeyDown(Key_Left))
	{
		mAt += mRight * mSpeed;
		mToUpdate = true;
	}
	if(inputs.IsKeyDown(Key_Right))
	{
		mAt -= mRight * mSpeed;
		mToUpdate = true;
	}

	if(mToUpdate)
	{
		mTo = mAt + mForward;
		UpdateView();
		mToUpdate = false;
	}

	if(inputs.GetEventType() == sf::Event::MouseMoved)
	{
		s32 mouseX = inputs.GetMouseX();
		s32 mouseY = inputs.GetMouseY();

		if(mouseX != mLastMouseX || mouseY != mLastMouseY)
		{
			mTheta += (mLastMouseX - mouseX) * mSensivity;
			mPhi += (mLastMouseY - mouseY) * mSensivity;

			mLastMouseX = mouseX;
			mLastMouseY = mouseY;

			VectorsFromAngles();
			UpdateView();
		}	
	}
}