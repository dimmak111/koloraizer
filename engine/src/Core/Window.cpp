#include "Core/Window.h"
#include "Core/Settings.h"
#include "Debug/LogManager.h"

#include <glew/glew.h>
#include <SFML/OpenGL.hpp>

Window::Window()
{
	Settings::Call().Parse();

	mWidth		= Settings::Call().GetSettingInt(Settings::WINDOW_WIDTH);
	mHeight		= Settings::Call().GetSettingInt(Settings::WINDOW_HEIGHT);
	mWinTitle	= Settings::Call().GetSettingString(Settings::WINDOW_TITLE);

	mHalfWidth = mWidth >> 1;
	mHalfHeight = mHeight >> 1;

	sf::ContextSettings cSettings;
	
	// default settings, TODO: use user settings.
	cSettings.depthBits = 24;
	cSettings.stencilBits = 8;
	cSettings.antialiasingLevel = 4;
	cSettings.majorVersion = 3;
	cSettings.minorVersion = 3;

	mWindow.create(sf::VideoMode(mWidth, mHeight), mWinTitle, sf::Style::Default, cSettings);
	Resize(mWidth, mHeight);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glEnable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

Window::Window(u32 w, u32 h, std::string title) : mWindow(), mIsOpen(true), 
	mWidth(w), mHeight(h), mHalfWidth(w>>1), mHalfHeight(h>>1)
{
	mWindow.create(sf::VideoMode(w, h), title);
}

Window::~Window()
{
	mWindow.close();
}

void Window::Close() 
{
	mIsOpen = false;
}

void Window::SetClearColor(const Color& color)
{
	mClearColor = color;
	glClearColor(color.R(), color.G(), color.B(), color.A());
	glClearDepth(1.0);
}

void Window::Resize(s32 w, s32 h)
{
	glViewport(0, 0, w, h);
	
	mWidth			= w;
	mHeight			= h;
	mHalfWidth		= w >> 1;
	mHalfHeight		= h >> 1;
}

void Window::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::ShowCursor(bool state)
{
	mWindow.setMouseCursorVisible(state);
}