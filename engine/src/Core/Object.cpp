#include "Core/Object.h"

u32 Object::NextId = 0;

Object::Object(const std::string& name)
	: mName(name),
	  mParent(0),
	  mVisible(true)
{
	mId = NextId;
	++NextId;
}

Object::~Object()
{
	for(u32 i = 0; i < mChildren.size(); ++i) {
		delete mChildren[i];
	}
	mChildren.clear();
}

/*
static bool OrderByZOrder(Object* obj1, Object* obj2)
{
	return obj1->mPosition.z < obj2->mPosition.z;
}*/

void Object::Add(Object* object)
{
	if(!object)
		return;

	for(Objects::const_iterator it = mChildren.begin(); it != mChildren.end(); ++it)
	{
		if(*it==object)
			return;
	}

	mChildren.push_back(object);
	object->SetParent(this);

	//std::sort(mChildren.begin(), mChildren.end(), &OrderByZOrder);

	OnAdded(object);
}

void Object::Remove(Object* object)
{
	if(!object)
		return;

	for(Objects::iterator it = mChildren.begin(); it != mChildren.end(); ++it)
	{
		if(*it==object) {
			object->SetParent(0);
			mChildren.erase(it);
			break;
		}	
	}

	OnRemoved(object);
}

void Object::Clear()
{
	mChildren.clear();
}

Objects& Object::GetChildren() 
{
	return mChildren;
}

void Object::SetName(const std::string& name)
{
	mName = name;
}

const std::string& Object::GetName() const
{
	return mName;
}

void Object::SetParent(Object* object)
{
	mParent = object;
}

Object* Object::GetParent()
{
	return mParent;
}

void Object::SetVisible(bool visible)
{
	mVisible = visible;
}

bool Object::IsVisible() const
{
	return mVisible;
}

void Object::SetEnabled(bool enable)
{
	mEnabled = enable;
}

bool Object::IsEnabled() const
{
	return mEnabled;
}

void Object::Update(u32 dt)
{
	Objects::iterator end = mChildren.end();
	for(Objects::iterator it = mChildren.begin(); it != end; ++it) {
	
		Object* child = *it;

		if(child){
			if(child->IsEnabled()) {
				child->Update(dt);
			}
		}
	}
}

void Object::Render()
{
	if(!mVisible)
		return;

	u32 size = mChildren.size();
	for(u32 i = 0; i < size; ++i) {
		mChildren[i]->Render();
	}
}

Object* Object::FindObjectByName(const std::string &name)
{
	for(Objects::iterator it = mChildren.begin(); it != mChildren.end(); ++it) {

		if((*it)->GetName() == name)
			return *it;
	}

	return 0;
}

Matrix4 Object::GetWorldTransform()
{
	if(mParent != 0) {

		Matrix4 Temp = GetTransform();
		return mParent->GetTransform() * Temp;
	}

	return GetTransform();
}