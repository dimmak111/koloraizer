#include "Core/Geometry.h"

Geometry::Geometry()
{
}

Geometry::Geometry(std::vector<Vector3f> &verts, std::vector<Vector3f> &normals, std::vector<Vector2f> &texcoords, std::vector<u32>& indices)
: mVertices(verts), mNormals(normals), mTexCoords(texcoords), mIndices(indices)
{
	mPrimitive = PrimitiveTriangle;
}

Geometry::~Geometry()
{
}

void Geometry::MakeCube(float size)
{
	float halfSize = size * 0.5f;

	Vector3f position[] = 
	{
		Vector3f(-halfSize,-halfSize,-halfSize), Vector3f(-halfSize,-halfSize, halfSize), Vector3f( halfSize,-halfSize, halfSize), Vector3f( halfSize,-halfSize,-halfSize),
		Vector3f(-halfSize, halfSize,-halfSize), Vector3f(-halfSize, halfSize, halfSize), Vector3f( halfSize, halfSize, halfSize), Vector3f( halfSize, halfSize,-halfSize), 
		Vector3f(-halfSize,-halfSize,-halfSize), Vector3f(-halfSize, halfSize,-halfSize), Vector3f( halfSize, halfSize,-halfSize), Vector3f( halfSize,-halfSize,-halfSize),
		Vector3f(-halfSize,-halfSize, halfSize), Vector3f(-halfSize, halfSize, halfSize), Vector3f( halfSize, halfSize, halfSize), Vector3f( halfSize,-halfSize, halfSize), 
		Vector3f(-halfSize,-halfSize,-halfSize), Vector3f(-halfSize,-halfSize, halfSize), Vector3f(-halfSize, halfSize, halfSize), Vector3f(-halfSize, halfSize,-halfSize), 
		Vector3f( halfSize,-halfSize,-halfSize), Vector3f( halfSize,-halfSize, halfSize), Vector3f( halfSize, halfSize, halfSize), Vector3f( halfSize, halfSize,-halfSize)
	};

	Vector2f texcoords[] = 
	{
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f)
	};

	Vector3f normals[] = 
	{	
		Vector3f::NEGUNIT_Y, Vector3f::NEGUNIT_Y, Vector3f::NEGUNIT_Y, Vector3f::NEGUNIT_Y,
		Vector3f::UNIT_Y, Vector3f::UNIT_Y, Vector3f::UNIT_Y, Vector3f::UNIT_Y, 
		Vector3f::NEGUNIT_Z, Vector3f::NEGUNIT_Z, Vector3f::NEGUNIT_Z, Vector3f::NEGUNIT_Z,
		Vector3f::UNIT_Z, Vector3f::UNIT_Z, Vector3f::UNIT_Z, Vector3f::UNIT_Z, 
		Vector3f::NEGUNIT_X, Vector3f::NEGUNIT_X, Vector3f::NEGUNIT_X, Vector3f::NEGUNIT_X,
		Vector3f::UNIT_X, Vector3f::UNIT_X, Vector3f::UNIT_X, Vector3f::UNIT_X 
	};

	u32 indices[] =	  
	{
		0, 2, 1, 0, 3, 2, 
		4, 5, 6, 4, 6, 7,
		8, 9, 10, 8, 10, 11, 
		12, 15, 14, 12, 14, 13, 
		16, 17, 18, 16, 18, 19, 
		20, 23, 22, 20, 22, 21
	};

	mVertices = std::vector<Vector3f>(position, position + sizeof(position) / sizeof(Vector3f));
	mNormals = std::vector<Vector3f>(normals, normals + sizeof(normals) / sizeof(Vector3f));
	mTexCoords = std::vector<Vector2f>(texcoords, texcoords + sizeof(texcoords) / sizeof(Vector2f));
	mIndices = std::vector<u32>(indices, indices + sizeof(indices) / sizeof(u32));

	mHasNormals = true;
	mHasTexCoords = true;
}

void Geometry::MakeSphere(f32 radius, u32 u, u32 v)
{
}

void Geometry::MakeQuad(f32 size)
{
	float halfSize = size * 0.5f;

	Vector3f position[] = 
	{
		Vector3f(-halfSize,-halfSize, 0), Vector3f(-halfSize,-halfSize, 0), Vector3f( halfSize,-halfSize, 0), Vector3f( halfSize,-halfSize, 0),
		Vector3f(-halfSize, halfSize, 0), Vector3f(-halfSize, halfSize, 0), Vector3f( halfSize, halfSize, 0), Vector3f( halfSize, halfSize, 0), 
		Vector3f(-halfSize,-halfSize, 0), Vector3f(-halfSize, halfSize, 0), Vector3f( halfSize, halfSize, 0), Vector3f( halfSize,-halfSize, 0),
		Vector3f(-halfSize,-halfSize, 0), Vector3f(-halfSize, halfSize, 0), Vector3f( halfSize, halfSize, 0), Vector3f( halfSize,-halfSize, 0), 
		Vector3f(-halfSize,-halfSize, 0), Vector3f(-halfSize,-halfSize, 0), Vector3f(-halfSize, halfSize, 0), Vector3f(-halfSize, halfSize, 0), 
		Vector3f( halfSize,-halfSize, 0), Vector3f( halfSize,-halfSize, 0), Vector3f( halfSize, halfSize, 0), Vector3f( halfSize, halfSize, 0)
	};

	Vector2f texcoords[] = 
	{
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f),
		Vector2f(1.f, 1.f), Vector2f(1.f, 0.f), Vector2f(0.f, 0.f), Vector2f(0.f, 1.f)
	};

	Vector3f normals[] = 
	{	
		Vector3f::NEGUNIT_Y, Vector3f::NEGUNIT_Y, Vector3f::NEGUNIT_Y, Vector3f::NEGUNIT_Y,
		Vector3f::UNIT_Y, Vector3f::UNIT_Y, Vector3f::UNIT_Y, Vector3f::UNIT_Y, 
		Vector3f::NEGUNIT_Z, Vector3f::NEGUNIT_Z, Vector3f::NEGUNIT_Z, Vector3f::NEGUNIT_Z,
		Vector3f::UNIT_Z, Vector3f::UNIT_Z, Vector3f::UNIT_Z, Vector3f::UNIT_Z, 
		Vector3f::NEGUNIT_X, Vector3f::NEGUNIT_X, Vector3f::NEGUNIT_X, Vector3f::NEGUNIT_X,
		Vector3f::UNIT_X, Vector3f::UNIT_X, Vector3f::UNIT_X, Vector3f::UNIT_X 
	};

	u32 indices[] =	  
	{
		0, 2, 1, 0, 3, 2, 
		4, 5, 6, 4, 6, 7,
		8, 9, 10, 8, 10, 11, 
		12, 15, 14, 12, 14, 13, 
		16, 17, 18, 16, 18, 19, 
		20, 23, 22, 20, 22, 21
	};

	mVertices = std::vector<Vector3f>(position, position + sizeof(position) / sizeof(Vector3f));
	mNormals = std::vector<Vector3f>(normals, normals + sizeof(normals) / sizeof(Vector3f));
	mTexCoords = std::vector<Vector2f>(texcoords, texcoords + sizeof(texcoords) / sizeof(Vector2f));
	mIndices = std::vector<u32>(indices, indices + sizeof(indices) / sizeof(u32));

	mHasNormals = true;
	mHasTexCoords = true;
}