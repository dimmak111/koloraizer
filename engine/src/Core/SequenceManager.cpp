#include <App.h>
#include <Core/SequenceManager.h>
#include <Core/Sequence.h>
#include <Core/Window.h>
#include <Core/Input.h>
#include <Debug/LogManager.h>
#include <Utils/Timer.h>

#include <iostream>

SequenceManager::SequenceManager()
	: mCurSequence(0)
{
}

SequenceManager::~SequenceManager()
{
	std::map<std::string, Sequence*>::iterator it;
	for(it = mSequences.begin(); it != mSequences.end(); ++it)
		delete it->second;
	mSequences.clear();
}

void SequenceManager::RegisterApp(App *app) 
{
	mApp = app;
}

void SequenceManager::RegisterSequence(const std::string& key, Sequence* sequence)
{
	if(!mSequences[key])
		mSequences[key] = sequence;
}

void SequenceManager::SetCurSequence(std::string key)
{
	if(!mSequences[key]) {
		LogManager::Call().Error("Sequence " + key + " not found! aborting\n");
		exit(EXIT_FAILURE);
	}

	if(mCurSequence) {
		mCurSequence->Release();
	}

	mCurSequence = mSequences[key];
	mCurSequence->Init();
}
