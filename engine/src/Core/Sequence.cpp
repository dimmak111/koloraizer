#include "Core/Sequence.h"

Sequence::Sequence(App* app, const std::string& next)
	: Object(),
	  mApp(app),
	  mNextSequence(next),
	  mStopped(false)
{
}

Sequence::~Sequence()
{
}

void Sequence::Stop()
{
	mStopped = true;
}