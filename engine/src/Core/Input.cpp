#include "Core/Input.h"
#include "Math/Mathf.h"

Input::Input() : mWindow(0), mThreshold(10.0f) 
{
}

void Input::Init(Window* pWindow)
{
	mWindow = pWindow;
}

const f32 Input::GetJoystickX(u32 index)
{
	if(!IsJoystickExist(index))
		return 0;

	f32 val = sf::Joystick::getAxisPosition(index, sf::Joystick::X);
	return (Math::Abs(val) > mThreshold) ? val : 0;
}

const f32 Input::GetJoystickY(u32 index)
{
	if(!IsJoystickExist(index))
		return 0;

	f32 val = sf::Joystick::getAxisPosition(index, sf::Joystick::Y);
	return (Math::Abs(val) > mThreshold) ? val : 0;
}

bool Input::IsKeyDown(Key key)
{
	return sf::Keyboard::isKeyPressed((sf::Keyboard::Key)key);
}

bool Input::IsKeyUp(Key key)
{
	return (mEvents.type == sf::Event::KeyReleased && mEvents.key.code == key);
}

bool Input::IsJoystickExist(u32 index)
{
	return sf::Joystick::isConnected(index);
}

bool Input::IsMouseDown(MouseButton button) {
	return sf::Mouse::isButtonPressed((sf::Mouse::Button)button);
}