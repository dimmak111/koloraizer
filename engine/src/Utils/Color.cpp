#include "Utils/Color.h"

Color::Color(f32 R, f32 G, f32 B, f32 A) : r(R), g(G), b(B), a(A)
{
}

const Color Color::Black(0.f, 0.f, 0.f, 1.f);
const Color Color::White(1.f, 1.f, 1.f, 1.f);

Color ToAPIColor(const sf::Color& color)
{
	return Color(color.r/255.f, color.g/255.f, color.b/255.f, color.a/255.f);
}

sf::Color ToSFMLColor(const Color& color)
{
	return sf::Color(static_cast<u8>(color.R()*255), 
					 static_cast<u8>(color.G()*255), 
					 static_cast<u8>(color.B()*255), 
					 static_cast<u8>(color.A()*255));
}