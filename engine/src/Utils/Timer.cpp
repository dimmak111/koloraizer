#include "Utils/Timer.h"
#include <iostream>

Timer::Timer()
{
	mFrameCount = 0u;
	mDeltaTime = 0;
	mTime = 0;

	mClock.restart();
}

Timer::~Timer() {
}

void Timer::Reset() 
{
	mFrameCount = 0u;
	mDeltaTime = 0;
	mTime = 0;

	mClock.restart();
}

void Timer::Tick() 
{
	++mFrameCount;

	u32 curTime =  mClock.getElapsedTime().asMilliseconds();
	mDeltaTime = curTime - mTime;
	mTime = curTime;
}