#include "Utils/File.h"

File::File() 
{
}

File::~File()
{
	if(IsOpened())
		Close();
}

bool File::IsOpened()
{
	return mFile.is_open();
}

void File::Open(const std::string& name)
{
	mFileName = name;
	mFile.open(mFileName.c_str(), std::ios::in);
}

std::string File::Read() const 
{
	if(mFile) {
		std::stringstream buffer;
		buffer << mFile.rdbuf();
		std::string ret(buffer.str());
		ret.push_back('\0');
		return ret;
	}
	else return "";
}

std::string File::GetLine()
{
	std::string ret;
	std::getline(mFile, ret);
	return ret;
}

bool File::Exist(const std::string& filename)
{
	std::ifstream readFile(filename.c_str());
	return readFile.is_open();
}

void File::Close()
{
	if(IsOpened())
		mFile.close();
}

bool File::End() const
{
	return mFile.eof();
}