#include "Math/Quaternion.h"
#include "Math/Mathf.h"
#include "Math/Matrix4.h"

Quaternion::Quaternion(const f32& X, const f32& Y, const f32& Z, const f32 W) : x(X), y(Y), z(Z), w(W) 
{
}

Quaternion::~Quaternion()
{
}

void Quaternion::Identity()
{
	w = 1.f;
	x = y = z = 0.f;
}

Quaternion Quaternion::Conjugate() const
{
	return Quaternion(-x, -y, -z, w);
}

void Quaternion::Normalize()
{
	f32 length = Length();

	if(length != 0.f)
	{
		x /= length;
		y /= length;
		z /= length;
		w /= length;
	}
}

const f32 Quaternion::Length() const
{
	return Math::Sqrt(w*w + x*x + y*y + z*z);
}

void Quaternion::FromEulerAngle(const f32& X, const f32& Y, const f32& Z)
{
	// P == X
	// B == Z
	// H == Y

	Quaternion Qx(Math::Sin(X/2), 0, 0, Math::Cos(X/2));
	Quaternion Qy(0, Math::Sin(Y/2), 0, Math::Cos(Y/2));
	Quaternion Qz(0, 0, Math::Sin(Z/2), Math::Cos(Z/2));

	*this = Qz * Qx * Qy;
}

const Matrix4 Quaternion::ToMatrix() const
{
	f32 x2 = x*x;
	f32 y2 = y*y;
	f32 z2 = z*z;
	f32 xy = x*y;
	f32 wz = w*z;
	f32 xz = x*z;
	f32 wy = w*y;
	f32 yz = y*z;
	f32 wx = w*x;

	return Matrix4(1.f-2.f*(y2 - z2) , 2.f * (xy + wz)   , 2.f * (xz - wy)   , 0,
				   2.f * (xy - wz)   , 1.f -2.f*(x2, z2) , 2.f * (yz + wx)   , 0,
				   2.f * (xz + wy)   , 2.f * (yz - wx)   , 1.f - 2.f*(x2-y2) , 0,
				   0			     , 0			     , 0			     , 1.f);
}

const Quaternion Quaternion::operator* (const Quaternion& quat) const
{
	return Quaternion(w * quat.x + x * quat.w + y * quat.z - z * quat.y,
					  w * quat.y + y * quat.w + z * quat.x - x * quat.z,
					  w * quat.z + z * quat.w + x * quat.y - y * quat.x,
					  w * quat.w - x * quat.x - y * quat.y - z * quat.z);
}