#include "Math/Mathf.h"
#include "Math/Vector3.h"

const f32 Math::Pi = 3.14159265f;
const f32 Math::HalfPi = 1.57079633f;
const f32 Math::Epsilon = std::numeric_limits<f32>::epsilon( );

template<> const Vector3i Vector3i::ZERO = Vector3i(0,0,0);
template<> const Vector3f Vector3f::ZERO = Vector3f(0.f,0.f,0.f);

template<> const Vector3f Vector3f::UNIT_X = Vector3f(1.f,0.f,0.f);
template<> const Vector3f Vector3f::UNIT_Y = Vector3f(0.f,1.f,0.f);
template<> const Vector3f Vector3f::UNIT_Z = Vector3f(1.f,0.f,0.f);
template<> const Vector3f Vector3f::NEGUNIT_X = Vector3f(-1.f,0.f,0.f);
template<> const Vector3f Vector3f::NEGUNIT_Y = Vector3f(0.f,-1.f,0.f);
template<> const Vector3f Vector3f::NEGUNIT_Z = Vector3f(0.f,0.f,-1.f);
