#include "Math/ITransform.h"
#include "Math/Mathf.h"
#include "Math/Quaternion.h"

ITransform::ITransform() 
	: mPosition(0, 0, 0),
	  mRotation(0, 0, 0),
	  mScale(1, 1, 1),
	  mOrigin(0, 0, 0)
{
	mTransform.Identity();
}

ITransform::~ITransform()
{
}

void ITransform::SetOrigin(const f32& x, const f32& y, const f32& z)
{
	mOrigin.x = x;
	mOrigin.y = y;
	mOrigin.z = z;

	mToComputeTransform = true;
}

void ITransform::SetPosition(const f32& x, const f32& y, const f32& z)
{
	mPosition.x = x;
	mPosition.y = y;
	mPosition.z = z;

	mToComputeTransform = true;
}

void ITransform::SetRotation(const f32& rX, const f32& rY, const f32& rZ)
{
	mRotation.x = rX;
	mRotation.y = rY;
	mRotation.z = rZ;

	mToComputeTransform = true;
}

void ITransform::SetScale(const f32& sX, const f32& sY, const f32& sZ)
{
	mScale.x = sX;
	mScale.y = sY;
	mScale.z = sZ;
	
	mToComputeTransform = true;
}

void ITransform::Move(const f32& offsetX, const f32& offsetY, const f32& offsetZ)
{
	SetPosition(mPosition.x + offsetX, mPosition.y + offsetY, mPosition.z + offsetZ);
}

void ITransform::Rotate(const f32& angleX, const f32& angleY, const f32& angleZ)
{
	SetRotation(mRotation.x + angleX, mRotation.y + angleY, mRotation.z + angleZ);
}

void ITransform::Scale(const f32& sX, const f32& sY, const f32& sZ)
{
	SetScale(mScale.x + sX, mScale.y + sY, mScale.z + sZ);
}

const Matrix4& ITransform::GetTransform() const
{
	if(mToComputeTransform)
	{
		mToComputeTransform = false;
		mTransform.Identity();

		if(mScale.x != 1.f || mScale.y != 1.f || mScale.z != 1.f) 
			mTransform.Scale(mScale.x, mScale.y, mScale.z);

		if(mRotation.x != 0.f || mRotation.y != 0.f || mRotation.z != 0.f)
			mTransform.Rotate(mRotation.x, mRotation.y, mRotation.z);
			
		if(mPosition.x != 0.f || mPosition.y != 0.f || mPosition.z != 0.f)
			mTransform.Translate(mPosition.x, mPosition.y, mPosition.z); 

		if(mOrigin.x != 0.f || mOrigin.y != 0.f || mOrigin.z != 0.f)
		{
			mTransform.m41 = mPosition.x - mTransform.m11 * mOrigin.x
										 - mTransform.m21 * mOrigin.y
										 - mTransform.m23 * mOrigin.z;

			mTransform.m42 = mPosition.y - mTransform.m12 * mOrigin.x
										 - mTransform.m22 * mOrigin.y
										 - mTransform.m23 * mOrigin.z;
		}
	}

	return mTransform;
}