#include "Math/Matrix4.h"

#include "Math/Mathf.h"
#include "Math/Quaternion.h"

Matrix4::Matrix4(f32 a11, f32 a12, f32 a13, f32 a14,
				 f32 a21, f32 a22, f32 a23, f32 a24,
				 f32 a31, f32 a32, f32 a33, f32 a34,
				 f32 a41, f32 a42, f32 a43, f32 a44)
{
	m11 = a11; m12 = a12; m13 = a13; m14 = a14;
	m21 = a21; m22 = a22; m23 = a23; m24 = a24;
	m31 = a31; m32 = a32; m33 = a33; m34 = a34;
	m41 = a41; m42 = a42; m43 = a43; m44 = a44;
}

void Matrix4::Identity()
{
	m11 = m22 = m33 = m44 = 1.f;
	m12 = m13 = m14 = m21 = m23 = m24 = m31 = m32 = m34 = m41 = m42 = m43 = 0.f;
}

f32 Matrix4::Det()
{
	f32 A = m22 * (m33 * m44 - m43 * m34) - m23 * (m32 * m44 - m34 * m42) + m24 * (m32 * m43 - m33 * m42);
    f32 B = m21 * (m33 * m44 - m43 * m34) - m23 * (m31 * m44 - m34 * m41) + m24 * (m31 * m43 - m33 * m41);
    f32 C = m21 * (m32 * m44 - m34 * m42) - m22 * (m31 * m44 - m34 * m41) + m24 * (m31 * m42 - m32 * m41);
    f32 D = m21 * (m32 * m43 - m33 * m42) - m22 * (m31 * m43 - m33 * m41) + m23 * (m31 * m42 - m32 * m41);
	return m11 * A - m12 * B + m13 * C - m14 * D;
}

void Matrix4::Transpose()
{
	Matrix4 old(*this);
	m12 = old.m21;
	m13 = old.m31;
	m14 = old.m41;

	m21 = old.m12;
	m23 = old.m32;
	m24 = old.m42;

	m31 = old.m13;
	m32 = old.m23;
	m34 = old.m43;
	
	m41 = old.m14;
	m42 = old.m24;
	m43 = old.m34;
}

void Matrix4::Inverse() {

	f32 det = Det();
	Matrix4 inverse;

	if(Math::Abs(det) > Math::Epsilon){
		inverse.m11 =  (m22 * (m33 * m44 - m43 * m34) - m23 * (m32 * m44 - m34 * m42) + m24 * (m32 * m43 - m33 * m42)) / det;
        inverse.m21 = -(m21 * (m33 * m44 - m43 * m34) - m23 * (m31 * m44 - m34 * m41) + m24 * (m31 * m43 - m33 * m41)) / det;
        inverse.m31 =  (m21 * (m32 * m44 - m34 * m42) - m22 * (m31 * m44 - m34 * m41) + m24 * (m31 * m42 - m32 * m41)) / det;
        inverse.m41 = -(m21 * (m32 * m43 - m33 * m42) - m22 * (m31 * m43 - m33 * m41) + m23 * (m31 * m42 - m32 * m41)) / det;

        inverse.m12 = -(m12 * (m33 * m44 - m34 * m43) - m13 * (m32 * m44 - m34 * m42) + m14 * (m32 * m43 - m33 *  m42)) / det;
        inverse.m22 =  (m11 * (m33 * m44 - m34 * m43) - m13 * (m31 * m44 - m34 * m41) + m14 * (m31 * m43 - m33 *  m41)) / det;
        inverse.m32 = -(m11 * (m32 * m44 - m34 * m42) - m12 * (m31 * m44 - m34 * m41) + m14 * (m31 * m42 - m32 *  m41)) / det;
        inverse.m42 =  (m11 * (m32 * m43 - m33 * m42) - m12 * (m31 * m43 - m33 * m41) + m13 * (m31 * m42 - m32 *  m41)) / det;

        inverse.m13 =  (m12 * (m23 * m44 - m24 * m43) - m13 * (m22 * m44 - m24 * m42) + m14 * (m22 * m43 - m23 *  m42)) / det;
        inverse.m23 = -(m11 * (m23 * m44 - m24 * m43) - m13 * (m21 * m44 - m24 * m41) + m14 * (m21 * m43 - m23 *  m41)) / det;
        inverse.m33 =  (m11 * (m22 * m44 - m24 * m42) - m12 * (m21 * m44 - m24 * m41) + m14 * (m21 * m42 - m22 *  m41)) / det;
        inverse.m43 = -(m11 * (m22 * m43 - m23 * m42) - m12 * (m21 * m43 - m23 * m41) + m13 * (m21 * m42 - m22 *  m41)) / det;

        inverse.m14 = -(m12 * (m23 * m34 - m24 * m33) - m13 * (m22 * m34 - m24 * m32) + m14 * (m22 * m33 - m23 *  m32)) / det;
        inverse.m24 =  (m11 * (m23 * m34 - m24 * m33) - m13 * (m21 * m34 - m24 * m31) + m14 * (m21 * m33 - m23 *  m31)) / det;
        inverse.m34 = -(m11 * (m22 * m34 - m24 * m32) - m12 * (m21 * m34 - m24 * m31) + m14 * (m21 * m32 - m22 *  m31)) / det;
        inverse.m44 =  (m11 * (m22 * m33 - m23 * m32) - m12 * (m21 * m33 - m23 * m31) + m13 * (m21 * m32 - m22 *  m31)) / det;
	}

	(*this) = inverse;
}

void Matrix4::SetScale(const f32& sx, const f32& sy, const f32& sz)
{
	m11 = sx;
	m22 = sy;
	m33 = sz;
}

void Matrix4::SetRotationX(const f32& angle)
{
	f32 Cos = Math::Cos(angle);
	f32 Sin = Math::Sin(angle);

	m11 = 1.f; m12 = 0.f; m13 = 0.f;
	m21 = 0.f; m22 = Cos; m23 = Sin;
	m31 = 0.f; m32 = -Sin; m33 = Cos;
}

void Matrix4::SetRotationY(const f32& angle)
{
	f32 Cos = Math::Cos(angle);
	f32 Sin = Math::Sin(angle);

	m11 = Cos; m12 = 0.f; m13 = -Sin;
	m21 = 0.f; m22 = 1.f; m23 = 0.f;
	m31 = Sin; m32 = 0.f; m33 = Cos;
}

void Matrix4::SetRotationZ(const f32& angle)
{
	f32 Cos = Math::Cos(angle);
	f32 Sin = Math::Sin(angle);

	m11 = Cos; m12 = Sin; m13 = 0.f;
	m21 = -Sin; m22 = Cos; m23 = 0.f;
	m31 = 0.f; m32 = 0.f; m33 = 1.f;
}

void Matrix4::Translate(const f32& tx, const f32& ty, const f32& tz)
{
	m41 += tx;
	m42 += ty;
	m43 += tz;
}

void Matrix4::Scale(const f32& sx, const f32& sy, const f32& sz)
{
	m11 *= sx;
	m22 *= sy;
	m33 *= sz;
}

void Matrix4::Rotate(const f32& rx, const f32& ry, const f32& rz)
{
	Matrix4 Temp;
	Matrix4 Rot;

	Temp.SetRotationX(rx);
	Rot *= Temp;
	Temp.SetRotationY(ry);
	Rot *= Temp;
	Temp.SetRotationZ(rz);
	Rot *= Temp;
	
	*this *= Rot;
}

Vector3f Matrix4::GetTranslation()
{
	return Vector3f(m41, m42, m43);
}

void Matrix4::MakeOrtho(f32 left, f32 right, f32 top, f32 bottom)
{
	m11 = 2.f / (right - left);		m12 = 0.f;								m13 = 0.f;		m14 = 0.f;
	m21 = 0.f;						m22 = 2.f / (top - bottom);				m23 = 0.f;		m24 = 0.f;
	m31 = 0.f;						m32 = 0.f;								m33 = 1.f;		m34 = 0.f;
	m41 = (1+right)/(1-right);		m42 = (top + bottom) / (bottom - top);	m43 = 0.f;		m44 = 1.f;
}

void Matrix4::MakePerspective(f32 fov, f32 ratio, f32 nearz, f32 farz)
{
	f32 range = Math::Tan(fov/2.f) * nearz;
	
	f32 left = -range * ratio;
	f32 right = range * ratio;
	f32 bottom = -range;
	f32 top = range;

	m11 = (2 * nearz) / (right - left); 
	m22 = (2 * nearz) / (top - bottom);
	m33 = -(farz + nearz) / (farz - nearz);	
	m34 = -1.f;
	m43 = -(2 * farz * nearz) / (farz - nearz);
}

void Matrix4::LookAt(const Vector3f &from, const Vector3f &to, const Vector3f &up)
{
	Vector3f zaxis = to - from;
	zaxis.Normalize();
	
	Vector3f yaxis = up;
	yaxis.Normalize();

	Vector3f xaxis = CrossProduct(zaxis, yaxis);
	xaxis.Normalize();

	yaxis = CrossProduct(xaxis, zaxis);
	yaxis.Normalize();

	m11 = xaxis.x;	m12 = yaxis.x;	m13 = -zaxis.x;
	m21 = xaxis.y;	m22 = yaxis.y;	m23 = -zaxis.y;
	m31 = xaxis.z;	m32 = yaxis.z;	m33 = -zaxis.z;
	m41 = -DotProduct(xaxis, from);	m42 = -DotProduct(yaxis, from);	m43 = DotProduct(zaxis, from);	m44 = 1.f;
}

f32& Matrix4::operator() (size_t i, size_t j)
{
	return operator f32*()[i * 4 + j];
}


Matrix4 Matrix4::operator* (const Matrix4& m) const
{
	return Matrix4(m11 * m.m11 + m21 * m.m12 + m31 * m.m13 + m41 * m.m14,
				   m12 * m.m11 + m22 * m.m12 + m32 * m.m13 + m42 * m.m14,
				   m13 * m.m11 + m23 * m.m12 + m33 * m.m13 + m43 * m.m14,
				   m14 * m.m11 + m24 * m.m12 + m34 * m.m13 + m44 * m.m14,
				   
				   m11 * m.m21 + m21 * m.m22 + m31 * m.m23 + m41 * m.m24,
				   m12 * m.m21 + m22 * m.m22 + m32 * m.m23 + m42 * m.m24,
				   m13 * m.m21 + m23 * m.m22 + m33 * m.m23 + m43 * m.m24,
				   m14 * m.m21 + m24 * m.m22 + m34 * m.m23 + m44 * m.m24,
				   
					m11 * m.m31 + m21 * m.m32 + m31 * m.m33 + m41 * m.m34,
					m12 * m.m31 + m22 * m.m32 + m32 * m.m33 + m42 * m.m34,
					m13 * m.m31 + m23 * m.m32 + m33 * m.m33 + m43 * m.m34,
					m14 * m.m31 + m24 * m.m32 + m34 * m.m33 + m44 * m.m34,

					m11 * m.m41 + m21 * m.m42 + m31 * m.m43 + m41 * m.m44,
					m12 * m.m41 + m22 * m.m42 + m32 * m.m43 + m42 * m.m44,
					m13 * m.m41 + m23 * m.m42 + m33 * m.m43 + m43 * m.m44,
					m14 * m.m41 + m24 * m.m42 + m34 * m.m43 + m44 * m.m44);
}

Matrix4& Matrix4::operator*= (const Matrix4& m)
{
	*this = *this * m;
	return *this;
}

Vector4f operator*(const Matrix4& mat, const Vector4f& vec) {
	Vector4f v;
	v.x = mat.m11 * vec.x + mat.m21 * vec.y + mat.m31 * vec.z + mat.m41 * vec.w;
	v.y = mat.m12 * vec.x + mat.m22 * vec.y + mat.m32 * vec.z + mat.m42 * vec.w;
	v.z = mat.m13 * vec.x + mat.m23 * vec.y + mat.m33 * vec.z + mat.m43 * vec.w;
	v.w = mat.m14 * vec.x + mat.m24 * vec.y + mat.m34 * vec.z + mat.m44 * vec.w;
	return v;
}