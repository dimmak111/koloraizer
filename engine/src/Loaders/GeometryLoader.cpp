#include "Loaders/GeometryLoader.h"
#include "Debug/LogManager.h"

#include <assimp/Importer.hpp> 
#include <assimp/scene.h>          
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

bool GeometryLoader::Load(const std::string& url, Geometry& geom)
{
	// Create an instance of the Importer class
	Assimp::Importer importer;

	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll 
	// propably to request more postprocessing than we do in this example.
	const aiScene* scene = importer.ReadFile( url, aiProcess_Triangulate );
        //aiProcess_Triangulate            |
        //aiProcess_JoinIdenticalVertices  |
        //aiProcess_SortByPType);
  
	// If the import failed, report it
	if( !scene)
	{
		const std::string err = "Parsing " + url + importer.GetErrorString(); 
		LogManager::Call().Error(err);
		return false;
	}

	const aiMesh* mesh = scene->mMeshes[0];

	std::vector<Vector3f> vertices;
	std::vector<Vector3f> normals;
	std::vector<Vector2f> texcoords;

	vertices.resize(mesh->mNumVertices);
	normals.resize(mesh->mNumVertices);
	texcoords.resize(mesh->mNumVertices);

	for(u32 i = 0; i < mesh->mNumVertices; ++i)
	{
		vertices[i] = Vector3f(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
		normals[i]	= Vector3f(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);
		
		if(mesh->HasTextureCoords(0))
			texcoords[i] = Vector2f(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
	}

	std::vector<u32> indices;
	
	indices.resize(mesh->mNumFaces * 3, 0);

	int index = 0;
	for(u32 i = 0; i < mesh->mNumFaces; ++i)
	{
		const aiFace& face = mesh->mFaces[i];
		
		indices[index]		= face.mIndices[0];
        indices[index+1]	= face.mIndices[1];
        indices[index+2]	= face.mIndices[2];

		index += 3;
	}

	geom = Geometry(vertices, normals, texcoords, indices);
	return true;
}

/*
using namespace std;

OBJLoader::OBJLoader(const std::string &path):
mFilename(path),
mCurrentToken(""),
mFile(0x00) {
	mFile = fopen(path.c_str(), "rt");
}

OBJLoader::~OBJLoader() {
	if ( mFile )
		fclose(mFile);
	mFile = 0x00;
}

bool OBJLoader::Eof() const {
	return false;
}

bool OBJLoader::IsValid() const {
	return mFile != 0x00;
}

int OBJLoader::ReadToken() {
	if ( !IsValid() ) {
		return EOF;
	}

	char c;

	do {
		c = fgetc(mFile);
	}
	while ( c != EOF && GetCharInfo(c) == WhiteSpace );

	if ( GetCharInfo(c) == NewLine ) {
		c = '\n'; // some os uses \t so we convert
	}

	// Read token
	mCurrentToken.clear();
	while ( c != EOF && GetCharInfo(c) == AlphaNum ) {
		mCurrentToken.push_back(c);
		c = fgetc(mFile);
		if ( GetCharInfo(c) == NewLine ) {
			c = '\n';
		}
	}

	if ( c == '#' ) {
		if ( mCurrentToken.length() == 0 ) {
			mCurrentToken.push_back(c);
		}
		else {
			ungetc(c, mFile);
		}
	}
	else if ( c == '/' ) {
		mCurrentToken.push_back(c);
	}

	if ( mCurrentToken.length() == 0 )
		return c;

	if ( c == EOF || GetCharInfo(c) == NewLine ) {
		ungetc('\n', mFile);
	}
	return 0;
}


OBJLoader::CharInfo OBJLoader::GetCharInfo(const char &c) const {
	CharInfo res;

	if ( isalnum(c) || c == '_' || c == '.' || c == '-')
		res = AlphaNum;
	else if ( c == ' ' || c == '\t' )
		res = WhiteSpace;
	else if  ( c == '\n' || c == '\r' )
		res = NewLine;
	else
		res = Undefined;

	return res;
}


const std::string& OBJLoader::GetToken() const { return mCurrentToken; }

char OBJLoader::GetLastChar() const {
	if ( mCurrentToken.length() > 0 )
		return mCurrentToken[ mCurrentToken.size() - 1 ];
	return -1;
}
char OBJLoader::GetChar(u32 at) const {
	assert( at < mCurrentToken.length() );
	return mCurrentToken[at];
}

int OBJLoader::GetFloat(f32& target) {
	if ( sscanf_s(mCurrentToken.c_str(), "%f", &target) != 1 )
		return -1;
	return 0;

}
int OBJLoader::GetUnsigned(u32& target) {
	if ( sscanf_s(mCurrentToken.c_str(), "%u", &target) != 1 )
		return -1;
	return 0;
}

int OBJLoader::GetVec2(Vector2f& target) {
	int i = 0;
	while( i < 2 && ReadToken() != '\n' ) {
		if ( GetFloat((&target.x)[i++]) < 0 )
			return -1;
	}
	if ( i != 2 )
		return -1;
	return 0;
}

int OBJLoader::GetVec3(Vector3f& target) {
	int i = 0;
	while( i < 3 && ReadToken() != '\n' ) {
		if ( GetFloat((&target.x)[i++]) < 0 )
			return -1;
	}
	if ( i != 3 )
		return -1;
	return 0;
}

void OBJLoader::SkipLine() { while( ReadToken() != '\n'); }

int VertexExist( const std::vector<Vector3i>& list, const Vector3i& vertex) {
	u32 it = 0;
	while(it < list.size()) {
		const Vector3i& vertexCmp = list[it];
		if (vertex.x == vertexCmp.x &&
			vertex.y == vertexCmp.y &&
			vertex.z == vertexCmp.z )
				return it;
		++it;
	}
	return -1;
}

bool OBJLoader::GetGeometry(Geometry& geom)
{
	if ( !IsValid() ) 
		return false;

	// member
	//bHasNormals = false;
	//bHasTexCoords = false;
	// local
	bool hasNormal = false;
	bool hasTexCoord = false;

	// tmp buffers
	std::vector< Vector3f > vertexPosition(0);
	std::vector< Vector2f > texCoord(0);
	std::vector< Vector3f > normal(0);
	std::vector< Vector3i > vertexId(0);
	std::vector< Vector3i > triangles(0);

	while(!Eof())  {
		int code = ReadToken();
		if ( code == EOF )
			break;
		if ( code == '\n') // avoid empty lines
			continue;

		const std::string& token = GetToken();
		if ( token == "mtllib") {
			// TODO
			SkipLine();
		}
		else if ( token == "usemtl" ) {
			SkipLine();

		}
		else if ( token == "v" ) {
			Vector3f tmp;
			GetVec3(tmp);
			vertexPosition.push_back(tmp);
		}
		else if( token == "vt" ) {
			Vector2f tmp;
			GetVec2(tmp);
			texCoord.push_back(tmp);
		}
		else if( token == "vn" ) {
			Vector3f tmp;
			GetVec3(tmp);
			normal.push_back(tmp);
		}
		else if( token == "f" ) {
			std::vector< int > indices(0);
			indices.clear();
			int i = 0;

			while( ReadToken() != '\n' ) {
				u32 indice_v = 0;
				u32 indice_t = 0;
				u32 indice_n = 0;
				
				GetUnsigned(indice_v);

				if ( GetLastChar() == '/' ) {
					if ( ReadToken() == '\n' )
						return false;
					if ( GetUnsigned(indice_t) < 0 )
						return false;
					hasTexCoord = true;

					if ( GetLastChar() == '/' ) {
						if ( ReadToken() == '\n'
							|| GetUnsigned(indice_n) < 0 )
							return 0;
						hasNormal = true;
					}
				}

				if ( !hasNormal )
					indice_n = indice_v;
				if ( !hasTexCoord )
					indice_t = indice_v;

				Vector3i vertex(indice_v - 1, indice_n - 1, indice_t - 1);

				int vIndice = VertexExist(vertexId, vertex);
				if ( vIndice == -1 ) {
					vertexId.push_back(vertex);
					vIndice = vertexId.size() - 1;
				}
				indices.push_back(vIndice);

				if ( ++i >= 3 ) {
					Vector3i tmp( indices[0], indices[i-2], indices[i-1] );
					triangles.push_back(tmp);
				}
			}
		}
		else {
			SkipLine();
		}
	}

	std::vector<Vector3f> vertices;
	std::vector<Vector3f> normals;
	std::vector<Vector2f> texcoords;
	std::vector<u32> indices;

	if ( normal.size() > 0 ) {
		hasNormal = true;
	}
	if ( texCoord.size() > 0 ) {
		hasTexCoord = true;
	}

	for(u32 i = 0; i < vertexId.size(); ++i) {
		vertices.push_back( vertexPosition[vertexId[i].x] );
		if ( hasNormal ) {
			normals.push_back( normal[vertexId[i].y] );
		}
		if ( hasTexCoord ) {
			texcoords.push_back( texCoord[vertexId[i].z] );
		}
	}
	for(u32 i = 0; i < triangles.size(); ++i) {
		indices.push_back( triangles[i].x );		
		indices.push_back( triangles[i].y );
		indices.push_back( triangles[i].z );
	}

	geom = Geometry(vertices, normals, texcoords, indices);

	return true;
}*/