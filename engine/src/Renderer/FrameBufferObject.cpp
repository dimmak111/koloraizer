#include "Renderer/FrameBufferObject.h"

#include <Asset/Texture.h>

#include <SFML/OpenGL.hpp>

#include <cassert>

FrameBufferObject::FrameBufferObject(bool useDepth)
{
	mId				= 0;
	mDepthBuffer	= 0;
	mHasDepth		= useDepth;

	mWidth			= MAXE_FRAMEBUFFER_SIZE;
	mHeight			= MAXE_FRAMEBUFFER_SIZE;
}

FrameBufferObject::~FrameBufferObject()
{
	for(u32 i = 0; i < mTextures.size(); ++i) {
		delete mTextures[i];
	}

	//delete[] mDrawBuffers;
}

void FrameBufferObject::Create(u32 texts)
{
	glGenFramebuffers(1, &mId);
    glBindFramebuffer(GL_FRAMEBUFFER, mId);

	for(u32 i = 0; i < texts; ++i)
	{
		Texture* text = new Texture();
		text->SetSamplerParameters(GL_CLAMP_TO_EDGE);
		text->Create(mWidth, mHeight);
		text->Bind();
	
		mTextures.push_back(text);
		mDrawBuffers.push_back(GetAttachement(i));

		glFramebufferTexture2D(GL_FRAMEBUFFER, GetAttachement(i), GL_TEXTURE_2D, text->GetId(), 0);
	}

	glDrawBuffers(texts, &(mDrawBuffers[0]));

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		system("PAUSE");
	}

	UnBind();
}

u32 FrameBufferObject::GetAttachement(u32 i)
{	
	assert(i >= 0 && i <= 7);
	return GL_COLOR_ATTACHMENT0+i;
}

void FrameBufferObject::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void FrameBufferObject::Resize(u32 w, u32 h)
{
	mWidth = w;
	mHeight = h;
}

void FrameBufferObject::ResizeViewPort()
{
	glViewport(0, 0, mWidth+1, mHeight+1);
}

void FrameBufferObject::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, mId);
	glDrawBuffers(mDrawBuffers.size(), &(mDrawBuffers[0]));
}

void FrameBufferObject::UnBind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}