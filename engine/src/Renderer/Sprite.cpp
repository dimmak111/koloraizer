#include "Renderer/Sprite.h"

#include "Asset/Shader.h"
#include "Debug/LogManager.h"
#include "Renderer/Renderer.h"

#include <iostream>
//
//Sprite::Sprite(Texture* text, Shader* shader, const IntRect& textRect) : mTexture(text), mShader(shader), mTextRect(textRect)
//{
//	/*
//	if(!mTexture.FromFile(texture)) {
//		LogManager::Get()->Warning("Texture " + texture + " not found");	
//	}*/
//
//	// precache everything by resizing vectors or use statics?
//	mVertices.resize(4, Vector3f());
//	mTextCoord.resize(4, Vector2f());
//	mIndices.resize(6);
//
//	SetTextureRect(textRect);
//	SetSize(Vector3f(static_cast<f32>(mTexture->GetWidth()), 
//					 static_cast<f32>(mTexture->GetHeight()), 0));
//	
//	mVertices[0] = Vector3f(0, 0);
//	mVertices[1] = Vector3f((f32)mTexture->GetWidth(), 0);
//	mVertices[2] = Vector3f(0, (f32)mTexture->GetHeight());
//	mVertices[3] = Vector3f((f32)mTexture->GetWidth(), (f32)mTexture->GetHeight());
//
//	mTextCoord[0] = Vector2f(0.f, 0.f);
//	mTextCoord[1] = Vector2f(1.f, 0.f);
//	mTextCoord[2] = Vector2f(0.f, 1.f);
//	mTextCoord[3] = Vector2f(1.f, 1.f);
//
//	mIndices[0] = 2;
//	mIndices[1] = 1;
//	mIndices[2] = 0;
//
//	mIndices[3] = 1;
//	mIndices[4] = 2;
//	mIndices[5] = 3;
//	
//	glGenVertexArrays(1, &mVao);
//	glBindVertexArray(mVao);
//
//	glGenBuffers(1,	&mVbo);
//	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
//
//	glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(Vector3f), &(mVertices[0]), GL_STATIC_DRAW);
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), 0);
//
//	glGenBuffers(1,	&mTbo);
//	glBindBuffer(GL_ARRAY_BUFFER, mTbo);
//
//	glBufferData(GL_ARRAY_BUFFER, mTextCoord.size() * sizeof(Vector2f), &(mTextCoord[0]), GL_STATIC_DRAW);
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vector2f), 0);
//
//	glGenBuffers(1,	&mIbo);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIbo);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size() * sizeof(u32), &(mIndices[0]), GL_STATIC_DRAW);
//
//	glBindVertexArray(0);
//}
//
//Sprite::~Sprite()
//{
//}
//
//void Sprite::SetTextureRect(const IntRect& rect)
//{
//}
//
//void Sprite::Update(u32 dt)
//{
//}
//
//void Sprite::Render()
//{
//	glActiveTexture(GL_TEXTURE0);
//	mTexture->Bind();
//	mShader->Bind();
//		glBindVertexArray(mVao);
//			glEnableVertexAttribArray(0);
//			glEnableVertexAttribArray(1);
//
//				mShader->SendMatrix4("OP", Renderer::Call().Get2DProjectionMatrix());
//				mShader->SendMatrix4("Model", GetWorldTransform());
//				mShader->SendTexture("Texture", 0);
//
//				PreRender();
//
//				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
//
//				PostRender();
//
//			glDisableVertexAttribArray(1);
//			glDisableVertexAttribArray(0);
//		glBindVertexArray(0);
//	mShader->UnBind();
//	mTexture->UnBind();
//
////	Renderer::Call().EnableDepth();
//	
//	// Render Childrens
//	Object::Render();
//}
