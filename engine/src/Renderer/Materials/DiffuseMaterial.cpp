#include "Renderer/Materials/DiffuseMaterial.h"

DiffuseMaterial::DiffuseMaterial(Color diffuse) : mDiffuseColor(diffuse), mDiffuseTex(NULL)
{

}

void DiffuseMaterial::SetDiffuseColor(Color c)
{
	mDiffuseColor = c;
}

void DiffuseMaterial::SetDiffuseTexture(Texture* tex)
{
	mDiffuseTex = tex;
}

void DiffuseMaterial::Bind()
{
	mShader->Bind();
	
	glActiveTexture(GL_TEXTURE0);

	mDiffuseTex->Bind();

	mShader->SendTexture("u_DiffuseTex", 0);
	mShader->SendColor("u_DiffuseColor", mDiffuseColor);
}