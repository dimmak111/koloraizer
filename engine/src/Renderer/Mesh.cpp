#include "Core/Geometry.h"

#include "Renderer/Material.h"
#include "Renderer/Mesh.h"
#include "Renderer/Renderer.h"

#include "Utils/Color.h"
#include "Utils/File.h"

#include <string>

Mesh::Mesh()
{

}

Mesh::Mesh(Geometry* geom, Material* mat)
{
	mGeometry = geom;
	mMaterial = mat;

	CreateBuffers();
}

void Mesh::CreateBuffers()
{
	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);

	glGenBuffers(1,	&mVbo);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);

	glBufferData(GL_ARRAY_BUFFER, mGeometry->GetVertices().size() * sizeof(Vector3f), &(mGeometry->GetVertices()[0]), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), 0);
	
	glGenBuffers(1,	&mNbo);
	glBindBuffer(GL_ARRAY_BUFFER, mNbo);

	if ( mGeometry->HasNormals() ) {
		glBufferData(GL_ARRAY_BUFFER, mGeometry->GetNormals().size() * sizeof(Vector3f), &(mGeometry->GetNormals()[0]), GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), 0);
	}
	
	if( mGeometry->HasTexCoords() ) {
		glGenBuffers(1,	&mTbo);
		glBindBuffer(GL_ARRAY_BUFFER, mTbo);

		glBufferData(GL_ARRAY_BUFFER, mGeometry->GetTexCoords().size() * sizeof(Vector2f), &( mGeometry->GetTexCoords()[0]), GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vector2f), 0);
	}
	
	glGenBuffers(1,	&mIbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,  mGeometry->GetIndices().size() * sizeof(u32), &( mGeometry->GetIndices()[0]), GL_STATIC_DRAW);

	glBindVertexArray(0);
}

void Mesh::Render()
{
	Matrix4 Projection = Renderer::Call().Get3DProjectionMatrix();
	Matrix4 View = Renderer::Call().Get3DViewMatrix();
	Matrix4 Model = GetWorldTransform();
	Matrix4 ModelView = View * Model;
	Matrix4 MVP = Projection * ModelView;
	
	Matrix4 Normal = Model;
	Normal.Inverse();
	Normal.Transpose();

	mMaterial->Bind();

	glBindVertexArray(mVao);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		if(mGeometry->HasTexCoords())
		{
			glEnableVertexAttribArray(2);
		}
			
		mMaterial->GetShader()->SendMatrix4("u_Projection", Projection);
		mMaterial->GetShader()->SendMatrix4("u_View", View);
		mMaterial->GetShader()->SendMatrix4("u_Model", Model);
		mMaterial->GetShader()->SendMatrix4("u_ModelView", ModelView);
		mMaterial->GetShader()->SendMatrix4("u_MVP", MVP);
		mMaterial->GetShader()->SendMatrix4("u_Normal", MVP);

		glDrawElements(GL_TRIANGLES, mGeometry->GetIndices().size(), GL_UNSIGNED_INT, 0);
	
		if(mGeometry->HasTexCoords())
		{
			glDisableVertexAttribArray(2);
		}

		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
	glBindVertexArray(0);

	Object::Render();
}



//void Mesh::CreateAABB() 
//{
//	Vector3f min = mVertices[0];
//	Vector3f max = mVertices[0];
//
//	for(u32 i = 1; i < mVertices.size(); ++i) 
//	{
//		if(min.x > mVertices[i].x) min.x = mVertices[i].x;
//		if(min.y > mVertices[i].y) min.y = mVertices[i].y;
//		if(min.z > mVertices[i].z) min.z = mVertices[i].z;
//		if(max.x < mVertices[i].x) max.x = mVertices[i].x;
//		if(max.y < mVertices[i].y) max.y = mVertices[i].y;
//		if(max.z < mVertices[i].z) max.z = mVertices[i].z;
//	}
//
//	mAABB.SetOrigin(min);
//	mAABB.SetEnd(max);
//
//	SetSize(max-min);
//	SetCenter((min+max)/2);
//}
//
//int VertexExist( const std::vector<Vector3i>& list, const Vector3i& vertex) {
//	u32 it = 0;
//	while(it < list.size()) {
//		const Vector3i& vertexCmp = list[it];
//		if (vertex.x == vertexCmp.x &&
//			vertex.y == vertexCmp.y &&
//			vertex.z == vertexCmp.z )
//				return it;
//		++it;
//	}
//	return -1;
//}
//
//bool Mesh::FromOBJ(const std::string& name)
//{
//	ObjParser file(name);
//
//	if ( !file.IsValid() ) 
//		return false;
//
//	// member
//	bHasNormals = false;
//	bHasTexCoords = false;
//	// local
//	bool hasNormal = false;
//	bool hasTexCoord = false;
//
//	// tmp buffers
//	std::vector< Vector3f > vertexPosition(0);
//	std::vector< Vector2f > texCoord(0);
//	std::vector< Vector3f > normal(0);
//	std::vector< Vector3i > vertexId(0);
//	std::vector< Vector3i > triangles(0);
//
//	while(!file.Eof())  {
//		int code = file.ReadToken();
//		if ( code == EOF )
//			break;
//		if ( code == '\n') // avoid empty lines
//			continue;
//
//		const std::string& token = file.GetToken();
//		if ( token == "mtllib") {
//			// TODO
//			file.SkipLine();
//		}
//		else if ( token == "usemtl" ) {
//			file.SkipLine();
//
//		}
//		else if ( token == "v" ) {
//			Vector3f tmp;
//			file.GetVec3(tmp);
//			vertexPosition.push_back(tmp);
//		}
//		else if( token == "vt" ) {
//			Vector2f tmp;
//			file.GetVec2(tmp);
//			texCoord.push_back(tmp);
//		}
//		else if( token == "vn" ) {
//			Vector3f tmp;
//			file.GetVec3(tmp);
//			normal.push_back(tmp);
//		}
//		else if( token == "f" ) {
//			std::vector< int > indices(0);
//			indices.clear();
//			int i = 0;
//
//			while( file.ReadToken() != '\n' ) {
//				u32 indice_v = 0;
//				u32 indice_t = 0;
//				u32 indice_n = 0;
//				
//				file.GetUnsigned(indice_v);
//
//				if ( file.GetLastChar() == '/' ) {
//					if ( file.ReadToken() == '\n' )
//						return false;
//					if ( file.GetUnsigned(indice_t) < 0 )
//						return false;
//					hasTexCoord = true;
//
//					if ( file.GetLastChar() == '/' ) {
//						if ( file.ReadToken() == '\n'
//							|| file.GetUnsigned(indice_n) < 0 )
//							return false;
//						hasNormal = true;
//					}
//				}
//
//				if ( !hasNormal )
//					indice_n = indice_v;
//				if ( !hasTexCoord )
//					indice_t = indice_v;
//
//				Vector3i vertex(indice_v - 1, indice_n - 1, indice_t - 1);
//
//				int vIndice = VertexExist(vertexId, vertex);
//				if ( vIndice == -1 ) {
//					vertexId.push_back(vertex);
//					vIndice = vertexId.size() - 1;
//				}
//				indices.push_back(vIndice);
//
//				if ( ++i >= 3 ) {
//					Vector3i tmp( indices[0], indices[i-2], indices[i-1] );
//					triangles.push_back(tmp);
//				}
//			}
//		}
//		else {
//			file.SkipLine();
//		}
//	}
//
//	// Time to create buffers
//	mVertices.clear();
//	mNormals.clear();
//	mTextCoord.clear();
//	mIndices.clear();
//
//	if ( normal.size() > 0 ) {
//		bHasNormals = true;
//	}
//	if ( texCoord.size() > 0 ) {
//		bHasTexCoords = true;
//	}
//
//	for(u32 i = 0; i < vertexId.size(); ++i) {
//		mVertices.push_back( vertexPosition[vertexId[i].x] );
//		if ( bHasNormals ) {
//			mNormals.push_back( normal[vertexId[i].y] );
//		}
//		if ( bHasTexCoords ) {
//			mTextCoord.push_back( texCoord[vertexId[i].z] );
//		}
//	}
//	for(u32 i = 0; i < triangles.size(); ++i) {
//		mIndices.push_back( triangles[i].x );		
//		mIndices.push_back( triangles[i].y );
//		mIndices.push_back( triangles[i].z );
//	}
//
//	CreateBuffers();
//	return true;
//}