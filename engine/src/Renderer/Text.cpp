#include "Renderer/Text.h"

#include <iostream>

Shader Text::StaticShader;

Text::Text(const std::string& text, Font* font, u32 size)
{
	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);
	glGenBuffers(1, &mVbo);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glGenBuffers(1, &mIbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIbo);
	glBindVertexArray(0);
	
	mFont = font;
	mSize = size;

	SetText(text);
}

Text::~Text()
{
}

void Text::InitStatics()
{
	static const char* vs =
	"#version 330\n\
	\n\
	void main(void)\n\
	{\n\
		gl_Position = vec4(1.0);\n\
	}\n";

	static const char* fs = 
	"#version 330\n\
	\n\
	out vec4 FragColor\n\
	void main(void)\n\
	{\n\
		FragColor = vec4(1.0);\n\
	}\n";

	//StaticShader.ComputeAndLink(vs, fs);
}

void Text::SetText(const std::string& pText)
{
	std::string::const_iterator begin = pText.begin();
	std::string::const_iterator end = pText.end();

	for(std::string::const_iterator it = begin; it != end; ++it) {
		std::cout << (*it) << eol;
	}
}