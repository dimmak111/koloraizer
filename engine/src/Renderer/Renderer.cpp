#include "Renderer/Renderer.h"
#include "Core/Object.h"
#include "Core/Window.h"

Renderer::Renderer() : mWindow(NULL), mRoot(NULL)
{
}

Renderer::~Renderer()
{
}

void Renderer::Init(Window *win, Object* root)
{
	mWindow = win;
	mRoot = root;
	SetRenderMode(Renderer::RENDERMODE_FILL);
}

void Renderer::SetRoot(Object* root)
{
	mRoot = root;
}

void Renderer::PushText(Text* pText)
{
	mTextToRender.push_back(pText);
}

void Renderer::Set2DProjectionMatrix(const Matrix4& matrix)
{
	m2DProjectionMatrix = matrix;
}

void Renderer::Set3DProjectionMatrix(const Matrix4& matrix)
{
	m3DProjectionMatrix = matrix;
}

void Renderer::Set3DViewMatrix(const Matrix4& view)
{
	m3DViewMatrix = view;
}

Vector3f Renderer::UnProject(s32 x, s32 y) {

	s32 width =  mWindow->GetWidth();
	s32 height = mWindow->GetHeight();
	f32 xScreen, yScreen, depth;

	x = Math::Clamp<s32>(x, 0, width);
	y = height - Math::Clamp<s32>(y, 0, height);

	xScreen = ((f32)x / width) * 2.f - 1.f;
	yScreen = ((f32)y / height) * 2.f - 1.f;  
	
	glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

	if ( depth < 1.f ) { // found something
		Matrix4 inverse = Call().Get3DProjectionMatrix() * Call().Get3DViewMatrix();
		inverse.Inverse();
		
		f32 zScreen = depth * 2.f - 1.f;

		Vector4f tmp(xScreen, yScreen, zScreen, 1.f);
		tmp = inverse * tmp;
		tmp /= tmp[3];
		return Vector3f(tmp.x, tmp.y, tmp.z);
	}
	return Vector3f(0, 0, depth);
}

void Renderer::EnableDepth()
{
	glEnable(GL_DEPTH_TEST);
}

void Renderer::DisableDepth()
{
	glDisable(GL_DEPTH_TEST);
}

void Renderer::Begin()
{
	mWindow->Clear();
}

void Renderer::End()
{
	mWindow->Display();
}

void Renderer::Render()
{
	if(mRoot != NULL)
		mRoot->Render();

#ifdef _DEBUG
	RenderDebug();	
#endif

	// Until fixed.
	/*
	mWindow->GetWindow().pushGLStates();
		RenderText();
	mWindow->GetWindow().popGLStates();*/
}

void Renderer::RenderDebug()
{
}

void Renderer::RenderText()
{
	if(mTextToRender.size() <= 0)
		return;
	
	/*
	for(mTextIt = mTextToRender.begin(); 
		mTextIt != mTextToRender.end(); ++mTextIt) 
	{
		mWindow->Draw((*mTextIt)->GetSFMLText());
	}*/
}