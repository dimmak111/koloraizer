#include "Asset/Shader.h"

#include "Debug/LogManager.h"
#include "Utils/Color.h"
#include "Utils/File.h"

#include <SFML/OpenGL.hpp>

Shader::Shader() : mProgram(0), mVShader(0), mFShader(0)
{
}

Shader::~Shader()
{
	if(mProgram)
		glDeleteProgram(mProgram);
}

bool Shader::ComputeAndLink(const std::string &vs, const std::string &fs, const std::string& gs)
{
	mProgram = glCreateProgram();

	LoadVertexShader(vs);
	LoadFragmentShader(fs);

	if(gs != "") {
		LoadGeometryShader(gs);
		glAttachShader(mProgram, mGShader);
	}
	
	glAttachShader(mProgram, mVShader);
	glAttachShader(mProgram, mFShader);

	glLinkProgram(mProgram);

	int link_ok;
	glGetProgramiv(mProgram, GL_LINK_STATUS, (int *)&link_ok);
    if(link_ok == FALSE)
    {
		LogManager::Call().Error("while linking the program " + vs + " " + fs);
		return false;
	}
	
	UnBind();

	return true;
}

void Shader::Bind()
{
	glUseProgram(mProgram);
}

void Shader::UnBind()
{
	glUseProgram(0);
}

void Shader::LoadVertexShader(const std::string &vs)
{
	mVShader = glCreateShader(GL_VERTEX_SHADER);

	File vertexFile;
	vertexFile.Open(vs);
	std::string vertexSource = vertexFile.Read();
	const char* vertexChar = vertexSource.c_str();

	glShaderSource(mVShader, 1, &vertexChar, NULL);
	glCompileShader(mVShader);

	GLint compile_ok = GL_FALSE;
	glGetShaderiv(mVShader, GL_COMPILE_STATUS, &compile_ok);
	if(compile_ok == GL_FALSE) {
		LogManager::Call().Error("Shader " + vs + " can't be computed, aborting!");
		
		GLint length;
		glGetShaderiv(mVShader, GL_INFO_LOG_LENGTH, &length);
		char log[10000];
		glGetShaderInfoLog(mVShader, 10000, NULL, &(log[0]));
		LogManager::Call().Error(log);

		glDeleteShader(mVShader);

		system("PAUSE");
		exit(EXIT_FAILURE);
	}
}

void Shader::LoadGeometryShader(const std::string gs)
{
	mGShader = glCreateShader(GL_GEOMETRY_SHADER);

	File geometryFile;
	geometryFile.Open(gs);
	std::string geometrySource = geometryFile.Read();
	const char* geometryChar = geometrySource.c_str();

	glShaderSource(mGShader, 1, &geometryChar, NULL);
	glCompileShader(mGShader);

	GLint compile_ok = GL_FALSE;
	glGetShaderiv(mGShader, GL_COMPILE_STATUS, &compile_ok);
	if(compile_ok == GL_FALSE) {
		LogManager::Call().Error("Shader " + gs + " can't be computed, aborting!");
		
		GLint length;
		glGetShaderiv(mGShader, GL_INFO_LOG_LENGTH, &length);
		char log[10000];
		glGetShaderInfoLog(mGShader, 10000, NULL, &(log[0]));
		LogManager::Call().Error(log);
		
		glDeleteShader(mVShader);
		glDeleteShader(mFShader);
		glDeleteShader(mGShader);
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
}

void Shader::LoadFragmentShader(const std::string &fs)
{
	mFShader = glCreateShader(GL_FRAGMENT_SHADER);

	File fragmentFile;
	fragmentFile.Open(fs);
	std::string fragmentSource = fragmentFile.Read();
	const char* fragmentChar = fragmentSource.c_str();

	glShaderSource(mFShader, 1, &fragmentChar, NULL);
	glCompileShader(mFShader);

	GLint compile_ok = GL_FALSE;
	glGetShaderiv(mFShader, GL_COMPILE_STATUS, &compile_ok);
	if(compile_ok == GL_FALSE) {
		LogManager::Call().Error("Shader " + fs + " can't be computed, aborting!");
		
		GLint length;
		glGetShaderiv(mFShader, GL_INFO_LOG_LENGTH, &length);
		char log[10000];
		glGetShaderInfoLog(mFShader, 10000, NULL, log);
		LogManager::Call().Error(log);
		
		glDeleteShader(mVShader);
		glDeleteShader(mFShader);
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
}

void Shader::SendFloat(const std::string& key, f32 value)
{
	glUniform1f(glGetUniformLocation(mProgram, key.c_str()), value);
}

void Shader::SendVector2(const std::string& key, const Vector2f& vec)
{
	glUniform2fv(glGetUniformLocation(mProgram, key.c_str()), 1, vec);
}

void Shader::SendVector3(const std::string& key, const Vector3f& vec)
{
	glUniform3fv(glGetUniformLocation(mProgram, key.c_str()), 1, vec);
}

void Shader::SendVector4(const std::string& key, const Vector4f& vec) {
	glUniform4fv(glGetUniformLocation(mProgram, key.c_str()), 1, vec);
}

void Shader::SendMatrix4(const std::string &key, const Matrix4 &pMatrix)
{
	glUniformMatrix4fv(glGetUniformLocation(mProgram, key.c_str()), 1, GL_FALSE, pMatrix);
}

void Shader::SendTexture(const std::string& key, const u32& index)
{
	glUniform1i(glGetUniformLocation(mProgram, key.c_str()), index);
}

void Shader::SendColor(const std::string& key, const Color& pColor)
{
	glUniform4fv(glGetUniformLocation(mProgram, key.c_str()), 1, pColor);
}