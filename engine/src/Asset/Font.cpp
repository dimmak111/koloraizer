#include "Asset/Font.h"

#include "Asset/RessourceManager.h"

#include <FreeType/ft2build.h>
#include FT_FREETYPE_H

Font::Font()
{
	mGlyphOffset = 0;
}

Font::~Font()
{
}

bool Font::FromFile(const std::string &filename)
{
	/*
	FT_Face font;
	if ( FT_New_Face(RessourceManager::Call().GetFontLib(), filename.c_str(), 0, &font) ) {
		return false;
	}

	FT_Set_Pixel_Sizes(font, 0, 48);
	FT_GlyphSlot g = font->glyph;

	 for(int i = 32; i < 128; ++i) { // ascii char ranged from 32 to 128
		 if ( FT_Load_Char(font, i, FT_LOAD_RENDER) )
					 continue;

			mWidth += g->bitmap.width;                 
			mHeight = std::max(mHeight, g->bitmap.rows);
	 }

	 mTextureAtlas.SetFormats(GL_ALPHA, GL_ALPHA);
	 mTextureAtlas.SetFilters(GL_LINEAR, GL_LINEAR);
	 mTextureAtlas.SetSamplerParameters(GL_CLAMP_TO_EDGE);
	 mTextureAtlas.Create(mWidth, mHeight);
	 mTextureAtlas.Bind();

	 glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	for(u32 i = 32; i < 128; ++i) {
		if ( FT_Load_Char(font, i, FT_LOAD_RENDER) )
			continue;

		Glyph glyph;

		glyph.width		= (f32)g->bitmap.width;
		glyph.height    = (f32)g->bitmap.rows;
		glyph.left		= (f32)g->bitmap_left;
		glyph.top		= (f32)g->bitmap_top;
		glyph.ax		= (f32)(g->advance.x >> 6);
		glyph.ay		= (f32)(g->advance.y >> 6);

		glyph.tx =  mGlyphOffset / ((f32) mWidth);
		
		glTexSubImage2D(GL_TEXTURE_2D, 
						0, 
						mGlyphOffset, 0, 
						glyph.width, glyph.height, 
						GL_ALPHA, 
						GL_UNSIGNED_BYTE, 
						g->bitmap.buffer);
		
		mGlyphs.insert( std::pair<u32, Glyph>(i, glyph));
		mGlyphOffset += glyph.width;
	}
	
	mTextureAtlas.UnBind();*/
	return true;
}