#include "Asset/RessourceManager.h"

template <class T>
void RessourceManager<T>::AddLocation(const std::string& name)
{
	for(LocationsVec::iterator it = mLocations.begin();
		it != mLocations.end(); ++it)
	{
		if((*it) == name)
			return;
	}

	mLocations.push_back(name);
}

template <class T>
void RessourceManager<T>::Add(const std::string& name, Ressource* res)
{
	mRessources.insert(name, res);
}

template <class T>
void RessourceManager<T>::Remove(const std::string& name)
{
	 typename RessourcesMap::iterator it;
     it = mRessources.find(name);

	 if(it != mRessources.end())
	 {
		 mRessources.erase(it);
	 }
}

template <class T>
void RessourceManager<T>::CleanUp()
{
}


template <class T>
T* RessourceManager<T>::Get(const std::string& name)
{
	if(mRessources[name])
		return mRessources[name];

	return NULL;
}

/*
RessourceManager::RessourceManager()
{

}

RessourceManager::~RessourceManager()
{
}

void RessourceManager::AddLocation(const std::string& name, const std::string& type)
{
	mLocations[name] = type;
} 

const std::string& RessourceManager::GetLocation(const std::string& type)
{
	for(mLocationIt = mLocations.begin(); mLocationIt != mLocations.end(); ++mLocationIt)
	{
		if(mLocationIt->second == type)
			return mLocationIt->first;
	}

	return std::string();
}

void RessourceManager::Add(const std::string& name, Ressource* pRessource)
{
}

void RessourceManager::Remove(const std::string& name)
{
}*/