#include "Asset/TextureCubeMap.h"

#include "Debug/LogManager.h"

#include <SFML/Graphics/Image.hpp>
#include <vector>

TextureCubeMap::TextureCubeMap() : mId(0)
{
}

TextureCubeMap::~TextureCubeMap()
{
	if(mId)
		glDeleteTextures(1, &mId);
}

void TextureCubeMap::Bind()
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, mId);
}

void TextureCubeMap::UnBind()
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

bool TextureCubeMap::FromFiles(	const std::string& front, 
								const std::string& back, 
								const std::string& left, 
								const std::string& right, 
								const std::string& top, 
								const std::string& bottom )
{
	std::string files[] = { front, back, left, right, top, bottom };
	std::vector<sf::Image> images(6);
	
	for(s32 i = 0; i < 6; ++i) {
		if(!images[i].loadFromFile(files[i])) {
			LogManager::Call().Error("Image " + files[i] + "not found");
			return false;
		}
	}

	glGenTextures(1, &mId);
    glBindTexture(GL_TEXTURE_CUBE_MAP, mId);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	u32 faces[] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
					GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
					GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };

	for(s32 i = 0; i < 6; ++i) {

		glTexImage2D(faces[i], 
					 0, 
					 GL_RGBA, 
					 images[i].getSize().x, 
					 images[i].getSize().y, 
					 0, 
					 GL_RGBA, 
					 GL_UNSIGNED_BYTE, 
					 images[i].getPixelsPtr());
	}
	
	UnBind();

	return true;
}