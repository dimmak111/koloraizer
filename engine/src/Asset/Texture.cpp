#include "Asset/Texture.h"
#include "Debug/LogManager.h"

#include <SFML/Graphics/Image.hpp>
#include <SFML/OpenGL.hpp>

const u32 Texture::LINEAR	= GL_LINEAR;
const u32 Texture::NEAREST	= GL_NEAREST;
const u32 Texture::MIPMAP	= GL_MIPMAP;


Texture::Texture() : Ressource(), mId(0), mWidth(0), mHeight(0) 
{
	mFormat			= GL_RGBA32F;
	mGlobalFormat	= GL_RGBA;
	mMinFilter		= GL_MIPMAP;
	mMagFilter		= GL_MIPMAP;
	mSamplerParam	= GL_REPEAT;
}

Texture::~Texture()
{
	if(mId)
		glDeleteTextures(1, &mId);
}

void Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, mId);
}

void Texture::UnBind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::Create(const u32 width, const u32 height)
{
	glGenTextures(1, &mId);
	glBindTexture(GL_TEXTURE_2D, mId);

	glTexImage2D(GL_TEXTURE_2D, 0, mFormat, width, height, 0, mGlobalFormat, GL_UNSIGNED_BYTE, 0);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mMinFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mMagFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mSamplerParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mSamplerParam);

	glGenerateMipmap(GL_TEXTURE_2D);

	mWidth = width;
	mHeight = height;

	UnBind();
}

bool Texture::FromFile(const std::string &file)
{
	sf::Image image;
	if(!image.loadFromFile(file)) {
		LogManager::Call().Error("Image " + file + "not found");
		return false;
	}
	
	glGenTextures(1, &mId);
	glBindTexture(GL_TEXTURE_2D, mId);

	glTexImage2D(GL_TEXTURE_2D, 0, mFormat, image.getSize().x, image.getSize().y, 0, mGlobalFormat, GL_UNSIGNED_BYTE, image.getPixelsPtr());
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mMinFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mMagFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mSamplerParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mSamplerParam);

	glGenerateMipmap(GL_TEXTURE_2D);

	mWidth = image.getSize().x;
	mHeight = image.getSize().y;

	UnBind();

	return true;
}