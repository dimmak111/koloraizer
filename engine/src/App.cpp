#include "App.h"

#include "Asset/Font.h"

#include "Core/Sequence.h"
#include "Core/Settings.h"

#include "Debug/LogManager.h"

#include "Renderer/Renderer.h"
#include "Renderer/Text.h"

#include "Utils/StringConverter.h"
#include "Utils/Timer.h"
#include "Utils/Shared.h"

App::App()
: mUpdateRate(60),
  mMaxUpdate(20)
{
}

App::~App()
{
	LogManager::Kill();
	Renderer::Kill();
	//RessourceManager::Kill();
	Settings::Kill();
}

void App::Run()
{
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		LogManager::Call().Error((const char*)glewGetErrorString(err));
	}
	
	// check if gl core profil is supported
	if(!GLEW_VERSION_3_0) 
	{
		LogManager::Call().Warning("GL 3.0 isn't supported, using fixed pipeline");
	}

	// give the user a chance to do custom init
	Init();

	// register game sequences
	mSequenceManager.RegisterApp(this);
	RegisterSequences(&mSequenceManager);

	if(mSequenceManager.GetNbSequence() <= 0) {
		LogManager::Call().Error("No sequence registered, aborting!");
		system("PAUSE");
		exit(EXIT_FAILURE);
	}

	// parse existing settings
	Settings::Call().Parse();

	// initialize inputs
	mInputs.Init(&mWindow);

	// initialize the renderer to draw the current sequence
	Renderer::Call().Init(&mWindow, &mSequenceManager.GetCurSequence());

	// initialize font lib
	//RessourceManager::Call().InitFontLib();
	//Font f;
	//f.FromFile("ressources/fonts/Arial.ttf");

	Timer timer;

	u32 skipTick = 1000/mUpdateRate;
	
	u32 nextTick = timer.GetTimeSinceStartUp();
	u32 loopCount = 0;
	 
	// game loop
	while(mWindow.IsOpened())
	{
		Sequence& curSequence = mSequenceManager.GetCurSequence();

		ProcessEvents(mInputs);
		
		loopCount = 0;
		while(timer.GetTimeSinceStartUp() > nextTick && loopCount < mMaxUpdate)
		{
			timer.Tick();

			if(!curSequence.IsStopped()) {
				
				curSequence.Update(skipTick);

			} else {

				const std::string& next = curSequence.GetNextSequence();
				if(next != "") {
					mSequenceManager.SetCurSequence(next);
					Renderer::Call().SetRoot( &mSequenceManager.GetCurSequence());
					break;
				}
				else {
					mWindow.Close();
				}
			}

			nextTick += skipTick;
			++loopCount;
		}
		
		Renderer::Call().Begin();
			Renderer::Call().Render();
		Renderer::Call().End();
	}
}

void App::ProcessEvents(Input& input)
{
	Sequence& curSequence = mSequenceManager.GetCurSequence();

	while(input.GetEvents()) {

		switch(input.GetEventType())
		{
			case sf::Event::Closed:
			{
				mWindow.Close();
			}
			break;
			case sf::Event::GainedFocus:
			{
				curSequence.Resume();	
			}
			break;
			case sf::Event::LostFocus:
			{
				curSequence.Pause();	
			}
			break;
			case sf::Event::Resized:
			{
				mWindow.Resize(input.GetCurEvent().size.width, input.GetCurEvent().size.height);
			}
			break;
			default: {
				input.CatchEvent();
				curSequence.ProcessEvents(mInputs);
			}
		}
	}

	ProcessCustomEvents(mInputs);
}