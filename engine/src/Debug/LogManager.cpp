#include "Debug/LogManager.h"

#include <Utils/Shared.h>

#include <iostream>

namespace 
{
	const std::string defaultLog = "engine.log";
}

LogManager::LogManager()
{
	SetLogFile(defaultLog);
}

LogManager::~LogManager()
{
	if(mLogFile.is_open()) {
		mLogFile.close();
	}
}

void LogManager::SetLogFile(const std::string& logFile)
{
	mLogFile.open(logFile.c_str(), std::ios::out | std::ios::trunc);
	
	if(!mLogFile) {
		std::cerr << "[Error] Logger File " << logFile.c_str() << " not found!\n";
	}
}

void LogManager::Log(const std::string &message)
{
	mLogFile << "[Log] " << message << eol;
	std::cout << "[Log] " << message << eol;
}

void LogManager::Error(const std::string &message)
{
	mLogFile << "[Error] " << message << eol;
	std::cout << "[Error] " << message << eol;
}

void LogManager::Warning(const std::string &message)
{
	mLogFile << "[Warning] " << message << eol;
	std::cout << "[Warning] " << message << eol;
}
