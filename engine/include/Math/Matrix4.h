#ifndef H_MATRIX4_
#define H_MATRIX4_

#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Utils/Shared.h"

class ITransform;
class Quaternion;

class SFAPI Matrix4
{
	
	friend class ITransform;

public:

	Matrix4(f32 a11 = 1.f, f32 a12 = 0.f, f32 a13 = 0.f, f32 a14 = 0.f,
			f32 a21 = 0.f, f32 a22 = 1.f, f32 a23 = 0.f, f32 a24 = 0.f,
			f32 a31 = 0.f, f32 a32 = 0.f, f32 a33 = 1.f, f32 a34 = 0.f,
			f32 a41 = 0.f, f32 a42 = 0.f, f32 a43 = 0.f, f32 a44 = 1.f);

	void Identity();

	f32 Det();

	void Transpose();
	void Inverse();

	void SetTranslation(const f32& tx, const f32& ty, const f32 tz);
	void SetTranslation(const Vector3f& t);
	
	void SetScale(const f32& sx, const f32& sy, const f32& sz);

	void SetRotationX(const f32& angle);
	void SetRotationY(const f32& angle);
	void SetRotationZ(const f32& angle);

	void Scale(const f32& sx, const f32& sy, const f32& sz); 
	void Translate(const f32& tx, const f32& ty, const f32& tz);
	void Rotate(const f32& rx, const f32& ry, const f32& rz);

	Vector3f GetTranslation();

	void MakePerspective(f32 fov, f32 ratio, f32 nearz, f32 farz);
	void MakeOrtho(f32 left, f32 right, f32 top, f32 bottom);

	void LookAt(const Vector3f& from, const Vector3f& to, const Vector3f& up);

	f32& operator() (size_t i, size_t j);

	operator f32*() { return &m11; }
	operator const f32*() const { return &m11; }

	Matrix4 operator* (const Matrix4& m) const;
	Matrix4& operator*= (const Matrix4& m);

private:

	friend SFAPI Vector4f operator* (const Matrix4& mat, const Vector4f& vec);

	f32 m11, m12, m13, m14;
	f32 m21, m22, m23, m24;
	f32 m31, m32, m33, m34;
	f32 m41, m42, m43, m44;
};


SFAPI Vector4f operator* (const Matrix4& mat, const Vector4f& vec);

#endif