
template <class T>
inline Vector4<T>::Vector4(T X, T Y, T Z, T W) : x(X), y(Y), z(Z), w(W)
{
}

template <class T>
inline void Vector4<T>::Set(T X, T Y, T Z, T W)
{
	x = X;
	y = Y;
	z = Z;
	w = W;
}

template <class T>
inline T Vector4<T>::Length() const
{
	return Math::Sqrt(x*x + y*y + z*z + w*w);
}

template <class T>
inline T Vector4<T>::SquaredLength() const
{
	return (x*x + y*y + z*z + w*w);
}

template <class T>
inline void Vector4<T>::Normalize()
{
	f32 l = Length();
	if(l > 0.f) {
		x /= l;
		y /= l;
		z /= l;
		w /= l;
	}
}

template <class T>
inline Vector4<T> Vector4<T>::operator- () const
{
	return Vector4<T>(-x, -y, -z, -w);
}

template <class T>
inline Vector4<T> Vector4<T>::operator+ (const Vector4<T>& vec) const
{
	return Vector4<T>(x + vec.x, y + vec.y, z + vec.z, w + vec.w);
}

template <class T>
inline Vector4<T> Vector4<T>::operator- (const Vector4<T>& vec) const
{
	return Vector4<T>(x - vec.x, y - vec.y, z - vec.z, w - vec.w);
}

template <class T>
Vector4<T>& Vector4<T>::operator+= (const Vector4<T>& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
	w += vec.w;

	return (*this);
}

template <class T>
Vector4<T>& Vector4<T>::operator-= (const Vector4<T>& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	w -= vec.w;

	return (*this);
}

template <class T>
Vector4<T> Vector4<T>::operator* (const T t)
{
	return Vector4<T>(x*t, y*t, z*t, w*t);
}

template <class T>
Vector4<T> Vector4<T>::operator/ (const T t)
{
	return Vector4<T>(x/t, y/t, z/t, w/t); 
}

template <class T>
Vector4<T>& Vector4<T>::operator*= (const T t)
{
	x *= t;
	y *= t;
	z *= t;
	w *= t;

	return (*this);
}

template <class T>
Vector4<T>& Vector4<T>::operator/= (const T t)
{
	x /= t;
	y /= t;
	z /= t;
	w /= t;

	return (*this);
}

template <class T>
bool Vector4<T>::operator== (const Vector4<T>& vec) const
{
	return (x == vec.x && y == vec.y && z == vec.z && w == vec.w);
}

template <class T>
bool Vector4<T>::operator!= (const Vector4<T>& vec) const
{
	return !(*this == vec);
}

template <class T>
T DotProduct(const Vector4<T>& a, const Vector4<T>& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

template <class T> 
Vector4<T> operator* (const Vector4<T>& vec, const T t)
{
	return Vector4<T>(vec.x * t, vec.y * t, vec.z * t, vec.w * t);
}

template <class T> 
Vector4<T> operator* (const T t, const Vector4<T>& vec)
{
	return Vector4<T>(vec.x * t, vec.y * t, vec.z * t, vec.w * t);
}