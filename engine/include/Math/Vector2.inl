
template <class T>
inline Vector2<T>::Vector2(T X, T Y) : x(X), y(Y)
{
}

template <class T>
inline void Vector2<T>::Set(T X, T Y)
{
	x = X;
	y = Y;
}

template <class T>
inline T Vector2<T>::Length() const
{
	return Math::Sqrt(x*x + y*y);
}

template <class T>
inline T Vector2<T>::SquaredLength() const
{
	return (x*x + y*y);
}

template <class T>
inline void Vector2<T>::Normalize()
{
	f32 l = Length();
	if(l > 0.f) {
		x /= l;
		y /= l;
	}
}

template <class T>
inline Vector2<T> Vector2<T>::operator- () const
{
	return Vector2<T>(-x, -y);
}

template <class T>
inline Vector2<T> Vector2<T>::operator+ (const Vector2<T>& vec) const
{
	return Vector2<T>(x + vec.x, y + vec.y);
}

template <class T>
inline Vector2<T> Vector2<T>::operator- (const Vector2<T>& vec) const
{
	return Vector2<T>(x - vec.x, y - vec.y);
}

template <class T>
Vector2<T>& Vector2<T>::operator+= (const Vector2<T>& vec)
{
	x += vec.x;
	y += vec.y;

	return (*this);
}

template <class T>
Vector2<T>& Vector2<T>::operator-= (const Vector2<T>& vec)
{
	x -= vec.x;
	y -= vec.y;

	return (*this);
}

template <class T>
Vector2<T> Vector2<T>::operator* (const T t)
{
	return Vector2<T>(x*t, y*t);
}

template <class T>
Vector2<T> Vector2<T>::operator/ (const T t)
{
	return Vector2<T>(x/t, y/t); 
}

template <class T>
Vector2<T>& Vector2<T>::operator*= (const T t)
{
	x *= t;
	y *= t;

	return (*this);
}

template <class T>
Vector2<T>& Vector2<T>::operator/= (const T t)
{
	x /= t;
	y /= t;

	return (*this);
}

template <class T>
bool Vector2<T>::operator== (const Vector2<T>& vec) const
{
	return (x == vec.x && y == vec.y);
}

template <class T>
bool Vector2<T>::operator!= (const Vector2<T>& vec) const
{
	return !(*this == vec);
}

template <class T>
T DotProduct(const Vector2<T>& a, const Vector2<T>& b)
{
	return a.x * b.x + a.y * b.y;
}


template <class T> 
Vector2<T> operator* (const Vector2<T>& vec, const T t)
{
	return Vector2<T>(vec.x * t, vec.y * t);
}

template <class T> 
Vector2<T> operator* (const T t, const Vector2<T>& vec)
{
	return Vector2<T>(vec.x * t, vec.y * t);
}