inline f32 Math::ToDeg(f32 radian)
{
	return radian * 57.295779513082321f;
}

inline f32 Math::ToRad(f32 degree)
{
	return degree * 0.017453292519943295769f;
}

inline f32 Math::Sqrt(f32 a)
{
	return std::sqrt(a);
}

inline f32 Math::Cos(f32 a)
{
	return std::cos(ToRad(a));
}

inline f32 Math::Sin(f32 a)
{
	return std::sin(ToRad(a));
}

inline f32 Math::Tan(f32 a)
{
	return std::tan(ToRad(a));
}

inline f32 Math::ATan(f32 a)
{
	return std::atan(ToRad(a));
}

inline f32 Math::Abs(f32 a)
{
	return std::fabs(a);
}

template <class T> 
inline T Math::Min(T a, T b)
{
	return (a < b) ? a : b;
}

template <class T> 
inline T Math::Max(T a, T b)
{
	return (a > b) ? a : b;
}

template <class T> 
inline T Math::Clamp(T val, T min, T max)
{
	val = (val < min ? min : val);
	val = (val > max ? max : val);

	return val;
}

inline f32 Math::Clamp01(f32 val) 
{
	if( val < 0.f)
		val = 0.f;
	if(val > 1.0f)
		val = 1.0f;
	return val;
}

inline f32 Math::Lerp(f32 val, f32 from, f32 to)
{
	return from + Math::Clamp01(val) * (to - from);
}

inline f64 Math::Cerp(f64 val, f64 from, f64 to)
{
	f64 ft = val * Math::Pi;
	f64 f = (1 - std::cos(ft)) * .5;
	return  from*(1-f) + to*f;
}

inline f32 Math::Cubicerp(f32 val, f32 from1, f32 from2, f32 to1, f32 to2)
{
	f32 P = (to2 - to1) - (from1 - from2);
	f32 Q = (from1 - from2) - P;
	f32 R = to1 - from1;
	f32 S = from1;

	return (P*val*val*val) + (Q*val*val) + (R*val) + S;
}

inline f32 Math::Floor(f32 a)
{
	return std::floor(a);
}

inline f32 Math::Ceil(f32 a)
{
	return std::ceil(a);
}

inline f32 Math::Pow(f32 value, u32 exp)
{
	return static_cast<f32>(std::pow(value, (int)exp));
}

inline f32 Math::Rand(u32 min, u32 max, u32 seed)
{
	if(seed > 0) srand(seed);

	f32 ratio = ((f32)rand()) / ((f32)RAND_MAX);
	return min + (max - min) * ratio;
}

inline bool Math::IsNaN(double value)
{
	return value != value;
}

inline bool Math::IsEqual(f32 a, f32 b)
{
	return std::fabs(a - b) < Math::Epsilon;
}

inline bool Math::IsPowOf2(u32 value)
{
	return (value > 0) && ((value & (value - 1)) == 0);
}