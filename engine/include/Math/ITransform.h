#ifndef H_ITRANSFORM_
#define H_ITRANSFORM_

#include "Math/Matrix4.h"
#include "Utils/Shared.h"

class SFAPI ITransform
{
public:

	ITransform();
	~ITransform();

	void SetOrigin(const f32& x, const f32& y, const f32& z=0);
	void SetPosition(const f32& x, const f32& y, const f32& z=0);
	void SetScale(const f32& sX, const f32& sY, const f32& sZ);
	void SetRotation(const f32& rX, const f32& rY, const f32& rZ);

	void Move(const f32& offsetX, const f32& offsetY, const f32& offsetZ = 0);
	void Rotate(const f32& angleX, const f32& angleY, const f32& angleZ);
	void Scale(const f32& sX, const f32& sY, const f32& sZ);

	const Matrix4& GetTransform() const;
	inline const Vector3f& GetPosition() const		{ return mPosition; }
	inline const Vector3f& GetScale() const			{ return mScale; }

private:

	Vector3f mOrigin;
	Vector3f mPosition;
	Vector3f mScale;
	Vector3f mRotation;

	mutable bool mToComputeTransform;
	mutable Matrix4 mTransform;
};

#endif