#pragma once

#include <Utils/Shared.h>

#include <cmath>
#include <limits>

class SFAPI Math
{
public:

	static f32 ToDeg(f32 radian);
	static f32 ToRad(f32 degree);

	static f32 Abs(f32 a);
	static f32 ACos(f32 a);
	static f32 ASin(f32 a);
	static f32 ATan(f32 a);
	static f32 Cos(f32 a);
	static f32 Sin(f32 a);
	static f32 Tan(f32 a);
	static f32 Sqrt(f32 a);

	template <class T> static T Min(T a, T b);
	template <class T> static T Max(T a, T b);	
	template <class T> static T Clamp(T val, T min, T max);
	static f32 Clamp01(f32 val);

	static f32 Lerp(f32 val, f32 from, f32 to);
	static f64 Cerp(f64 val, f64 from, f64 to);
	static f32 Cubicerp(f32 val, f32 from1, f32 from2, f32 to1, f32 to2);
	static f32 LerpAngle(f32 val, f32 from, f32 to);

	static f32 Rand(u32 min = 0, u32 max = 1, u32 seed = 0);

	static f32 Floor(f32 a);
	static f32 Ceil(f32 a);

	static s32 Sign(s32 a);
	static s32 Sign(f32 a);

	static f32 Pow(f32 value, u32 exp);

	static bool IsEqual(f32 a, f32 b);
	static bool IsNaN(double value);
	static bool IsPowOf2(u32 value);

public:

	static const f32 Pi;
	static const f32 HalfPi;
	static const f32 Epsilon;
};

#include "Mathf.inl"