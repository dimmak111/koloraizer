#ifndef H_VECTOR2_
#define H_VECTOR2_

#include <SFML/System/Vector2.hpp>
#include "Utils/Shared.h"
#include "Math/Mathf.h"

template <class T>
class Vector2
{
public:

	Vector2(T X = 0, T Y = 0);
	
	void Set(T X, T Y);
	
	T Length() const;
	T SquaredLength() const;

	void Normalize();

	// Negate operator
	Vector2<T> operator- () const;

	Vector2<T> operator- (const Vector2<T>& vec) const;
	Vector2<T> operator+ (const Vector2<T>& vec) const;

	Vector2<T>& operator+= (const Vector2<T>& vec);
	Vector2<T>& operator-= (const Vector2<T>& vec);
	
	Vector2<T> operator* (const T t);
	Vector2<T> operator/ (const T t);

	Vector2<T>& operator*= (const T t);
	Vector2<T>& operator/= (const T t);

	bool operator== (const Vector2<T>& vec) const;
	bool operator!= (const Vector2<T>& vec) const;

	SFAPI operator f32*() { return &x; }
	SFAPI operator const f32*() const { return &x; }

public:

	static const Vector2 ZERO;

	static const Vector2 UNIT_Z; // Z
	static const Vector2 NEGUNIT_Z; // -Z

	static const Vector2 UNIT_Y; // Y
	static const Vector2 NEGUNIT_Y; // -Y

	static const Vector2 UNIT_X; // X
	static const Vector2 NEGUNIT_X; // -X

public:

	T x;
	T y;
};

template <class T> 
T DotProduct(const Vector2<T>& a, const Vector2<T>& b);

template <class T> 
Vector2<T> Lerp(f32 value, const Vector2<T>& min, const Vector2<T>& max);

template <class T> Vector2<T> operator* (const Vector2<T>& vec, const T t);
template <class T> Vector2<T> operator* (const T t, const Vector2<T>& vec);

template <class T>
sf::Vector2<T> Vec2ToSFML(const Vector2<T>& vec)
{
        return sf::Vector2<T>(vec.x, vec.y);
}

#include "Vector2.inl"
typedef Vector2<f32> Vector2f;
typedef Vector2<s32> Vector2i;

#endif