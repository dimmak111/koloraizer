#ifndef H_RECT_
#define H_RECT_

#include "Math/Vector2.h"
#include "Utils/Shared.h"

template <class T>
class SFAPI Rect
{
public:

	Rect(T left, T right, T up, T down) : mOrigine(left, top), mEnd(left+top, right+down)
	{
	}

	Rect(Vector2<T> min = Vector2<T>(0,0), Vector2<T> max = Vector2<T>(0,0)) : mOrigine(min), mEnd(max)
	{
	}

	inline T Left() const	{ return mOrigine.x; }
	inline T Right() const	{ return mEnd.x; }
	inline T Up() const		{ return mOrigine.y; }
	inline T Down() const	{ return mEnd.y; }

	bool operator== (const Rect<T>& rect) const;
	bool operator!= (const Rect<T>& rect) const;

private:

	Vector2<T> mOrigine;
	Vector2<T> mEnd;
};

typedef Rect<s32> IntRect;
typedef Rect<f32> FloatRect;

#endif