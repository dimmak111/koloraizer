#ifndef H_SPLINE_
#define H_SPLINE_

#include <Math/Vector2.h>
#include <Utils/Shared.h>

#include <vector>

class Spline
{

public:

	Spline(const std::vector<Vector2f>& pControlPoints);
	~Spline();

	Vector2f Compute();
	virtual Vector2f ComputeAt(f32& t);
	virtual void Render();

private:

	std::vector<Vector2f> mControlPoints;
};

#endif