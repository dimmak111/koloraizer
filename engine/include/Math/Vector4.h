#ifndef H_VECTOR4_
#define H_VECTOR4_

#include "Utils/Shared.h"
#include "Math/Mathf.h"

template <class T>
class Vector4
{
public:

	Vector4(T X = 0, T Y = 0, T Z = 0, T W = 0);
	
	void Set(T X, T Y, T Z, T W);
	
	T Length() const;
	T SquaredLength() const;

	void Normalize();

	// Negate operator
	Vector4<T> operator- () const;

	Vector4<T> operator- (const Vector4<T>& vec) const;
	Vector4<T> operator+ (const Vector4<T>& vec) const;

	Vector4<T>& operator+= (const Vector4<T>& vec);
	Vector4<T>& operator-= (const Vector4<T>& vec);
	
	Vector4<T> operator* (const T t);
	Vector4<T> operator/ (const T t);

	Vector4<T>& operator*= (const T t);
	Vector4<T>& operator/= (const T t);

	bool operator== (const Vector4<T>& vec) const;
	bool operator!= (const Vector4<T>& vec) const;

	operator f32*() { return &x; }
	operator const f32*() const { return &x; }

public:

	static const Vector4 ZERO;

	static const Vector4 UNIT_Z; // Z
	static const Vector4 NEGUNIT_Z; // -Z

	static const Vector4 UNIT_Y; // Y
	static const Vector4 NEGUNIT_Y; // -Y

	static const Vector4 UNIT_X; // X
	static const Vector4 NEGUNIT_X; // -X

public:

	T x;
	T y;
	T z;
	T w;
};

template <class T> 
T DotProduct(const Vector4<T>& a, const Vector4<T>& b);

template <class T> 
Vector4<T> Lerp(f32 value, const Vector4<T>& min, const Vector4<T>& max);

template <class T> Vector4<T> operator* (const Vector4<T>& vec, const T t);
template <class T> Vector4<T> operator* (const T t, const Vector4<T>& vec);

#include "Vector4.inl"
typedef Vector4<f32> Vector4f;
typedef Vector4<s32> Vector4i;

#endif