#ifndef H_VECTOR3_
#define H_VECTOR3_

#include "Utils/Shared.h"
#include "Math/Mathf.h"

template <class T>
class Vector3
{
public:

	Vector3(T X = 0, T Y = 0, T Z = 0);
	
	void Set(T X, T Y, T Z);
	
	T Length() const;
	T SquaredLength() const;

	void Normalize();

	// Negate operator
	Vector3<T> operator- () const;

	Vector3<T> operator- (const Vector3<T>& vec) const;
	Vector3<T> operator+ (const Vector3<T>& vec) const;

	Vector3<T>& operator+= (const Vector3<T>& vec);
	Vector3<T>& operator-= (const Vector3<T>& vec);
	
	Vector3<T> operator* (const T t);
	Vector3<T> operator/ (const T t);

	Vector3<T>& operator*= (const T t);
	Vector3<T>& operator/= (const T t);

	bool operator== (const Vector3<T>& vec) const;
	bool operator!= (const Vector3<T>& vec) const;

	SFAPI operator f32*() { return &x; }
	SFAPI operator const f32*() const { return &x; }

public:

	static const Vector3 ZERO;

	static const Vector3 UNIT_Z; // Z
	static const Vector3 NEGUNIT_Z; // -Z

	static const Vector3 UNIT_Y; // Y
	static const Vector3 NEGUNIT_Y; // -Y

	static const Vector3 UNIT_X; // X
	static const Vector3 NEGUNIT_X; // -X

public:

	T x;
	T y;
	T z;
};

template <class T> 
T DotProduct(const Vector3<T>& a, const Vector3<T>& b);

template <class T> 
Vector3<T> CrossProduct(const Vector3<T>& a, const Vector3<T> b);

template <class T> 
Vector3<T> Lerp(f32 value, const Vector3<T>& min, const Vector3<T>& max);

template <class T> Vector3<T> operator* (const Vector3<T>& vec, const T t);
template <class T> Vector3<T> operator* (const T t, const Vector3<T>& vec);

#include "Vector3.inl"
typedef Vector3<f32> Vector3f;
typedef Vector3<s32> Vector3i;

// #ifdef SHORTCUT
typedef Vector3f vec3;
typedef Vector3i ivec3;
// #endif

#endif