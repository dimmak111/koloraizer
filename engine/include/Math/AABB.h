#ifndef H_AABB_
#define H_AABB_

#include "Math/Vector3.h"
#include "Utils/Shared.h"

class SFAPI AABB
{
	friend class BSphere;

public:
	AABB();
	AABB(const Vector3f& pOrigin, const Vector3f& pEnd);
	~AABB();

	void SetOrigin(const Vector3f& o)			{ mOrigin = o; }
	void SetEnd(const Vector3f& e)				{ mEnd = e; }

	inline const Vector3f& GetOrigin() const	{ return mOrigin; }
	inline const Vector3f& GetEnd() const		{ return mEnd; }

private:

	Vector3f mOrigin;
	Vector3f mEnd;
};

class BSphere
{
	friend class AABB;

public:

	BSphere();
	~BSphere();
};

#endif