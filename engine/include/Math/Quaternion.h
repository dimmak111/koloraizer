#ifndef H_QUATERNION_
#define H_QUATERNION_

#include "Utils/Shared.h"

class Matrix4;

class SFAPI Quaternion
{
public:

	Quaternion(const f32& X = 0.f, const f32& Y = 0.f, const f32& Z = 0.f, const f32 W = 1.f);
	virtual ~Quaternion();

	void Identity();

	void Normalize();
	
	Quaternion Conjugate() const;
	
	const f32 Length() const;
	
	void FromMatrix(const Matrix4& mat);
	void FromEulerAngle(const f32& X, const f32& Y, const f32& Z);

	const Matrix4 ToMatrix() const;

	const Quaternion operator* (const Quaternion& quat) const;

public:

	f32 x;
	f32 y;
	f32 z;
	f32 w;
};

#endif