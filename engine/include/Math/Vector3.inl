
template <class T>
inline Vector3<T>::Vector3(T X, T Y, T Z) : x(X), y(Y), z(Z)
{
}

template <class T>
inline void Vector3<T>::Set(T X, T Y, T Z)
{
	x = X;
	y = Y;
	z = Z;
}

template <class T>
inline T Vector3<T>::Length() const
{
	return Math::Sqrt(x*x + y*y + z*z);
}

template <class T>
inline T Vector3<T>::SquaredLength() const
{
	return (x*x + y*y + z*z);
}

template <class T>
inline void Vector3<T>::Normalize()
{
	f32 l = Length();
	if(l > 0.f) {
		x /= l;
		y /= l;
		z /= l;
	}
}

template <class T>
inline Vector3<T> Vector3<T>::operator- () const
{
	return Vector3<T>(-x, -y, -z);
}

template <class T>
inline Vector3<T> Vector3<T>::operator+ (const Vector3<T>& vec) const
{
	return Vector3<T>(x + vec.x, y + vec.y, z + vec.z);
}

template <class T>
inline Vector3<T> Vector3<T>::operator- (const Vector3<T>& vec) const
{
	return Vector3<T>(x - vec.x, y - vec.y, z - vec.z);
}

template <class T>
Vector3<T>& Vector3<T>::operator+= (const Vector3<T>& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;

	return (*this);
}

template <class T>
Vector3<T>& Vector3<T>::operator-= (const Vector3<T>& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;

	return (*this);
}

template <class T>
Vector3<T> Vector3<T>::operator* (const T t)
{
	return Vector3<T>(x*t, y*t, z*t);
}

template <class T>
Vector3<T> Vector3<T>::operator/ (const T t)
{
	return Vector3<T>(x/t, y/t, z/t); 
}

template <class T>
Vector3<T>& Vector3<T>::operator*= (const T t)
{
	x *= t;
	y *= t;
	z *= t;

	return (*this);
}

template <class T>
Vector3<T>& Vector3<T>::operator/= (const T t)
{
	x /= t;
	y /= t;
	z /= t;

	return (*this);
}

template <class T>
bool Vector3<T>::operator== (const Vector3<T>& vec) const
{
	return (x == vec.x && y == vec.y && z == vec.z);
}

template <class T>
bool Vector3<T>::operator!= (const Vector3<T>& vec) const
{
	return !(*this == vec);
}

template <class T>
T DotProduct(const Vector3<T>& a, const Vector3<T>& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

template <class T>
Vector3<T> CrossProduct(const Vector3<T>& a, const Vector3<T> b)
{
	return Vector3<T>((a.y * b.z) - (b.y * a.z), (a.z * b.x) - (b.z * a.x), (a.x * b.y) - (b.x * a.y));
}

template <class T> 
Vector3<T> operator* (const Vector3<T>& vec, const T t)
{
	return Vector3<T>(vec.x * t, vec.y * t, vec.z * t);
}

template <class T> 
Vector3<T> operator* (const T t, const Vector3<T>& vec)
{
	return Vector3<T>(vec.x * t, vec.y * t, vec.z * t);
}