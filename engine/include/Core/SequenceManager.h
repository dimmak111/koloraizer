#ifndef H_SEQUENCEMANAGER_
#define H_SEQUENCEMANAGER_

#include "Utils/Shared.h"
#include <map>

class App;
class Sequence;

class SFAPI SequenceManager
{
public:

	SequenceManager();
	~SequenceManager();
	
	void RegisterApp(App* app);
	void RegisterSequence(const std::string& key, Sequence* sequence);
	
	void SetCurSequence(std::string key);
	inline Sequence& GetCurSequence()			{ return (*mCurSequence); }

	inline u32 GetNbSequence()					{ return mSequences.size(); }

private:

	App* mApp;

	std::map<std::string, Sequence*> mSequences;
	Sequence* mCurSequence;

private:

	/** 
	* copy ctor private,
	* we dont want this to be copied
	*/
	SequenceManager(const SequenceManager&);

	/** 
	* assignment operator private, 
	* we dont want this class to be copied
	*/
	SequenceManager& operator=(const SequenceManager&);
};

#endif
