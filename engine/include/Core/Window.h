#ifndef H_WINDOW_
#define H_WINDOW_

#include "Utils/Color.h"
#include "Utils/Shared.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class SFAPI Window 
{
public:

	Window();
	Window(u32 w, u32 h, std::string title="");
	virtual ~Window();

	inline bool IsOpened()													{ return mIsOpen; }
	void Close();
	
	void SetClearColor(const Color& color);
	inline Color& GetClearColor()											{ return mClearColor; }

	inline u32 GetWidth()													{ return mWidth; }
	inline u32 GetHeight()													{ return mHeight; }
	inline u32 GetHalfWidth()												{ return mHalfWidth; }
	inline u32 GetHalfHeight()												{ return mHalfHeight; }

	void ShowCursor(bool state);
	
	void SetFullScreen();													//{ mWindow}
	bool IsFullScreen();

	void SetTitle(const std::string& title);

	inline void Clear();
	inline void Draw(const sf::Drawable& object)							{ mWindow.draw(object); }
	inline void Display()													{ mWindow.display(); }
	
	void Resize(s32 w, s32 h);

	inline sf::RenderWindow& GetWindow()									{ return mWindow; }

private:
	
	bool mIsOpen;

	u32 mWidth;
	u32 mHeight;
	u32 mHalfWidth;
	u32 mHalfHeight;

	std::string mWinTitle;

	sf::RenderWindow mWindow;
	sf::ContextSettings mSettings;

	Color mClearColor;
};

#endif