#ifndef H_OBJECT_
#define H_OBJECT_

#include "Math/ITransform.h"
#include "Math/Matrix4.h"
#include "Math/Vector3.h"

#include "Utils/Shared.h"

#include <vector>

class Object;

typedef std::vector<Object*> Objects;

class SFAPI Object : public ITransform
{
public:

	Object(const std::string& name="");
	virtual ~Object();

	const inline u32 GetId() const { return mId; } 

	void SetName(const std::string& name);
	const std::string& GetName() const;

	void Add(Object* object);
	void Remove(Object* object);
	void Clear();

	virtual void OnAdded(Object* object)			{ }
	virtual void OnRemoved(Object* object)			{ }

	virtual void Update(u32 dt=0);
	virtual void Render();

	Objects& GetChildren();
	Object* FindObjectByName(const std::string& name);

	void SetParent(Object* object);
	Object* GetParent();

	void SetVisible(bool visible);
	bool IsVisible() const;

	void SetEnabled(bool enable);
	bool IsEnabled() const;

	void SetSize(const Vector3f& size) { mSize = size; }
	inline const Vector3f& GetSize() const { return mSize; }

	void SetCenter(const Vector3f center) { mCenter = center; }
	inline const Vector3f& GetCenter() const { return mCenter; }

	virtual Matrix4 GetWorldTransform();

	static u32 NextId;

private:

	std::string mName;
	u32 mId;

	Objects mChildren;
	Object* mParent;

	Vector3f mSize;
	Vector3f mCenter;

	bool mVisible;
	bool mEnabled;
};

#endif