#ifndef H_SEQUENCE_
#define H_SEQUENCE_

#include "App.h"
#include "Core/Object.h"
#include "Utils/Shared.h"

#include <vector>

class Input;

class SFAPI Sequence : public Object
{

public:

	Sequence(App* app, const std::string& next="");
	virtual ~Sequence();

	virtual void Init()										{ }
	virtual void Release()									{ }
	virtual void Pause()									{ }
	virtual void Resume()									{ }
	
	virtual void ProcessEvents(Input& input)				{ }

	void Stop();
	
	inline void SetStopState(bool stop)						{ mStopped = stop; }
	inline bool IsStopped() const							{ return mStopped; }

	bool IsPaused();

	inline void SetNextSequenceAs(const std::string& key)	{ mNextSequence = key; }
	inline const std::string& GetNextSequence() const		{ return mNextSequence; }

	inline App& GetApp()									{ return *mApp; }
	inline Input& GetInputManager()							{ return mApp->GetInputsManager(); }

private:

	App* mApp;

	std::string mNextSequence;
	
	bool mStopped;
};

#endif