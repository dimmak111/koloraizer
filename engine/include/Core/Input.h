#ifndef H_INPUT_
#define H_INPUT_

#include "Core/Window.h"
#include "Utils/Keys.h"
#include "Utils/Shared.h"

#include <map>

enum eInputType
{
	IT_Mouse,
	IT_Keyboard,
	IT_Joystick
};

struct InputBind
{
	eInputType mType;
	sf::Event::EventType mEventType;	
};

class SFAPI Input 
{
public:

	Input();
	void Init(Window* pWindow);

	void BindKey(const std::string& key);
	void BindMouseButton(const std::string& key);
	void BindJoystickButton(const std::string& key);

	const s32 GetMouseX() const { return sf::Mouse::getPosition(mWindow->GetWindow()).x; }
	const s32 GetMouseY() const { return sf::Mouse::getPosition(mWindow->GetWindow()).y; }
	
	const f32 GetJoystickX(u32 index);
	const f32 GetJoystickY(u32 index);

	bool IsKeyUp(Key key);
	bool IsKeyHit(Key key);
	bool IsKeyDown(Key key);

	bool IsMouseUp(MouseButton button);
	bool IsMouseHit(MouseButton button);
	bool IsMouseDown(MouseButton button);

	bool IsJoystickExist(u32 index);
	bool IsJoystickButtonUp(u32 index);

	bool GetEvents()						{ return mWindow->GetWindow().pollEvent(mEvents); }
	inline const sf::Event& GetCurEvent()	{ return mEvents; }
	sf::Event::EventType GetEventType()		{ return mEvents.type; }
	void CatchEvent(){};

private:
	
	std::map<std::string, InputBind> mBinds;

	f32 mThreshold;

	sf::Event mEvents;
	Window* mWindow;
};

#endif