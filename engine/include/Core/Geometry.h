#ifndef H_GEOMETRY_
#define H_GEOMETRY_

#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Utils/Shared.h"

#include <vector>

#include <SFML/OpenGL.hpp>

class SFAPI Geometry
{
public: 

	enum Primitive
	{
		PrimitiveTriangle	= GL_TRIANGLES,
		PrimitiveQuad		= GL_QUADS,
		PrimitivePoint		= GL_POINTS
	};

	Geometry();
	Geometry(std::vector<Vector3f> &verts, std::vector<Vector3f>& normals, std::vector<Vector2f>& texcoords, std::vector<u32>& indices);
	Geometry(std::vector<Vector3f> &verts, std::vector<Vector3f>& normals, std::vector<Vector2f>& texcoords, std::vector<Vector3f> colors);
	virtual ~Geometry();

	inline const std::vector<Vector3f>& GetVertices() const		{ return mVertices; }
	inline const std::vector<Vector3f>& GetNormals() const		{ return mNormals; }
	inline const std::vector<Vector2f>& GetTexCoords() const	{ return mTexCoords; }
	inline const std::vector<Vector3f>& GetColors() const		{ return mColors; }
	inline const std::vector<u32>& GetIndices() const			{ return mIndices; }

	inline const bool HasTexCoords() const						{ return mHasTexCoords; }
	inline const bool HasNormals() const						{ return mHasNormals; }

	void MakeCube(f32 size);
	void MakeSphere(f32 radius, u32 u, u32 v);
	void MakeQuad(f32 size);

private:

	std::vector<Vector3f>	mVertices;
	std::vector<Vector3f>	mNormals;
	std::vector<Vector3f>	mColors;
	std::vector<Vector2f>	mTexCoords;

	std::vector<u32>		mIndices;

	bool					mHasNormals;
	bool					mHasTexCoords;

	Primitive				mPrimitive;
};

#endif