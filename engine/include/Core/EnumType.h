#ifndef ENUMS_H
#define ENUMS_H

enum eRessourceType
{
	RT_Font,
	RT_Shader,
	RT_Texture
};

#endif