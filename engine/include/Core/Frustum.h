#ifndef H_FRUSTUM_
#define H_FRUSTUM_

#include "Asset/Shader.h"
#include "Core/Camera.h"
#include "Utils/Shared.h"

/* http://www.lighthouse3d.com/tutorials/view-frustum-culling/ */

class Matrix4;
class Mesh;

class SFAPI Frustum
{
public:

	friend class Camera;

	Frustum();
	~Frustum();

	bool IsPointInsied();
	bool IsBoxInside(const Matrix4& pMVP, const Vector3f& min, const Vector3f& max);
	bool IsSphereInside();
	bool IsMeshInside(Mesh* m);

	void AttachCamera(Camera* pCam) { mCamera = pCam; }

private:

	Camera* mCamera;
};

#endif