#ifndef H_CAMERA_
#define H_CAMERA_

#include "Math/Vector3.h"
#include "Utils/Shared.h"

#include "Core/Frustum.h"
#include "Core/Input.h"

#include "App.h"

class Window;

class SFAPI Camera
{
public:

	friend class Frustum;

	Camera();
	Camera(const Vector3f& at, const Vector3f& to, const Vector3f& up);
	
	void Set(const Vector3f& at, const Vector3f& to, const Vector3f& up, App& app);

	void Enable()								{ mEnabled = true; }
	void Disable()								{ mEnabled = false; }
	inline bool IsEnabled()						{ return mEnabled; }

	virtual void ProcessEvents(Input& inputs);
	
	void AttachFrustum(Frustum* pFrustum)		{ mFrustum = pFrustum; }

	// setters
	void SetSensivity(const f32 s)				{ mSensivity = s; }
	void SetSpeed(const f32 s)					{ mSpeed = s; }

	// getters
	inline f32 GetSensivity() const				{ return mSensivity; }
	inline f32 GetSpeed() const					{ return mSpeed; }
	inline const Matrix4& GetViewMatrix() const { return mViewMatrix; }


private:

	void VectorsFromAngles();
	void UpdateView();

private:

	f32 mTheta;
	f32 mPhi;

	Vector3f mAt;
	Vector3f mTo;
	Vector3f mUp;

	Vector3f mForward;
	Vector3f mRight;

	f32 mSpeed;
	f32 mSensivity;

	s32 mLastMouseX;
	s32 mLastMouseY;

	bool mToUpdate;
	bool mEnabled;

	Window* mWindow;
	Frustum* mFrustum;

	Matrix4 mViewMatrix;
};

#endif