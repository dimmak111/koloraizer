#ifndef H_SETTINGS_
#define H_SETTINGS_

#include "Utils/File.h"
#include "Utils/Shared.h"
#include "Utils/Singleton.h"

#include <vector>

namespace 
{
	const std::string defaultSettings = "Settings.cfg";
}

struct SFAPI Setting
{
	Setting() : Key(""), Value("") {
	}

	Setting(const std::string& key, const std::string& value) : Key(key), Value(value) {
	}

	std::string Key;
	std::string Value;
};

class SFAPI Settings : public Singleton<Settings>
{
	friend class Singleton<Settings>;

public:

	// default settings.
	static const std::string WINDOW_TITLE;
	static const std::string WINDOW_WIDTH;
	static const std::string WINDOW_HEIGHT;
	static const std::string ASSET_FOLDER; 

	void Parse();

	const static std::string GetSettingString(const std::string& key);
	const static s32 GetSettingInt(const std::string& key);
	const static f32 GetSettingFloat(const std::string& key);
	const static bool GetSettingBool(const std::string& key);

protected:

	Settings();
	virtual ~Settings();

	void AddSetting(const Setting& set);
	Setting SplitRawSetting(const std::string& set);

	const std::string GetSetting(const std::string& key);

private:

	bool mParsed;

	std::vector<Setting> mSettings;
	File mSettingFile;
};

#endif