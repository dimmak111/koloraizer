#ifndef H_TIMER_
#define H_TIMER_

#include "Shared.h"

#include <SFML/System/Clock.hpp>

class SFAPI Timer 
{
public:

	Timer();
	~Timer();
	
	void Reset();
	void Tick();

	inline u32 GetTimeSinceStartUp()	{ return mClock.getElapsedTime().asMilliseconds(); }
	inline u32 GetFrameCount()			{ return mFrameCount; }
	inline u32 GetDeltaTime()			{ return mDeltaTime; }

private:
	
	u32 mFrameCount;
	u32 mDeltaTime;
	u32 mTime;

	sf::Clock mClock;

};

#endif