#ifndef H_COLOR_
#define H_COLOR_

#include "Utils/Shared.h"

#include <SFML/Graphics.hpp>

class SFAPI Color
{
public:

	Color(f32 R = 0.f, f32 G = 0.f, f32 B = 0.f, f32 A = 1.f);

	//void RGB(f32 );
	void RGBA();

	f32 R() const { return r; }
	f32 G() const { return g; }
	f32 B() const { return b; }
	f32 A() const { return a; }

	static const Color Black;
	static const Color White;

	operator f32*() { return &r; }
	operator const f32*() const { return &r; }

private:

	f32 r, g, b, a;
};

Color ToAPIColor(const sf::Color& color);
sf::Color ToSFMLColor(const Color& color);

#endif