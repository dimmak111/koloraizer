#ifndef H_SINGLETON_
#define H_SINGLETON_

template <typename T>
class Singleton
{
protected:
	Singleton () { }
	~Singleton () { }

public:

	static T* Get ()
	{
		if (0 == mSingleton)
		{
			mSingleton = new T;
		}

		return (static_cast<T*> (mSingleton));
	}

	static T& Call ()
	{
		if (0 == mSingleton)
		{
			mSingleton = new T;
		}

		return *mSingleton;
	}

	static void Kill ()
	{
		if (0 != mSingleton)
		{
			delete mSingleton;
			mSingleton = 0;
		}
	}

private:

	Singleton(Singleton &){}
	void operator=(Singleton &){}

	static T *mSingleton;
};

template <typename T>
T *Singleton<T>::mSingleton = 0;

#endif