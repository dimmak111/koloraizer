#ifndef H_SMARTPOINTER_
#define H_SMARTPOINTER_

template <class T>
class SmartPointer
{
public:

	SmartPointer();
	virtual ~SmartPointer();

	T& operator*() const				{ return *mPtr; }
	T* operator->()	const				{ return mPtr; }

private:

	T* mPtr;
};

#endif