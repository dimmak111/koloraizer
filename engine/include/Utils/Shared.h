#ifndef H_SHARED_
#define H_SHARED_

	#define ENGINE_MAJOR 0
	#define ENGINE_MINOR 3
	#define ENGINE_PATCH 0

	#if defined(_WIN32) || defined(WIN32)
	#	define PLATFORM_WIN32
	#	include <ctime>
	#else
	#	include <sys/time.h>
	#	if (__APPLE__)
	#		include <TargetConditionals.h>
   	#		if TARGET_OS_MAC == 1 && TARGET_OS_IPHONE == 0
    #			define PLATFORM_MACOSX
   	#		endif
   	#		if TARGET_OS_IPHONE==1 
    #			define PLATFORM_IOS
  	#		endif
	#	elif ANDROID
	#		define PLATFORM_ANDROID
	#	endif
	#endif

	#define eol "\n"

	// types redefines
	typedef unsigned char 		u8;
	typedef char 				s8;
	typedef unsigned short 		u16;
	typedef short 				s16;
	typedef unsigned int 		u32;
	typedef int 				s32;
	typedef long long 			s64;
	typedef unsigned long long 	u64;
	typedef float 				f32;
	typedef double 				f64;

	/// DLL managment under WIN32
	#ifdef PLATFORM_WIN32
	#	ifdef SFEXPORTS
	#		define SFAPI __declspec(dllexport)
	#	else
	#		define SFAPI __declspec(dllimport)
	#	endif
	#	ifdef _MSC_VER
    #		pragma warning(disable : 4251)
    #		pragma warning(disable : 4661)
	#	endif
	#else
	#	define SFAPI
	#endif

	// Used everywhere
	#include <string>
	#include "Debug/LogManager.h"

	// GL
	#ifdef PLATFORM_WIN32
		#include <glew/glew.h>	
	#endif

	// Returns time in "HH:MM:SS" format
	inline std::string GetTime(){
		time_t tps = time(NULL);
		tm temps;
		localtime_s(&temps, &tps);
		char ret[9];
		strftime(ret, 9, "%H:%M:%S", &temps);
		return ret;
	}

	// Returns date in "Day DD MMM YYYY" format
	inline std::string GetDate(){
		time_t tps = time(NULL);
		tm temps;
		localtime_s(&temps, &tps);
		char ret[16];
		strftime(ret, 16, "%a %d %b %Y", &temps);
		return ret;
	}

#endif