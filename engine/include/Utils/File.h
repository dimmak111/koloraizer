#ifndef H_FILE_
#define H_FILE_

#include "Utils/Shared.h"

#include <fstream>
#include <sstream>

class SFAPI File
{
public:

	File();
	virtual ~File();

	void Open(const std::string& file);
	
	bool IsOpened();

	void Write(const std::string& line);
	std::string Read() const;
	std::string GetLine();

	bool End() const;
	void Close();

	static bool Exist(const std::string& filename);

private:

	std::fstream mFile;
	std::string mFileName;
};

#endif