#ifndef _H_GEOMETRYLOADER_
#define _H_GEOMETRYLOADER_

#include "Core/Geometry.h"
#include "Utils/Shared.h"

class SFAPI GeometryLoader
{
public:
	bool Load(const std::string& url, Geometry& geom);
};

/*
class SFAPI OBJLoader {
public:
	OBJLoader(const std::string& path);
	~OBJLoader();

	bool Eof() const;
	bool IsValid() const;

	const std::string& GetToken() const;
	char GetLastChar() const;
	char GetChar(u32 at) const;

	int GetFloat(f32& target);
	int GetUnsigned(u32& target);
	int GetVec2(Vector2f& target);
	int GetVec3(Vector3f& target);

	int ReadToken();
	int ReadLine();
	void SkipLine();

	bool GetGeometry(Geometry& geom);

private:
	enum CharInfo{ AlphaNum = 0, WhiteSpace, NewLine, Undefined };

	CharInfo GetCharInfo(const char& c) const;

private:

	FILE* mFile;
	std::string mFilename;
	std::string mCurrentToken;
};
*/

#endif