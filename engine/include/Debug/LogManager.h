#ifndef H_ILOGGER_
#define H_ILOGGER_

#include "Utils/Singleton.h"
#include "Utils/Shared.h"

#include <fstream>
#include <iostream>

class SFAPI LogManager : public Singleton<LogManager>
{
	friend class Singleton<LogManager>;

public:
	
	void SetLogFile(const std::string& logFile);

	void Error(const std::string& message);
	void Log(const std::string& message);
	void Warning(const std::string& message);

protected:

	LogManager();
	virtual ~LogManager();

private:

	std::ofstream mLogFile;
};

#endif