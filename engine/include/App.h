#ifndef H_APP_
#define H_APP_

#include "Asset/RessourceManager.h"

#include "Core/Window.h"
#include "Core/Input.h"
#include "Core/SequenceManager.h"

#include "Renderer/Renderer.h"

#include "Utils/Shared.h"
#include <map>

class Settings;

class SFAPI App
{

public:

	static const u32 DEFAULT_WINDOW_WIDTH				= 640;
	static const u32 DEFAULT_WINDOW_HEIGHT				= 480;

	App();
	virtual ~App();
	
	virtual void Init()									{ }

	virtual void RegisterSequences(SequenceManager* sm)=0;
	virtual void ReleaseAndCleanUp()=0;

	inline void SetUpdateRate(const u32 updateRate)		{ mUpdateRate = updateRate; }
	inline const u32 GetUpdateRate() const				{ return mUpdateRate; }

	inline void SetMaxUpdate(const u32 maxUpdate)		{ mMaxUpdate = maxUpdate; }
	inline const u32 GetMaxUpdate() const				{ return mMaxUpdate; }

	void Run();

	inline Window& GetWindow()							{ return mWindow; }
	inline SequenceManager& GetSequenceManager()		{ return mSequenceManager; }
	//inline RessourceManager& GetRessourceManager()		{ return RessourceManager::Call(); }
	inline Renderer& GetRendererManager()				{ return Renderer::Call(); }
	inline Input& GetInputsManager()					{ return mInputs; }

	void ProcessEvents(Input& input);
	virtual void ProcessCustomEvents(Input& input)		{ }

private:
	
	void RegisterInternalShaders();

private:

	Window									mWindow;
	Input									mInputs;

	SequenceManager mSequenceManager;

	u32										mUpdateRate;
	u32										mMaxUpdate;

private:

	App(const App&);
	App& operator=(const App&);

};

#endif