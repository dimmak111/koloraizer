#ifndef H_FONT_
#define H_FONT_

#include "Asset/Ressource.h"
#include "Asset/Texture.h"
#include "Utils/Shared.h"

#include <map>

struct Glyph 
{
    f32 width, height;
    f32 left, top;
    f32 ax, ay;
	f32 tx;

	Glyph() 
	: width(0.f), height(0.f), 
	  left(0.f), top(0.f), 
	  ax(0.f), ay(0.f), 
	  tx(0.f) 
	{
	}
};

class Font : public Ressource
{
public:

	Font();
	~Font();

	bool FromFile(const std::string& filename);
	
	// getters
	Glyph& GetGlyph(u32 keyCode);

private:
	
	Texture mTextureAtlas;
	s32 mGlyphOffset;
	s32 mWidth, mHeight;
	std::map<u32, Glyph> mGlyphs;
};

#endif