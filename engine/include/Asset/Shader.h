#ifndef H_SHADER_
#define H_SHADER_

#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Math/Matrix4.h"

#include "Utils/Shared.h"

class Color;

class SFAPI Shader
{
public:

	Shader();
	~Shader();
	
	bool ComputeAndLink(const std::string& vs, const std::string& fs, const std::string& gs = "");

	void Bind();
	void UnBind();

	inline u32 GetProgram() const { return mProgram; }

	void SendFloat(const std::string& key, f32 value);
	void SendVector2(const std::string& key, const Vector2f& vec);
	void SendVector3(const std::string& key, const Vector3f& vec);
	void SendVector4(const std::string& key, const Vector4f& vec);
	void SendColor(const std::string& key, const Color& pColor);
	void SendMatrix4(const std::string& key, const Matrix4& pMatrix);
	void SendTexture(const std::string& key, const u32& index);
	
private:
	
	void LoadVertexShader(const std::string& vs);
	void LoadGeometryShader(const std::string gs);
	void LoadFragmentShader(const std::string& fs);

private:

	u32 mProgram;

	u32 mVShader;
	u32 mFShader;
	u32 mGShader;
};

#endif