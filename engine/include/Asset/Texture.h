#ifndef H_TEXTURE_
#define H_TEXTURE_

#include "Ressource.h"
#include "Utils/Shared.h"

#include <SFML/Graphics/Texture.hpp>

class SFAPI Texture : public Ressource
{
public:

	Texture();
	virtual ~Texture();

	void Create(const u32 width, const u32 height);
	bool FromFile(const std::string& file);

	void Resize(u32 width, u32 height);

	void Bind();
	void UnBind();

	//setters
	void SetFormats(u32 format, u32 global)			{ mFormat = format; mGlobalFormat = global; }
	void SetFilters(u32 min, u32 mag)				{ mMinFilter = min; mMagFilter = mag; }
	void SetSamplerParameters(u32 param)			{ mSamplerParam = param; }

	//getters
	inline const u32 GetId() const					{ return mId; }
	inline const u32 GetWidth() const				{ return mWidth; }
	inline const u32 GetHeight() const				{ return mHeight; }

public:

	static const u32 LINEAR;
    static const u32 NEAREST;
    static const u32 MIPMAP;

private:
	
	u32 mUnit;
	u32 mId;
	
	u32 mWidth;
	u32 mHeight;
	
	u32 mFormat;
	u32 mGlobalFormat;

	u32 mMinFilter;
	u32 mMagFilter;
	
	u32 mSamplerParam;
};

#endif