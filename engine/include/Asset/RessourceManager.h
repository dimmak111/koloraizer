#ifndef H_RESSOURCEMANAGER_
#define H_RESSOURCEMANAGER_

#include "Asset/Ressource.h"
#include "Asset/Shader.h"
#include "Utils/Singleton.h"
#include "Utils/Shared.h"

#include <map>
#include <vector>

template <class T>
class SFAPI RessourceManager
{

public:

	void AddLocation(const std::string& name);

	void Add(const std::string& name, Ressource* res);
	void Remove(const std::string& name);

	void CleanUp();

	T* Get(const std::string& name);

protected:

	RessourceManager()				{ }
	virtual ~RessourceManager()		{ }

private:
	
	typedef std::vector<std::string>	LocationsVec;
	typedef std::map<std::string, T*>	RessourcesMap;

	LocationsVec	mLocations;
	RessourcesMap	mRessources;
};

/*
namespace 
{
	typedef std::map<std::string, std::string> LocationMap;
	typedef std::map<std::string, Ressource*> RessourceMap;
}

class SFAPI RessourceManager : public Singleton<RessourceManager>
{
	friend class Singleton<RessourceManager>;

public:

	void AddLocation(const std::string& name, const std::string& type);
	const std::string& GetLocation(const std::string& type);

	void Add(const std::string& name, Ressource* pRessource);
	void Remove(const std::string& name);

	template<class T>
	T* Get(const std::string& key);

protected:

	RessourceManager();
	virtual ~RessourceManager();

private:
	
	LocationMap mLocations;
	LocationMap::iterator mLocationIt;

	RessourceMap mRessources;
	RessourceMap::iterator mRessourcesIt;
};

template <class T>
inline T* RessourceManager::Get(const std::string& key)
{
	if(mRessources[key])
		return static_cast<T*>(mRessources[key]);
	return 0;
}
*/
#endif