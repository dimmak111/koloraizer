#ifndef H_RESSOURCE_
#define H_RESSOURCE_

#include "Utils/Shared.h"

class SFAPI Ressource 
{

public:

	Ressource();
	virtual ~Ressource();

	void SetName(const std::string& name)	{ mName = name; }
	inline const std::string& GetName()		{ return mName; }

private:

	u32			mId;
	std::string mName;
};

#endif