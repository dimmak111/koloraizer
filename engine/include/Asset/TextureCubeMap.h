#ifndef H_TEXTURECUBEMAP_
#define H_TEXTURECUBEMAP_

#include "Asset/Ressource.h"
#include "Utils/Shared.h"

class SFAPI TextureCubeMap : public Ressource
{
public:

	TextureCubeMap();
	~TextureCubeMap();

	bool FromFiles(	const std::string& front, 
					const std::string& back, 
					const std::string& left, 
					const std::string& right, 
					const std::string& top, 
					const std::string& bottom );

	void Bind();
	void UnBind();

private:

	u32 mId;
};

#endif