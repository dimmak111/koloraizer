#ifndef H_MESH_
#define H_MESH_

#include "Core/Object.h"
#include "Math/AABB.h"
#include "Math/Vector2.h"
#include "Utils/Shared.h"

#include <vector>

class Geometry;
class Material;

class SFAPI Mesh : public Object
{
public:
	
	Mesh();
	Mesh(Geometry* geom, Material* mat);

	void CreateBuffers();
	virtual void Render();

private:

	Geometry* mGeometry;
	Material* mMaterial;

	u32 mVao;

	u32 mVbo;
	u32 mNbo;
	u32 mCbo;
	u32 mTbo;
	u32 mIbo;
	/*
	Mesh();
	Mesh(const std::vector<Vector3f>& v, const std::vector<Vector3f>& n, const std::vector<Vector2f>& t, const std::vector<u32>& indices, Shader* s);
	virtual ~Mesh();

	void MakeQuad();

	// \brief create normalized cube ie (1, 1, 1)
	void MakeCube();

	// \brief create normalized sphere ie (r = 1)
	void MakeSphere();

	bool FromOBJ(const std::string& name);

	void CreateBuffers();
	void CreateNormals();
	void CreateTextCoords();
	void CreateAABB();

	void SetShaderToUse(Shader* shader)						{ mShader = shader; }
	inline Shader* GetShaderUsed()							{ return mShader; }

	void SetTexture(Texture* text)							{ mTexture = text; }
	inline Texture* GetTexture()							{ return mTexture; }

	virtual void Render();
	virtual void RenderSimplified();

	virtual void PreRender()								{ }
	virtual void PostRender()								{ }

	// getters
	inline const std::vector<Vector3f> GetVertices() const	{ return mVertices; }
	inline const std::vector<Vector3f> GetNormals() const	{ return mNormals; }
	inline const std::vector<u32> GetIndices() const		{ return mIndices; }
	inline const AABB GetAABB() const						{ return mAABB; }
	inline const u32 GetVAO() const							{ return mVao; }

protected:

	std::vector<Vector3f> mVertices;
	std::vector<Vector3f> mNormals;
	std::vector<Vector2f> mTextCoord;
	std::vector<Vector3f> mColors;
	std::vector<u32> mIndices;

	u32 mVao;

	u32 mVbo;
	u32 mNbo;
	u32 mCbo;
	u32 mTbo;
	u32 mIbo;

	u32 mNbIndices;

	Shader* mShader;
	Texture* mTexture;

	AABB mAABB;
	//Geometry mGeometry;
	//Material mMaterial;

private:
	bool bHasNormals;
	bool bHasTexCoords;
	*/
};

#endif