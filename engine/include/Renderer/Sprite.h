#ifndef H_SPRITE_
#define H_SPRITE_

#include "Asset/Texture.h"
#include "Core/Object.h"
#include "Math/Rect.h"
#include "Utils/Shared.h"

#include <vector>

/*
class Animation
{
	struct Frame
	{
		sf::IntRect mRect;
	};

public:

	Animation();
	~Animation();

	void AddFrame();
	void Clear();

	std::vector<Frame> mFrames;
};
*/

class Texture;
class Shader;

class SFAPI Sprite : public Object
{
public:

	/*
	Sprite(Texture* text, Shader* shader, const IntRect& textRect = IntRect());
	virtual ~Sprite();

	virtual void Update(u32 dt=0);
	virtual void Render();

	virtual void PreRender()					{ }
	virtual void PostRender()					{ }

	void SetTextureRect(const IntRect& rect);

	inline void SetShader(Shader* shader)		{ mShader = shader; }
	inline Shader* GetUsedShader()				{ return mShader; }	
	
	inline void SetTexture(Texture* text)		{ mTexture = text; }
	inline Texture* GetTexture()				{ return mTexture; }
	// getters
	std::vector<Vector3f>& GetVertices()		{ return mVertices; }


protected:

	void ComputeVerticesCoords();
	void ComputeTextCoords();
	
	void UpdateBuffers();

private:

	Shader* mShader;

	Texture* mTexture;
	IntRect mTextRect;

	std::vector<Vector3f> mVertices;
	std::vector<Vector2f> mTextCoord;
	std::vector<u32> mIndices;

	u32 mVao;
	u32 mVbo;
	u32 mTbo;
	u32 mIbo;
	*/
};

#endif