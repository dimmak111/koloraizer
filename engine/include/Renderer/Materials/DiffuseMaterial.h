#ifndef H_DIFFUSEMATERIAL_
#define H_DIFFUSEMATERIAL_

#include "Asset/Texture.h"
#include "Renderer/Material.h"
#include "Utils/Color.h"

class SFAPI DiffuseMaterial : public Material
{

public:
	
	DiffuseMaterial(Color diffuse = Color::White);

	void SetDiffuseColor(Color c);
	void SetDiffuseTexture(Texture* tex);

	virtual void Bind();

protected:

	Color mDiffuseColor;
	Texture* mDiffuseTex;
};

#endif