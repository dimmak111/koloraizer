#ifndef H_LIGHT_
#define H_LIGHT_

#include "Utils/Color.h"
#include "Utils/Shared.h"

class Light
{
public:

private:
	
	f32 mAttenuation;
	f32 mRadius;
	Color mColor;

};

#endif