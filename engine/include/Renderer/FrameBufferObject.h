#ifndef H_FrameBufferObject_
#define H_FrameBufferObject_

#include "Utils/Shared.h"

#include <vector>

namespace
{
	const u32 MAXE_FRAMEBUFFER_SIZE = 1024;
}

class Texture;

class SFAPI FrameBufferObject
{
public:

	FrameBufferObject(bool useDepth = false);
	virtual ~FrameBufferObject();

	void Create(u32 texts = 1);
	void Clear();
	
	void Resize(u32 w, u32 h);
	void ResizeViewPort();

	void Bind();
	void UnBind();

	// getters
	inline u32 GetId()					{ return mId; }
	Texture* GetTexture(u32 i)			{ return mTextures[i]; }

private:

	u32 GetAttachement(u32 i);

private:

	u32 mId;

	std::vector<Texture*> mTextures;
	std::vector<u32> mDrawBuffers;

	bool mHasDepth;
	u32 mDepthBuffer;
	
	//u32 mDrawBuffers[1];

	u32 mWidth;
	u32 mHeight;
};

#endif