#ifndef H_TEXT_
#define H_TEXT_

#include "Asset/Font.h"
#include "Asset/Shader.h"
#include "Core/Object.h"
#include "Math/Vector2.h"
#include "Utils/Shared.h"

class Text : public Object
{
public: 

	Text(const std::string& text="", Font* font = 0, u32 size = 10);
	~Text();

	static void InitStatics();

	void SetFont(const Font* font);
	const Font* GetFont() const;

	void SetStyle();
	void GetStyle();

	void SetText(const std::string& text);
	const std::string& GetText() const;

private:

	static Shader StaticShader;

	std::string mText;
	u32 mSize;

	Font* mFont;

	bool mItalic;
	bool mBold;

	u32 mVao;
	u32 mVbo;
	u32 mIbo;
};

#endif