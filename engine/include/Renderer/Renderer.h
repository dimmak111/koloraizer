#ifndef H_RENDERER_
#define H_RENDERER_

#include "Math/Matrix4.h"

#include "Renderer/Text.h"

#include "Utils/Shared.h"
#include "Utils/Singleton.h"

#include <vector>

#include <glew/glew.h>
#include <SFML/OpenGL.hpp>

class Object;
class Window;

class SFAPI Renderer : public Singleton<Renderer>
{
	friend class Singleton<Renderer>;

public:

	void Init(Window* win, Object* root=0);

	void SetRoot(Object* root);
	inline Object* GetRoot() const							{ return mRoot; }

	void Begin();
	void Render();
	void End();

	void Clear();

	void PushText(Text* pText);
	
	enum eRenderMode
	{
		RENDERMODE_WIREFRAME	= GL_LINE,
		RENDERMODE_POINT		= GL_POINT,
		RENDERMODE_FILL			= GL_FILL
	};

	void SetRenderMode(eRenderMode pRenderMode)					{ glPolygonMode(GL_FRONT, pRenderMode); }
	//eRenderMode GetRenderMode(eRenderMode& pRenderMode)		{ return mRenderMode; }

	void EnableDepth();
	void DisableDepth();

	void Set2DProjectionMatrix(const Matrix4& matrix);
	void Set3DProjectionMatrix(const Matrix4& matrix);
	void Set3DViewMatrix(const Matrix4& view);

	inline const Matrix4& Get2DProjectionMatrix()			{ return m2DProjectionMatrix; }
	inline const Matrix4& Get3DProjectionMatrix()			{ return m3DProjectionMatrix; }
	inline const Matrix4& Get3DViewMatrix()					{ return m3DViewMatrix; }

	Vector3f UnProject(s32 x, s32 y);
	// getters for normal, inverse

protected:

	Renderer();
	virtual ~Renderer();

	virtual void RenderDebug();
	virtual void RenderText();

private:

	Window* mWindow;
	Object* mRoot;

	eRenderMode mRenderMode;

	std::vector<Text*> mTextToRender;
	std::vector<Text*>::iterator mTextIt;

	Matrix4 m3DViewMatrix, m3DProjectionMatrix;
	Matrix4 m2DProjectionMatrix;
};

#endif

