#ifndef H_MATERIAL_
#define H_MATERIAL_

#include "Asset/Shader.h"
#include "Utils/Shared.h"

class SFAPI Material
{
public:

	Material();
	Material(Shader* s);
	~Material();

	inline void		SetShader(Shader* s)	{ mShader = s; } 
	inline Shader*	GetShader()				{ return mShader; }

	virtual void Bind();

protected:

	Shader* mShader;
};

#endif